<?php
class Doc extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = "Dokumen Manajemen Risiko";
    /*$data['res'] = $this->db
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_PERIODISAKTIF, 'desc')
    ->order_by(COL_PERIODFROM, 'desc')
    ->get(TBL_TPMDPERIODE)
    ->result_array();*/
    if($ruser[COL_ROLEID]==ROLEADMIN) {
      $this->template->load('backend' , 'doc/index', $data);
    } else {
      $this->template->load('backend' , 'doc/index-opd', $data);
    }
  }

  public function index_load() {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterPeriod = !empty($_POST['filterPeriod'])?$_POST['filterPeriod']:null;
    $filterOPD = !empty($_POST['filterOPD'])?$_POST['filterOPD']:null;

    $ruser = GetLoggedUser();
    $orderables = array(null,COL_OPDNAMA,COL_TAHUN,COL_KETERANGAN);
    $cols = array(COL_OPDNAMA,TBL_TRISIKO.'.'.COL_TAHUN,COL_KETERANGAN,COL_STATUS);

    $queryAll = $this->db->where(COL_ISDELETED, 0)->get(TBL_TRISIKO);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($filterPeriod)) {
      $this->db->where(TBL_TRENSTRA.'.'.COL_IDPERIODE, $filterPeriod);
    }
    if(!empty($filterOPD)) {
      $this->db->where(TBL_TRISIKO.'.'.COL_IDOPD, $filterOPD);
    }
    if($ruser[COL_ROLEID]==ROLEUSER) {
      $this->db->where(TBL_TRISIKO.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO]);
    }

    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      $this->db->order_by($order, $_POST['order']['0']['dir']);

    } else {
      $this->db->order_by(TBL_MOPD.'.'.COL_OPDNAMA, 'asc');
      $this->db->order_by(TBL_TRISIKO.'.'.COL_TAHUN, 'asc');
      $this->db->order_by(TBL_TRISIKO.'.'.COL_KETERANGAN, 'asc');
    }

    $q = $this->db
    ->select("trisiko.*, mopd.OPDNama")
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDOPD,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_MOPD.'.'.COL_ISDELETED, 0)
    ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
    ->get_compiled_select(TBL_TRISIKO, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlStatus = '<span class="badge badge-secondary">BARU</span>';
      if($r[COL_STATUS]=='VERIFIKASI') $htmlStatus = '<span class="badge badge-danger">VERIFIKASI</span>';
      else if($r[COL_STATUS]=='FINAL') $htmlStatus = '<span class="badge badge-info">FINAL</span>';

      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/doc/edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/doc/detail/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary"><i class="fas fa-search"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/doc/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i></a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_OPDNAMA],
        $r[COL_TAHUN],
        $r[COL_KETERANGAN],
        $htmlStatus
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    if(!empty($_POST)) {
      $rrenstra = $this->db
      ->where(COL_UNIQ, $this->input->post(COL_IDRENSTRA))
      ->where(COL_ISDELETED, 0)
      ->get(TBL_TRENSTRA)
      ->row_array();
      if(empty($rrenstra)) {
        ShowJsonError('Dokumen Renstra tidak ditemukan!');
        exit();
      }

      $rpmdtujuan = $this->db->query("select * from tpmdtujuan where Uniq in (select distinct(IdTujuanPmd) from trenstratujuan where IdRenstra = ".$rrenstra[COL_UNIQ].")")->result_array();
      $rpmdsasaran = $this->db->query("select * from tpmdsasaran where Uniq in (select distinct(trenstrasasaran.IdSasaranPmd) from trenstrasasaran left join trenstratujuan on trenstratujuan.Uniq = trenstrasasaran.IdTujuan where trenstratujuan.IdRenstra = ".$rrenstra[COL_UNIQ].")")->result_array();
      $rpmdsasarandet = $this->db->query("select * from tpmdiku where IdSasaran in (select distinct(trenstrasasaran.IdSasaranPmd) from trenstrasasaran left join trenstratujuan on trenstratujuan.Uniq = trenstrasasaran.IdTujuan where trenstratujuan.IdRenstra = ".$rrenstra[COL_UNIQ].")")->result_array();

      $rtujuan = $this->db
      ->select('trenstratujuan.*, tpmdtujuan.Uraian as TujuanPmd')
      ->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDTUJUANPMD,"inner")
      ->where(COL_IDRENSTRA, $rrenstra[COL_UNIQ])
      ->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
      ->order_by(TBL_TPMDTUJUAN.'.'.COL_IDMISI)
      ->order_by(TBL_TPMDTUJUAN.'.'.COL_UNIQ)
      ->get(TBL_TRENSTRATUJUAN)
      ->result_array();

      $rsasaran = $this->db
      ->select('trenstrasasaran.*, tpmdsasaran.Uraian as SasaranPmd')
      ->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDSASARANPMD,"inner")
      ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
      ->where(TBL_TRENSTRATUJUAN.'.'.COL_IDRENSTRA, $rrenstra[COL_UNIQ])
      ->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRENSTRASASARAN)
      ->result_array();

      $riku = $this->db
      ->select('trenstrasasarandet.*')
      ->join(TBL_TRENSTRASASARAN,TBL_TRENSTRASASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARANDET.".".COL_IDSASARAN,"inner")
      ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
      ->where(TBL_TRENSTRATUJUAN.'.'.COL_IDRENSTRA, $rrenstra[COL_UNIQ])
      ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENSTRASASARANDET.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRENSTRASASARANDET)
      ->result_array();

      $rprogram = $this->db
      ->select('trenjaprogram.*')
      ->where(TBL_TRENJAPROGRAM.'.'.COL_IDRENSTRA, $rrenstra[COL_UNIQ])
      ->where(TBL_TRENJAPROGRAM.'.'.COL_TAHUN, $this->input->post(COL_TAHUN))
      ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRENJAPROGRAM)
      ->result_array();

      $rkegiatan = $this->db
      ->select('trenjakegiatan.*')
      ->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRENJAKEGIATAN.".".COL_IDPROGRAM,"inner")
      ->where(TBL_TRENJAPROGRAM.'.'.COL_IDRENSTRA, $rrenstra[COL_UNIQ])
      ->where(TBL_TRENJAPROGRAM.'.'.COL_TAHUN, $this->input->post(COL_TAHUN))
      ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRENJAKEGIATAN)
      ->result_array();

      $rkegiatansub = $this->db
      ->select('trenjakegiatansub.*')
      ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRENJAKEGIATANSUB.".".COL_IDKEGIATAN,"inner")
      ->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRENJAKEGIATAN.".".COL_IDPROGRAM,"inner")
      ->where(TBL_TRENJAPROGRAM.'.'.COL_IDRENSTRA, $rrenstra[COL_UNIQ])
      ->where(TBL_TRENJAPROGRAM.'.'.COL_TAHUN, $this->input->post(COL_TAHUN))
      ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRENJAKEGIATANSUB)
      ->result_array();

      if(empty($rtujuan) || empty($rsasaran) || empty($riku)) {
        ShowJsonError('Dokumen Renstra tidak lengkap!');
        exit();
      }

      if(empty($rprogram) || empty($rkegiatan) || empty($rkegiatansub)) {
        ShowJsonError('Dokumen Renja tidak lengkap!');
        exit();
      }

      $dat = array(
        COL_IDOPD=>$this->input->post(COL_IDOPD),
        COL_IDRENSTRA=>$this->input->post(COL_IDRENSTRA),
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_KETERANGAN=>$this->input->post(COL_KETERANGAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $datPmdTujuan = array();
      $datPmdSasaran = array();
      $datPmdIKU = array();
      $datTujuan = array();
      $datSasaran = array();
      $datIKU = array();
      $datProgram = array();
      $datKegiatan = array();
      $datKegiatanSub = array();
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRISIKO, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idRisiko = $this->db->insert_id();

        foreach($rpmdtujuan as $r) {
          $datPmdTujuan[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$r[COL_UNIQ],
            COL_LEVEL=>'PMDTUJUAN',
            COL_KONTEKSURAIAN=>$r[COL_URAIAN],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($rpmdsasaran as $r) {
          $datPmdSasaran[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$r[COL_UNIQ],
            COL_LEVEL=>'PMDSASARAN',
            COL_KONTEKSURAIAN=>$r[COL_URAIAN],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($rpmdsasarandet as $r) {
          $datPmdIKU[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$r[COL_UNIQ],
            COL_LEVEL=>'PMDIKU',
            COL_KONTEKSURAIAN=>$r[COL_URAIAN],
            COL_KONTEKSSATUAN=>$r[COL_SATUAN],
            COL_KONTEKSTARGET=>$r[COL_TARGET],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($rtujuan as $t) {
          $datTujuan[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$t[COL_UNIQ],
            COL_LEVEL=>'TUJUAN',
            COL_KONTEKSURAIAN=>$t[COL_URAIAN],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($rsasaran as $s) {
          $datSasaran[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$s[COL_UNIQ],
            COL_LEVEL=>'SASARAN',
            COL_KONTEKSURAIAN=>$s[COL_URAIAN],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($riku as $i) {
          $datIKU[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$i[COL_UNIQ],
            COL_LEVEL=>'IKU',
            COL_KONTEKSURAIAN=>$i[COL_URAIAN],
            COL_KONTEKSSATUAN=>$i[COL_SATUAN],
            COL_KONTEKSTARGET=>$i[COL_TARGET],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($rprogram as $r) {
          $datProgram[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$r[COL_UNIQ],
            COL_LEVEL=>'PROGRAM',
            COL_KONTEKSURAIAN=>$r[COL_PROGURAIAN],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($rkegiatan as $r) {
          $datKegiatan[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$r[COL_UNIQ],
            COL_LEVEL=>'KEGIATAN',
            COL_KONTEKSURAIAN=>$r[COL_KEGURAIAN],
            COL_KONTEKSINDIKATOR=>$r[COL_KEGINDIKATOR],
            COL_KONTEKSSATUAN=>$r[COL_KEGSATUAN],
            COL_KONTEKSTARGET=>$r[COL_KEGTARGET],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        foreach($rkegiatansub as $r) {
          $datKegiatanSub[] = array(
            COL_IDRISIKO=>$idRisiko,
            COL_IDREF=>$r[COL_UNIQ],
            COL_LEVEL=>'SUBKEGIATAN',
            COL_KONTEKSURAIAN=>$r[COL_SUBURAIAN],
            COL_KONTEKSINDIKATOR=>$r[COL_SUBINDIKATOR],
            COL_KONTEKSSATUAN=>$r[COL_SUBSATUAN],
            COL_KONTEKSTARGET=>$r[COL_SUBTARGET],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        if(!empty($datPmdTujuan)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datPmdTujuan);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datPmdSasaran)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datPmdSasaran);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datPmdIKU)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datPmdIKU);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datTujuan)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datTujuan);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datSasaran)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datSasaran);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datIKU)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datIKU);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datProgram)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datProgram);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datKegiatan)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datKegiatan);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($datKegiatanSub)) {
          $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datKegiatanSub);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('doc/form');
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->select("trisiko.*, mopd.OPDNama, trenstra.RenstraNama, tpmdperiode.PeriodNama, trenstra.IdPeriode")
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDOPD,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_MOPD.'.'.COL_ISDELETED, 0)
    ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_KETERANGAN=>$this->input->post(COL_KETERANGAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKO, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form', $data);
    }
  }

  public function detail($id) {
    $tab = $this->input->get('tab');
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->db->where(TBL_TRISIKO.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO]);
    }
    $rdata = $this->db
    ->select("
      trisiko.*,
      mopd.OPDNama,
      trenstra.RenstraNama,
      tpmdperiode.PeriodNama,
      stat_reg.StatClass as regLabel, stat_reg.StatIcon regIcon,
      stat_analysis.StatClass as analysisLabel, stat_analysis.StatIcon analysisIcon,
      stat_rtp.StatClass as rtpLabel, stat_rtp.StatIcon rtpIcon,
      stat_infokom.StatClass as infokomLabel, stat_infokom.StatIcon infokomIcon,
      stat_monitoring.StatClass as monitorLabel, stat_monitoring.StatIcon monitorIcon,
      stat_event.StatClass as eventLabel, stat_event.StatIcon eventIcon,
      ")
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDOPD,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->join("mstatus stat_reg","stat_reg.StatNama = trisiko.StatRegister","left")
    ->join("mstatus stat_analysis","stat_analysis.StatNama = trisiko.StatAnalisis","left")
    ->join("mstatus stat_rtp","stat_rtp.StatNama = trisiko.StatRTP","left")
    ->join("mstatus stat_infokom","stat_infokom.StatNama = trisiko.StatInfokom","left")
    ->join("mstatus stat_monitoring","stat_monitoring.StatNama = trisiko.StatInfokom","left")
    ->join("mstatus stat_event","stat_event.StatNama = trisiko.StatEvent","left")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_MOPD.'.'.COL_ISDELETED, 0)
    ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)
    ->row_array();

    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    $data['data'] = $rdata;
    switch ($tab) {
      case 'info':
      $this->load->view('doc/detail-info', $data);
      break;

      case 'konteks':
      $this->load->view('doc/detail-konteks', $data);
      break;

      case 'register':
      $this->load->view('doc/detail-register', $data);
      break;

      case 'analisis':
      $this->load->view('doc/detail-analisis', $data);
      break;

      case 'rtp':
      $this->load->view('doc/detail-rtp', $data);
      break;

      case 'infokom':
      $this->load->view('doc/detail-infokom', $data);
      break;

      case 'renmonitor':
      $this->load->view('doc/detail-renjamonitoring', $data);
      break;

      case 'monitoring':
      $this->load->view('doc/detail-monitoring', $data);
      break;

      default:
      $data['title'] = 'Dokumen Manajemen Risiko';
      $this->template->load('backend' , 'doc/detail', $data);
      break;
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->get(TBL_TRISIKO)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKO, array(COL_ISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError('Error: '.$err['message']);
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function konteks_add_program($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select("trisiko.*,trenstra.Uniq as IdRenstra")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_IDRENSTRA=>$rdata[COL_IDRENSTRA],
        COL_IDSASARANSKPD=>$this->input->post(COL_IDSASARANSKPD),
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_PROGKODE=>$this->input->post(COL_PROGKODE),
        COL_PROGURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENJAPROGRAM, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idRef = $this->db->insert_id();
        $datKonteks = array(
          COL_IDRISIKO=>$rdata[COL_UNIQ],
          COL_IDREF=>$idRef,
          COL_LEVEL=>'PROGRAM',
          COL_KONTEKSURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
        $res = $this->db->insert(TBL_TRISIKOKONTEKS, $datKonteks);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrisiko'] = $rdata;
      $this->load->view('doc/form-konteks-program', $data);
    }
  }

  public function konteks_add_operasional($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select("trisiko.*,trenstra.Uniq as IdRenstra")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $rprogram = $this->db
      ->where(COL_UNIQ, $this->input->post(COL_IDPROGRAM))
      ->where(COL_ISDELETED, 0)
      ->get(TBL_TRENJAPROGRAM)
      ->row_array();
      if(empty($rprogram)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $rkegiatan = $this->db
      ->select('trenjakegiatan.*')
      ->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRENJAKEGIATAN.".".COL_IDPROGRAM,"inner")
      ->where(TBL_TRENJAKEGIATAN.'.'.COL_IDPROGRAM, $this->input->post(COL_IDPROGRAM))
      ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRENJAKEGIATAN)
      ->result_array();
      if(empty($rkegiatan)) {
        ShowJsonError('Konteks gagal ditambahkan karena kegiatan tidak tersedia pada Dokumen Renja!');
        exit();
      }

      $rkegiatansub = $this->db
      ->select('trenjakegiatansub.*')
      ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRENJAKEGIATANSUB.".".COL_IDKEGIATAN,"inner")
      ->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRENJAKEGIATAN.".".COL_IDPROGRAM,"inner")
      ->where(TBL_TRENJAKEGIATAN.'.'.COL_IDPROGRAM, $this->input->post(COL_IDPROGRAM))
      ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
      ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRENJAKEGIATANSUB)
      ->result_array();
      if(empty($rkegiatansub)) {
        ShowJsonError('Konteks gagal ditambahkan karena sub kegiatan tidak tersedia pada Dokumen Renja!');
        exit();
      }

      $datKonteks = array();
      $datKonteks[] = array(
        COL_IDRISIKO=>$rdata[COL_UNIQ],
        COL_IDREF=>$rprogram[COL_UNIQ],
        COL_LEVEL=>'PROGRAM',
        COL_KONTEKSURAIAN=>$rprogram[COL_PROGURAIAN],
        COL_KONTEKSINDIKATOR=>null,
        COL_KONTEKSSATUAN=>null,
        COL_KONTEKSTARGET=>null,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      foreach($rkegiatan as $r) {
        $datKonteks[] = array(
          COL_IDRISIKO=>$rdata[COL_UNIQ],
          COL_IDREF=>$r[COL_UNIQ],
          COL_LEVEL=>'KEGIATAN',
          COL_KONTEKSURAIAN=>$r[COL_KEGURAIAN],
          COL_KONTEKSINDIKATOR=>$r[COL_KEGINDIKATOR],
          COL_KONTEKSSATUAN=>$r[COL_KEGSATUAN],
          COL_KONTEKSTARGET=>$r[COL_KEGTARGET],
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
      }
      foreach($rkegiatansub as $r) {
        $datKonteks[] = array(
          COL_IDRISIKO=>$rdata[COL_UNIQ],
          COL_IDREF=>$r[COL_UNIQ],
          COL_LEVEL=>'SUBKEGIATAN',
          COL_KONTEKSURAIAN=>$r[COL_SUBURAIAN],
          COL_KONTEKSINDIKATOR=>$r[COL_SUBINDIKATOR],
          COL_KONTEKSSATUAN=>$r[COL_SUBSATUAN],
          COL_KONTEKSTARGET=>$r[COL_SUBTARGET],
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
      }

      //echo json_encode($datKonteks);
      //exit();
      $this->db->trans_begin();
      try {
        $res = $this->db->insert_batch(TBL_TRISIKOKONTEKS, $datKonteks);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrisiko'] = $rdata;
      $this->load->view('doc/form-konteks-operasional', $data);
    }
  }

  public function konteks_edit_program($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select('trisikokonteks.*, trenjaprogram.ProgKode, trenjaprogram.IdSasaranSkpd, trenjaprogram.Tahun')
    ->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PROGRAM')
    ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    $rrisiko = $this->db
    ->select("trisiko.*,trenstra.Uniq as IdRenstra")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $rdata[COL_IDRISIKO])
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)->row_array();
    if(empty($rrisiko)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_IDSASARANSKPD=>$this->input->post(COL_IDSASARANSKPD),
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_PROGKODE=>$this->input->post(COL_PROGKODE),
        COL_PROGURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $datKonteks = array(
        COL_KONTEKSURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $rdata[COL_IDREF])->update(TBL_TRENJAPROGRAM, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOKONTEKS, $datKonteks);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrisiko'] = $rrisiko;
      $data['data'] = $rdata;
      $this->load->view('doc/form-konteks-program', $data);
    }
  }

  public function konteks_delete_program($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKOKONTEKS)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rreg = $this->db->where(COL_IDKONTEKS, $id)->get(TBL_TRISIKOREG)->row_array();
    if(!empty($rreg)) {
      ShowJsonError('Data tidak dapat dihapus dikarenakan data sudah digunakan pada register risiko.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $rdata[COL_IDREF])->update(TBL_TRENJAPROGRAM, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKOKONTEKS);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function konteks_add_kegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select("trisiko.*,trenjaprogram.Uniq as IdProgram")
    ->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
    ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_IDPROGRAM=>$rdata[COL_IDPROGRAM],
        COL_KEGKODE=>$this->input->post(COL_KEGKODE),
        COL_KEGURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_KEGINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
        COL_KEGSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
        COL_KEGTARGET=>$this->input->post(COL_KONTEKSTARGET),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENJAKEGIATAN, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idRef = $this->db->insert_id();
        $datKonteks = array(
          COL_IDRISIKO=>$rdata[COL_UNIQ],
          COL_IDREF=>$idRef,
          COL_LEVEL=>'KEGIATAN',
          COL_KONTEKSURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
          COL_KONTEKSINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
          COL_KONTEKSSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
          COL_KONTEKSTARGET=>$this->input->post(COL_KONTEKSTARGET),
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
        $res = $this->db->insert(TBL_TRISIKOKONTEKS, $datKonteks);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrisiko'] = $rdata;
      $this->load->view('doc/form-konteks-kegiatan', $data);
    }
  }

  public function konteks_edit_kegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select('trisikokonteks.*, trenjakegiatan.KegKode')
    ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'KEGIATAN')
    ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_KEGKODE=>$this->input->post(COL_KEGKODE),
        COL_KEGURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_KEGINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
        COL_KEGSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
        COL_KEGTARGET=>$this->input->post(COL_KONTEKSTARGET),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $datKonteks = array(
        COL_KONTEKSURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_KONTEKSINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
        COL_KONTEKSSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
        COL_KONTEKSTARGET=>$this->input->post(COL_KONTEKSTARGET),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $rdata[COL_IDREF])->update(TBL_TRENJAKEGIATAN, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOKONTEKS, $datKonteks);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-konteks-kegiatan', $data);
    }
  }

  public function konteks_delete_kegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKOKONTEKS)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rreg = $this->db->where(COL_IDKONTEKS, $id)->get(TBL_TRISIKOREG)->row_array();
    if(!empty($rreg)) {
      ShowJsonError('Data tidak dapat dihapus dikarenakan data sudah digunakan pada register risiko.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $rdata[COL_IDREF])->update(TBL_TRENJAKEGIATAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKOKONTEKS);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function konteks_add_subkegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select("trisiko.*,trenjakegiatan.Uniq as IdKegiatan")
    ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
    ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_IDKEGIATAN=>$rdata[COL_IDKEGIATAN],
        COL_SUBKODE=>$this->input->post(COL_SUBKODE),
        COL_SUBURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_SUBINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
        COL_SUBSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
        COL_SUBTARGET=>$this->input->post(COL_KONTEKSTARGET),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENJAKEGIATANSUB, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idRef = $this->db->insert_id();
        $datKonteks = array(
          COL_IDRISIKO=>$rdata[COL_UNIQ],
          COL_IDREF=>$idRef,
          COL_LEVEL=>'SUBKEGIATAN',
          COL_KONTEKSURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
          COL_KONTEKSINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
          COL_KONTEKSSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
          COL_KONTEKSTARGET=>$this->input->post(COL_KONTEKSTARGET),
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
        $res = $this->db->insert(TBL_TRISIKOKONTEKS, $datKonteks);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrisiko'] = $rdata;
      $this->load->view('doc/form-konteks-kegiatansub', $data);
    }
  }

  public function konteks_edit_subkegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select('trisikokonteks.*, trenjakegiatansub.SubKode')
    ->join(TBL_TRENJAKEGIATANSUB,TBL_TRENJAKEGIATANSUB.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SUBKEGIATAN')
    ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_SUBKODE=>$this->input->post(COL_SUBKODE),
        COL_SUBURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_SUBINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
        COL_SUBSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
        COL_SUBTARGET=>$this->input->post(COL_KONTEKSTARGET),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $datKonteks = array(
        COL_KONTEKSURAIAN=>$this->input->post(COL_KONTEKSURAIAN),
        COL_KONTEKSINDIKATOR=>$this->input->post(COL_KONTEKSINDIKATOR),
        COL_KONTEKSSATUAN=>$this->input->post(COL_KONTEKSSATUAN),
        COL_KONTEKSTARGET=>$this->input->post(COL_KONTEKSTARGET),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $rdata[COL_IDREF])->update(TBL_TRENJAKEGIATANSUB, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOKONTEKS, $datKonteks);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-konteks-kegiatansub', $data);
    }
  }

  public function konteks_delete_subkegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKOKONTEKS)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rreg = $this->db->where(COL_IDKONTEKS, $id)->get(TBL_TRISIKOREG)->row_array();
    if(!empty($rreg)) {
      ShowJsonError('Data tidak dapat dihapus dikarenakan data sudah digunakan pada register risiko.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $rdata[COL_IDREF])->update(TBL_TRENJAKEGIATANSUB, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKOKONTEKS);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function register_add($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
    ->where(TBL_TRISIKOKONTEKS.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDKONTEKS=>$id,
        COL_RISIKOKODE=>$this->input->post(COL_RISIKOKODE),
        COL_RISIKOURAIAN=>$this->input->post(COL_RISIKOURAIAN),
        COL_RISIKOPEMILIK=>$this->input->post(COL_RISIKOPEMILIK),
        COL_SEBABURAIAN=>$this->input->post(COL_SEBABURAIAN),
        COL_SEBABSUMBER=>$this->input->post(COL_SEBABSUMBER),
        COL_CONTROL=>$this->input->post(COL_CONTROL),
        COL_DAMPAKURAIAN=>$this->input->post(COL_DAMPAKURAIAN),
        COL_DAMPAKPIHAK=>$this->input->post(COL_DAMPAKPIHAK),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRISIKOREG, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rkonteks'] = $rdata;
      $this->load->view('doc/form-register', $data);
    }
  }

  public function register_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRISIKOREG.'.'.COL_UNIQ, $id)
    ->get(TBL_TRISIKOREG)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_RISIKOKODE=>$this->input->post(COL_RISIKOKODE),
        COL_RISIKOURAIAN=>$this->input->post(COL_RISIKOURAIAN),
        COL_RISIKOPEMILIK=>$this->input->post(COL_RISIKOPEMILIK),
        COL_SEBABURAIAN=>$this->input->post(COL_SEBABURAIAN),
        COL_SEBABSUMBER=>$this->input->post(COL_SEBABSUMBER),
        COL_CONTROL=>$this->input->post(COL_CONTROL),
        COL_DAMPAKURAIAN=>$this->input->post(COL_DAMPAKURAIAN),
        COL_DAMPAKPIHAK=>$this->input->post(COL_DAMPAKPIHAK),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOREG, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-register', $data);
    }
  }

  public function register_delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKOREG)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rreg = $this->db->where(COL_IDRISIKOREG, $id)->get(TBL_TRISIKOLOG)->row_array();
    if(!empty($rreg)) {
      ShowJsonError('Data tidak dapat dihapus dikarenakan data sudah memiliki data monitoring.');
      exit();
    }
    $rreg = $this->db->where(COL_IDRISIKOREG, $id)->get(TBL_TRISIKORTP)->row_array();
    if(!empty($rreg)) {
      ShowJsonError('Data tidak dapat dihapus dikarenakan data sudah memiliki data RTP.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKOREG);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function register_edit_analisis($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRISIKOREG.'.'.COL_UNIQ, $id)
    ->get(TBL_TRISIKOREG)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_NUMDAMPAK=>toNum($this->input->post(COL_NUMDAMPAK)),
        COL_NUMFREQ=>toNum($this->input->post(COL_NUMFREQ)),
        COL_NUMRISIKO=>toNum($this->input->post(COL_NUMDAMPAK))*toNum($this->input->post(COL_NUMFREQ)),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOREG, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-analisis', $data);
    }
  }

  public function register_add_rtp($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select('trisikoreg.*')
    ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
    ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
    ->where(TBL_TRISIKOREG.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOREG)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDRISIKOREG=>$id,
        COL_URAIANPENGENDALIAN=>$this->input->post(COL_URAIANPENGENDALIAN),
        COL_URAIANCELAH=>$this->input->post(COL_URAIANCELAH),
        COL_URAIANRTP=>$this->input->post(COL_URAIANRTP),
        COL_RTPPENANGGUNGJAWAB=>$this->input->post(COL_RTPPENANGGUNGJAWAB),
        COL_RTPWAKTU=>$this->input->post(COL_RTPWAKTU),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRISIKORTP, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rkonteks'] = $rdata;
      $this->load->view('doc/form-rtp', $data);
    }
  }

  public function register_edit_rtp($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRISIKORTP.'.'.COL_UNIQ, $id)
    ->get(TBL_TRISIKORTP)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_URAIANPENGENDALIAN=>$this->input->post(COL_URAIANPENGENDALIAN),
        COL_URAIANCELAH=>$this->input->post(COL_URAIANCELAH),
        COL_URAIANRTP=>$this->input->post(COL_URAIANRTP),
        COL_RTPPENANGGUNGJAWAB=>$this->input->post(COL_RTPPENANGGUNGJAWAB),
        COL_RTPWAKTU=>$this->input->post(COL_RTPWAKTU),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKORTP, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-rtp', $data);
    }
  }

  public function register_delete_rtp($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKORTP)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKORTP);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function register_add_log($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select('trisikoreg.*')
    ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
    ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
    ->where(TBL_TRISIKOREG.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKOREG)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDRISIKOREG=>$id,
        COL_LOGTANGGAL=>date('Y-m-d', strtotime($this->input->post(COL_LOGTANGGAL))),
        COL_LOGSEBAB=>$this->input->post(COL_LOGSEBAB),
        COL_LOGDAMPAK=>$this->input->post(COL_LOGDAMPAK),
        COL_LOGKETERANGAN=>$this->input->post(COL_LOGKETERANGAN),
        COL_LOGRTP=>$this->input->post(COL_LOGRTP),
        COL_LOGRTPWAKTU=>$this->input->post(COL_LOGRTPWAKTU),
        COL_LOGRTPREALISASI=>$this->input->post(COL_LOGRTPREALISASI),
        COL_LOGRTPKETERANGAN=>$this->input->post(COL_LOGRTPKETERANGAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRISIKOLOG, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rkonteks'] = $rdata;
      $this->load->view('doc/form-log', $data);
    }
  }

  public function register_edit_log($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRISIKOLOG.'.'.COL_UNIQ, $id)
    ->get(TBL_TRISIKOLOG)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_LOGTANGGAL=>date('Y-m-d', strtotime($this->input->post(COL_LOGTANGGAL))),
        COL_LOGSEBAB=>$this->input->post(COL_LOGSEBAB),
        COL_LOGDAMPAK=>$this->input->post(COL_LOGDAMPAK),
        COL_LOGKETERANGAN=>$this->input->post(COL_LOGKETERANGAN),
        COL_LOGRTP=>$this->input->post(COL_LOGRTP),
        COL_LOGRTPWAKTU=>$this->input->post(COL_LOGRTPWAKTU),
        COL_LOGRTPREALISASI=>$this->input->post(COL_LOGRTPREALISASI),
        COL_LOGRTPKETERANGAN=>$this->input->post(COL_LOGRTPKETERANGAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOLOG, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-log', $data);
    }
  }

  public function register_delete_log($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKOLOG)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKOLOG);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function infokom_add($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select("trisiko.*,trenstra.Uniq as IdRenstra")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datInfokom = array(
        COL_IDRISIKO=>$id,
        COL_KOMKEGIATAN=>$this->input->post(COL_KOMKEGIATAN),
        COL_KOMMEDIA=>$this->input->post(COL_KOMMEDIA),
        COL_KOMPENYEDIA=>$this->input->post(COL_KOMPENYEDIA),
        COL_KOMPENERIMA=>$this->input->post(COL_KOMPENERIMA),
        COL_KOMWAKTU=>$this->input->post(COL_KOMWAKTU),
        COL_KOMREALISASI=>$this->input->post(COL_KOMREALISASI),
        COL_KOMKETERANGAN=>$this->input->post(COL_KOMKETERANGAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRISIKOKOM, $datInfokom);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrisiko'] = $rdata;
      $this->load->view('doc/form-infokom', $data);
    }
  }

  public function infokom_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRISIKOKOM.'.'.COL_UNIQ, $id)
    ->get(TBL_TRISIKOKOM)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_KOMKEGIATAN=>$this->input->post(COL_KOMKEGIATAN),
        COL_KOMMEDIA=>$this->input->post(COL_KOMMEDIA),
        COL_KOMPENYEDIA=>$this->input->post(COL_KOMPENYEDIA),
        COL_KOMPENERIMA=>$this->input->post(COL_KOMPENERIMA),
        COL_KOMWAKTU=>$this->input->post(COL_KOMWAKTU),
        COL_KOMREALISASI=>$this->input->post(COL_KOMREALISASI),
        COL_KOMKETERANGAN=>$this->input->post(COL_KOMKETERANGAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOKOM, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-infokom', $data);
    }
  }

  public function infokom_delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKOKOM)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKOKOM);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function monitoring_add($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->select("trisiko.*,trenstra.Uniq as IdRenstra")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDRISIKO=>$id,
        COL_MONKEGIATAN=>$this->input->post(COL_MONKEGIATAN),
        COL_MONMETODE=>$this->input->post(COL_MONMETODE),
        COL_MONPIC=>$this->input->post(COL_MONPIC),
        COL_MONWAKTU=>$this->input->post(COL_MONWAKTU),
        COL_MONREALISASI=>$this->input->post(COL_MONREALISASI),
        COL_MONKETERANGAN=>$this->input->post(COL_MONKETERANGAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRISIKOMONITORING, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrisiko'] = $rdata;
      $this->load->view('doc/form-monitoring', $data);
    }
  }

  public function monitoring_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRISIKOMONITORING.'.'.COL_UNIQ, $id)
    ->get(TBL_TRISIKOMONITORING)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_MONKEGIATAN=>$this->input->post(COL_MONKEGIATAN),
        COL_MONMETODE=>$this->input->post(COL_MONMETODE),
        COL_MONPIC=>$this->input->post(COL_MONPIC),
        COL_MONWAKTU=>$this->input->post(COL_MONWAKTU),
        COL_MONREALISASI=>$this->input->post(COL_MONREALISASI),
        COL_MONKETERANGAN=>$this->input->post(COL_MONKETERANGAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKOMONITORING, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('doc/form-monitoring', $data);
    }
  }

  public function monitoring_delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRISIKOMONITORING)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TRISIKOMONITORING);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function cetak($page,$id) {
    $rdata = $this->db
    ->select("trisiko.*,trenstra.Uniq as IdRenstra,mopd.OPDNama,mopd.OPDPimpinan,mopd.OPDPimpinanNIP,mopd.OPDPimpinanJab")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDOPD,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    $title = '';
    if($page=='konteks') $title = 'Konteks Risiko';
    else if($page=='register') $title = 'Register Risiko';
    else if($page=='analisis') $title = 'Analisis Risiko';
    else if($page=='rtp') $title = 'RTP Risiko';
    else if($page=='infokom') $title = 'Infokom';
    else if($page=='monitoring') $title = 'Rencana Monitoring';
    else if($page=='log') $title = 'Risk Event';

    $data['data'] = $rdata;

    $this->load->library('Mypdf');
    $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

    $html = $this->load->view('site/doc/cetak-'.$page, $data, TRUE);
    //echo $html;
    //exit();
    $htmlLogo = MY_IMAGEPATH.'logo.png';
    $htmlTitle = strtoupper($this->setting_web_desc);
    $htmlHeader = @"
    <table style=\"border: none !important\">
      <tr>
        <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: middle\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
      </tr>
      <tr>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">PEMERINTAH KOTA TEBING TINGGI</td>
      </tr>
    </table>
    <hr />
    ";
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle($this->setting_web_name.' - '.$title);
    $mpdf->pdf->SetHTMLHeader($htmlHeader);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('RMIS - '.$htmlTitle.'.pdf', 'I');
    //$this->load->view('doc/cetak-monitoring', $data);
  }

  public function comment($group, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rfeeds = array();
    $idDoc = 0;
    if($group=='register'|| $group=='analysis') {
      $rdet = $this->db
      ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
      ->where(TBL_TRISIKOREG.'.'.COL_UNIQ, $id)
      ->get(TBL_TRISIKOREG)->row_array();
      if(empty($rdet)) {
        echo 'Parameter tidak valid.';
        exit();
      }
      $idDoc = $rdet[COL_IDRISIKO];

    } else if($group=='rtp') {
      $rdet = $this->db
      ->join(TBL_TRISIKOREG,TBL_TRISIKOREG.'.'.COL_UNIQ." = ".TBL_TRISIKORTP.".".COL_IDRISIKOREG,"inner")
      ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
      ->where(TBL_TRISIKORTP.'.'.COL_UNIQ, $id)
      ->get(TBL_TRISIKORTP)->row_array();
      if(empty($rdet)) {
        echo 'Parameter tidak valid.';
        exit();
      }
      $idDoc = $rdet[COL_IDRISIKO];

    } else if($group=='infokom') {
      $rdet = $this->db
      ->where(TBL_TRISIKOKOM.'.'.COL_UNIQ, $id)
      ->get(TBL_TRISIKOKOM)->row_array();
      if(empty($rdet)) {
        echo 'Parameter tidak valid.';
        exit();
      }
      $idDoc = $rdet[COL_IDRISIKO];

    } else if($group=='monitoring') {
      $rdet = $this->db
      ->where(TBL_TRISIKOMONITORING.'.'.COL_UNIQ, $id)
      ->get(TBL_TRISIKOMONITORING)->row_array();
      if(empty($rdet)) {
        echo 'Parameter tidak valid.';
        exit();
      }
      $idDoc = $rdet[COL_IDRISIKO];

    } else if($group=='log') {
      $rdet = $this->db
      ->join(TBL_TRISIKOREG,TBL_TRISIKOREG.'.'.COL_UNIQ." = ".TBL_TRISIKOLOG.".".COL_IDRISIKOREG,"inner")
      ->where(TBL_TRISIKOLOG.'.'.COL_UNIQ, $id)
      ->get(TBL_TRISIKOLOG)->row_array();
      if(empty($rdet)) {
        echo 'Parameter tidak valid.';
        exit();
      }
      $idDoc = $rdet[COL_IDRISIKO];

    }

    $rfeeds = $this->db
    ->select('trisikofeeds.*, _userinformation.Name')
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TRISIKOFEEDS.".".COL_CREATEDBY,"inner")
    ->where(COL_FEEDGROUP, strtoupper($group))
    ->where(COL_FEEDTYPE, 'COMMENT')
    ->where(COL_FEEDREF, $id)
    ->order_by(COL_CREATEDON,'asc')
    ->get(TBL_TRISIKOFEEDS)
    ->result_array();

    $rdata = $this->db
    ->select("trisiko.*, mopd.OPDNama, trenstra.RenstraNama, tpmdperiode.PeriodNama, trenstra.IdPeriode")
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDOPD,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $idDoc)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_MOPD.'.'.COL_ISDELETED, 0)
    ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_FEEDGROUP=>strtoupper($group),
        COL_FEEDTYPE=>'COMMENT',
        COL_FEEDREF=>$id,
        COL_FEEDTEXT=>$this->input->post(COL_FEEDTEXT),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRISIKOFEEDS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah feedback / komentar!');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $res = $this->db
      ->where(COL_FEEDGROUP, strtoupper($group))
      ->where(COL_FEEDTYPE, 'COMMENT')
      ->where(COL_FEEDREF, $id)
      ->where(COL_FEEDISREAD, 0)
      ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
      ->update(TBL_TRISIKOFEEDS, array(COL_FEEDISREAD=>1));

      $data['data'] = $rdata;
      $data['feeds'] = $rfeeds;
      $this->load->view('doc/feeds', $data);
    }
  }

  public function status($col, $id, $stat=null) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->select("trisiko.*, mopd.OPDNama, trenstra.RenstraNama, tpmdperiode.PeriodNama, trenstra.IdPeriode")
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDOPD,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->where(TBL_TRISIKO.'.'.COL_UNIQ, $id)
    ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_MOPD.'.'.COL_ISDELETED, 0)
    ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $feedgrp = 'REGISTER';
    if($col==COL_STATANALISIS) $feedgrp='ANALYSIS';
    else if($col==COL_STATRTP) $feedgrp='RTP';
    else if($col==COL_STATINFOKOM) $feedgrp='INFOKOM';
    else if($col==COL_STATMONITORING) $feedgrp='MONITORING';
    else if($col==COL_STATEVENT) $feedgrp='EVENT';

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRISIKO, array($col=>$stat));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $res = $this->db->insert(TBL_TRISIKOFEEDS, array(COL_FEEDGROUP=>$feedgrp, COL_FEEDTYPE=>'APPROVAL', COL_FEEDREF=>$id, COL_CREATEDBY=>$ruser[COL_USERNAME], COL_CREATEDON=>date('Y-m-d H:i:s')));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil mengubah status!');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }
}
