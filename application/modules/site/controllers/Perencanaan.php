<?php
class Perencanaan extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }
  }

  public function opt_pmdsasaran_by_renstratujuan($id, $selected=null) {
    echo GetCombobox("select tpmdsasaran.Uraian, tpmdsasaran.Uniq from tpmdsasaran left join tpmdtujuan on tpmdtujuan.Uniq = tpmdsasaran.IdTujuan left join tpmdmisi on tpmdmisi.Uniq = tpmdtujuan.IdMisi where tpmdsasaran.IdTujuan = $id and tpmdsasaran.IsDeleted=0 and tpmdtujuan.IsDeleted=0 order by tpmdtujuan.IdMisi, tpmdtujuan.Uniq, tpmdsasaran.Uniq asc", COL_UNIQ, COL_URAIAN, (!empty($selected)?$selected:null));
  }

  public function opt_renstra_by_rpjmd() {
    $idPeriod = $this->input->post('IdPeriode');
    $idOPD = $this->input->post('IdOPD');
    echo GetCombobox("select * from trenstra where trenstra.IdPeriode=$idPeriod and trenstra.IdOPD=$idOPD and trenstra.IsDeleted=0 order by trenstra.Tahun desc", COL_UNIQ, COL_RENSTRANAMA);
  }

  public function rpjmd() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = "Periode RPJMD";
    $data['res'] = $this->db
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_PERIODISAKTIF, 'desc')
    ->order_by(COL_PERIODFROM, 'desc')
    ->get(TBL_TPMDPERIODE)
    ->result_array();
    $this->template->load('backend' , 'perencanaan/rpjmd', $data);
  }

  public function rpjmd_add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    if(!empty($_POST)) {
      $dat = array(
        COL_PERIODNAMA=>$this->input->post(COL_PERIODNAMA),
        COL_PERIODFROM=>$this->input->post(COL_PERIODNAMA),
        COL_PERIODTO=>$this->input->post(COL_PERIODNAMA),
        COL_PERIODKEPALADAERAH=>$this->input->post(COL_PERIODNAMA),
        COL_VISI=>$this->input->post(COL_PERIODNAMA),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPMDPERIODE, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('perencanaan/rpjmd-form');
    }
  }

  public function rpjmd_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPMDPERIODE)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_PERIODNAMA=>$this->input->post(COL_PERIODNAMA),
        COL_PERIODFROM=>$this->input->post(COL_PERIODFROM),
        COL_PERIODTO=>$this->input->post(COL_PERIODTO),
        COL_PERIODKEPALADAERAH=>$this->input->post(COL_PERIODKEPALADAERAH),
        COL_VISI=>$this->input->post(COL_VISI),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDPERIODE, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/rpjmd-form', $data);
    }
  }

  public function rpjmd_delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPMDPERIODE)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDPERIODE, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function rpjmd_sync($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPMDPERIODE)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    // Load db esakip
    $dbSAKIP = $this->load->database('esakip', TRUE);

    if(!empty($_POST)) {
      if(empty($dbSAKIP)) {
        ShowJsonError('Maaf, koneksi database gagal. Silakan hubungi Administrator.');
        exit();
      }

      $idSync = $this->input->post('SyncId');
      $rrpjmd = $dbSAKIP
      ->where('PmdId', $idSync)
      ->get('sakipv2_pemda')
      ->row_array();
      if(empty($rrpjmd)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $rmisi = $dbSAKIP
      ->where('IdPmd', $idSync)
      ->order_by('MisiNo', 'asc')
      ->get('sakipv2_pemda_misi')
      ->result_array();
      if(empty($rmisi)) {
        ShowJsonError('Misi tidak ditemukan (kosong)!');
        exit();
      }

      $this->db->trans_begin();
      try {
        /* Delete Existing */
        $rmisi_ = $this->db
        ->where(COL_IDPERIODE, $id)
        ->get(TBL_TPMDMISI)
        ->result_array();
        foreach($rmisi_ as $m) {
          $res = $this->db->where(COL_UNIQ, $m[COL_UNIQ])->update(TBL_TPMDMISI, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
          if(!$res) {
            throw new Exception('Gagal mengosongkan data (Misi)!');
          }

          $rtujuan = $this->db
          ->where(COL_IDMISI, $m[COL_UNIQ])
          ->get(TBL_TPMDTUJUAN)
          ->result_array();
          foreach($rtujuan as $t) {
            $res = $this->db->where(COL_UNIQ, $t[COL_UNIQ])->update(TBL_TPMDTUJUAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
            if(!$res) {
              throw new Exception('Gagal mengosongkan data (Tujuan)!');
            }

            $rsasaran = $this->db
            ->where(COL_IDTUJUAN, $t[COL_UNIQ])
            ->get(TBL_TPMDSASARAN)
            ->result_array();
            foreach($rsasaran as $s) {
              $res = $this->db->where(COL_UNIQ, $s[COL_UNIQ])->update(TBL_TPMDSASARAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
              if(!$res) {
                throw new Exception('Gagal mengosongkan data (Sasaran)!');
              }

              $riku = $this->db
              ->where(COL_IDSASARAN, $s[COL_UNIQ])
              ->get(TBL_TPMDIKU)
              ->result_array();
              foreach($riku as $iku) {
                $res = $this->db->where(COL_UNIQ, $iku[COL_UNIQ])->update(TBL_TPMDIKU, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
                if(!$res) {
                  throw new Exception('Gagal mengosongkan data (IKU)!');
                }
              }
            }
          }
        }
        /* Delete Existing */
        foreach($rmisi as $m) {
          $datMisi = array(
            COL_IDPERIODE=>$id,
            COL_URAIAN=>$m['MisiUraian'],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
          $res = $this->db->insert(TBL_TPMDMISI, $datMisi);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $idMisi = $this->db->insert_id();
          $rtujuan = $dbSAKIP
          ->where('IdMisi', $m['MisiId'])
          ->order_by('TujuanNo', 'asc')
          ->get('sakipv2_pemda_tujuan')
          ->result_array();
          if(empty($rtujuan)) {
            throw new Exception('Tujuan tidak ditemukan (kosong)!');
          }

          foreach($rtujuan as $t) {
            $datTujuan = array(
              COL_IDMISI=>$idMisi,
              COL_URAIAN=>$t['TujuanUraian'],
              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
            $res = $this->db->insert(TBL_TPMDTUJUAN, $datTujuan);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }

            $idTujuan = $this->db->insert_id();
            $rsasaran = $dbSAKIP
            ->where('IdTujuan', $t['TujuanId'])
            ->order_by('SasaranNo', 'asc')
            ->get('sakipv2_pemda_sasaran')
            ->result_array();
            if(empty($rsasaran)) {
              throw new Exception('Sasaran tidak ditemukan (kosong)!');
            }

            foreach($rsasaran as $s) {
              $datSasaran = array(
                COL_IDTUJUAN=>$idTujuan,
                COL_URAIAN=>$s['SasaranUraian'],
                COL_CREATEDBY=>$ruser[COL_USERNAME],
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );
              $res = $this->db->insert(TBL_TPMDSASARAN, $datSasaran);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception('Error: '.$err['message']);
              }

              $idSasaran = $this->db->insert_id();
              $riku = $dbSAKIP
              ->where('IdSasaran', $s['SasaranId'])
              ->order_by('SsrIndikatorId', 'asc')
              ->get('sakipv2_pemda_sasarandet')
              ->result_array();
              if(empty($riku)) {
                throw new Exception('IKU tidak ditemukan (kosong)!');
              }

              foreach($riku as $iku) {
                $datIKU = array(
                  COL_IDSASARAN=>$idSasaran,
                  COL_URAIAN=>$iku['SsrIndikatorUraian'],
                  COL_TARGET=>$iku['SsrIndikatorTarget'],
                  COL_SATUAN=>$iku['SsrIndikatorSatuan'],
                  COL_CREATEDBY=>$ruser[COL_USERNAME],
                  COL_CREATEDON=>date('Y-m-d H:i:s')
                );
                $res = $this->db->insert(TBL_TPMDIKU, $datIKU);
                if(!$res) {
                  $err = $this->db->error();
                  throw new Exception('Error: '.$err['message']);
                }
              }
            }
          }


        }
        $this->db->trans_commit();
        ShowJsonSuccess('Sinkronisasi berhasil!');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      if(empty($dbSAKIP)) {
        echo 'Maaf, koneksi database gagal. Silakan hubungi Administrator.';
        exit();
      }

      $data['data'] = $rdata;
      $data['res'] = $dbSAKIP
      ->where('PmdTahunMulai', $rdata[COL_PERIODFROM])
      ->where('PmdTahunAkhir', $rdata[COL_PERIODTO])
      ->where('PmdIsAktif', 1)
      ->order_by('PmdTahunMulai', 'desc')
      ->get('sakipv2_pemda')
      ->row_array();

      $this->load->view('perencanaan/rpjmd-sync', $data);
    }
  }

  public function rpjmd_det($id) {
    $page = $this->input->get('page');
    if(empty($page)) {
      show_404();
      exit();
    }

    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDPERIODE)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $data['data'] = $rdata;
    $data['page'] = $page;
    $data['title'] = 'Rincian RPJMD';
    if($page=='misi') $data['title'] = 'Daftar Misi RPJMD';
    else if($page=='tujuan') $data['title'] = 'Daftar Tujuan RPJMD';
    else if($page=='sasaran') $data['title'] = 'Daftar Sasaran RPJMD';
    else if($page=='iku') $data['title'] = 'Daftar IKU RPJMD';

    $this->template->load('backend', 'perencanaan/rpjmd-det', $data);
  }

  public function rpjmd_add_misi($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDPERIODE)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDPERIODE=>$id,
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPMDMISI, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('perencanaan/rpjmd-det-form');
    }
  }

  public function rpjmd_edit_misi($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDMISI)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDMISI, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/rpjmd-det-form', $data);
    }
  }

  public function rpjmd_delete_misi($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPMDMISI)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDMISI, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function rpjmd_add_tujuan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDMISI)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDMISI=>$id,
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPMDTUJUAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('perencanaan/rpjmd-det-form');
    }
  }

  public function rpjmd_edit_tujuan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDTUJUAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDTUJUAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/rpjmd-det-form', $data);
    }
  }

  public function rpjmd_delete_tujuan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPMDTUJUAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDTUJUAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function rpjmd_add_sasaran($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDTUJUAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDTUJUAN=>$id,
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPMDSASARAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('perencanaan/rpjmd-det-form');
    }
  }

  public function rpjmd_edit_sasaran($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDSASARAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDSASARAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/rpjmd-det-form', $data);
    }
  }

  public function rpjmd_delete_sasaran($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPMDSASARAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDSASARAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function rpjmd_add_iku($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDSASARAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDSASARAN=>$id,
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_TARGET=>$this->input->post(COL_TARGET),
        COL_SATUAN=>$this->input->post(COL_SATUAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPMDIKU, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['isIKU'] = 1;
      $this->load->view('perencanaan/rpjmd-det-form', $data);
    }
  }

  public function rpjmd_edit_iku($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TPMDIKU)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_TARGET=>$this->input->post(COL_TARGET),
        COL_SATUAN=>$this->input->post(COL_SATUAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDIKU, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $data['isIKU'] = 1;
      $this->load->view('perencanaan/rpjmd-det-form', $data);
    }
  }

  public function rpjmd_delete_iku($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPMDIKU)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPMDIKU, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function renstra() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = "Dokumen Renstra";
    $this->template->load('backend' , 'perencanaan/renstra', $data);
  }

  public function renstra_load() {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterPeriod = !empty($_POST['filterPeriod'])?$_POST['filterPeriod']:null;
    $filterOPD = !empty($_POST['filterOPD'])?$_POST['filterOPD']:null;

    $ruser = GetLoggedUser();
    $orderables = array(null,COL_OPDNAMA,COL_TAHUN,COL_RENSTRANAMA);
    $cols = array(COL_OPDNAMA,COL_TAHUN,COL_RENSTRANAMA,COL_RENSTRAKETERANGAN);

    $queryAll = $this->db->where(COL_ISDELETED, 0)->get(TBL_TRENSTRA);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($filterPeriod)) {
      $this->db->where(TBL_TRENSTRA.'.'.COL_IDPERIODE, $filterPeriod);
    }
    if(!empty($filterOPD)) {
      $this->db->where(TBL_TRENSTRA.'.'.COL_IDOPD, $filterOPD);
    }
    if($ruser[COL_ROLEID]==ROLEUSER) {
      $this->db->where(TBL_TRENSTRA.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO]);
    }

    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      $this->db->order_by($order, $_POST['order']['0']['dir']);

    } else {
      $this->db->order_by(TBL_MOPD.'.'.COL_OPDNAMA, 'asc');
      $this->db->order_by(TBL_TRENSTRA.'.'.COL_RENSTRANAMA, 'asc');
    }

    $q = $this->db
    ->select("trenstra.*, mopd.OPDNama")
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDOPD,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_MOPD.'.'.COL_ISDELETED, 0)
    ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
    ->get_compiled_select(TBL_TRENSTRA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/perencanaan/renstra-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/perencanaan/renstra-detail/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary"><i class="fas fa-search"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/perencanaan/renstra-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i></a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_OPDNAMA],
        $r[COL_TAHUN],
        $r[COL_RENSTRANAMA],
        $r[COL_RENSTRAKETERANGAN]
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function renstra_add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDOPD=>$this->input->post(COL_IDOPD),
        COL_IDPERIODE=>$this->input->post(COL_IDPERIODE),
        COL_RENSTRANAMA=>$this->input->post(COL_RENSTRANAMA),
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_RENSTRAKETERANGAN=>$this->input->post(COL_RENSTRAKETERANGAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENSTRA, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('perencanaan/renstra-form');
    }
  }

  public function renstra_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TRENSTRA)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_RENSTRANAMA=>$this->input->post(COL_RENSTRANAMA),
        COL_RENSTRAKETERANGAN=>$this->input->post(COL_RENSTRAKETERANGAN),
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRA, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/renstra-form', $data);
    }
  }

  public function renstra_delete($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRENSTRA)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRA, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function renstra_detail($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TRENSTRA)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $data['title'] = $rdata[COL_RENSTRANAMA];
    $data['data'] = $rdata;
    $this->template->load('backend' , 'perencanaan/renstra-detail', $data);
  }

  public function renstra_add_tujuan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TRENSTRA)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDRENSTRA=>$id,
        COL_IDTUJUANPMD=>$this->input->post(COL_IDTUJUANPMD),
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENSTRATUJUAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $rperiode = $this->db
      ->where(COL_UNIQ, $rdata[COL_IDPERIODE])
      ->where(COL_ISDELETED, 0)
      ->get(TBL_TPMDPERIODE)
      ->row_array();
      if(empty($rperiode)) {
        show_error('Parameter tidak valid.');
        exit();
      }

      $data['idPeriode'] = $rperiode[COL_UNIQ];
      $this->load->view('perencanaan/renstra-form-tujuan', $data);
    }
  }

  public function renstra_edit_tujuan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->select('trenstratujuan.*, trenstra.IdPeriode')
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRENSTRATUJUAN.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENSTRATUJUAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDTUJUANPMD=>$this->input->post(COL_IDTUJUANPMD),
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRATUJUAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['idPeriode'] = $rdata[COL_IDPERIODE];
      $data['data'] = $rdata;
      $this->load->view('perencanaan/renstra-form-tujuan', $data);
    }
  }

  public function renstra_delete_tujuan($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRENSTRATUJUAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRATUJUAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function renstra_add_sasaran($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rrenstra = $this->db
    ->where(COL_UNIQ, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TRENSTRA)
    ->row_array();
    if(empty($rrenstra)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDTUJUAN=>$this->input->post(COL_IDTUJUAN),
        COL_IDSASARANPMD=>$this->input->post(COL_IDSASARANPMD),
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENSTRASASARAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.', array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$id).'?tab=sasaran'));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $rperiode = $this->db
      ->where(COL_UNIQ, $rrenstra[COL_IDPERIODE])
      ->where(COL_ISDELETED, 0)
      ->get(TBL_TPMDPERIODE)
      ->row_array();
      if(empty($rperiode)) {
        show_error('Parameter tidak valid.');
        exit();
      }

      $data['idPeriode'] = $rperiode[COL_UNIQ];
      $data['idRenstra'] = $rrenstra[COL_UNIQ];
      $this->load->view('perencanaan/renstra-form-sasaran', $data);
    }
  }

  public function renstra_edit_sasaran($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db
    ->select('trenstrasasaran.*, trenstra.IdPeriode, trenstra.Uniq as IdRenstra')
    ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRENSTRASASARAN.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENSTRASASARAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDTUJUAN=>$this->input->post(COL_IDTUJUAN),
        COL_IDSASARANPMD=>$this->input->post(COL_IDSASARANPMD),
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRASASARAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.',array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$rdata[COL_IDRENSTRA]).'?tab=sasaran'));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['idPeriode'] = $rdata[COL_IDPERIODE];
      $data['idRenstra'] = $rdata[COL_IDRENSTRA];
      $data['data'] = $rdata;
      $this->load->view('perencanaan/renstra-form-sasaran', $data);
    }
  }

  public function renstra_delete_sasaran($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/

    $rdata = $this->db
    ->select('trenstrasasaran.*, trenstra.IdPeriode, trenstra.Uniq as IdRenstra')
    ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRENSTRASASARAN.'.'.COL_UNIQ, $id)
    ->get(TBL_TRENSTRASASARAN)
    ->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRASASARAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.',array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$rdata[COL_IDRENSTRA]).'?tab=sasaran'));
    exit();
  }

  public function renstra_add_iku($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/

    $rdata = $this->db
    ->select('trenstrasasaran.*, trenstra.IdPeriode, trenstra.Uniq as IdRenstra')
    ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRENSTRASASARAN.'.'.COL_UNIQ, $id)
    ->get(TBL_TRENSTRASASARAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDSASARAN=>$id,
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_TARGET=>$this->input->post(COL_TARGET),
        COL_SATUAN=>$this->input->post(COL_SATUAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENSTRASASARANDET, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.', array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$rdata[COL_IDRENSTRA]).'?tab=sasaran'));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('perencanaan/renstra-form-iku');
    }
  }

  public function renstra_edit_iku($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/
    $rdata = $this->db
    ->select('trenstrasasarandet.*, trenstra.IdPeriode, trenstra.Uniq as IdRenstra')
    ->join(TBL_TRENSTRASASARAN,TBL_TRENSTRASASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARANDET.".".COL_IDSASARAN,"inner")
    ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRENSTRASASARANDET.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
    ->where(TBL_TRENSTRASASARANDET.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENSTRASASARANDET)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_URAIAN=>$this->input->post(COL_URAIAN),
        COL_TARGET=>$this->input->post(COL_TARGET),
        COL_SATUAN=>$this->input->post(COL_SATUAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRASASARANDET, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.', array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$rdata[COL_IDRENSTRA]).'?tab=sasaran'));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/renstra-form-iku', $data);
    }
  }

  public function renstra_delete_iku($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/

    $rdata = $this->db
    ->select('trenstrasasarandet.*, trenstra.IdPeriode, trenstra.Uniq as IdRenstra')
    ->join(TBL_TRENSTRASASARAN,TBL_TRENSTRASASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARANDET.".".COL_IDSASARAN,"inner")
    ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
    ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDRENSTRA,"inner")
    ->where(TBL_TRENSTRASASARANDET.'.'.COL_UNIQ, $id)
    ->get(TBL_TRENSTRASASARANDET)
    ->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENSTRASASARANDET, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.', array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$rdata[COL_IDRENSTRA]).'?tab=sasaran'));
    exit();
  }

  public function renstra_sync($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/

    $rdata = $this->db
    ->select('trenstra.*, mopd.OPDRef, tpmdperiode.PeriodFrom, tpmdperiode.PeriodTo')
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDOPD,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->where(TBL_TRENSTRA.'.'.COL_UNIQ, $id)
    ->get(TBL_TRENSTRA)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rrisiko = $this->db
    ->where(COL_IDRENSTRA, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)
    ->row_array();

    // Load db esakip
    $dbSAKIP = $this->load->database('esakip', TRUE);

    if(!empty($_POST)) {
      if(!empty($rrisiko)) {
        ShowJsonError('Maaf, sinkronisasi gagal dikarenakan dokumen Renstra saat ini sudah dipakai sebagai sumber dokumen Manajemen Risiko.');
        exit();
      }

      if(empty($dbSAKIP)) {
        ShowJsonError('Maaf, koneksi database gagal. Silakan hubungi Administrator.');
        exit();
      }

      $idSync = $this->input->post('SyncId');
      $rrenstra = $dbSAKIP
      ->where('RenstraId', $idSync)
      ->get('sakipv2_skpd_renstra')
      ->row_array();
      if(empty($rrenstra)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $rtujuan = $dbSAKIP->query("
      select
        sakipv2_skpd_renstra_tujuan.*,
        sakipv2_pemda_tujuan.TujuanUraian as TujuanPmd
      from sakipv2_skpd_renstra_tujuan
      left join sakipv2_pemda_tujuan on sakipv2_pemda_tujuan.TujuanId = sakipv2_skpd_renstra_tujuan.IdTujuanPmd
      where IdRenstra = $idSync
      order by sakipv2_skpd_renstra_tujuan.TujuanNo
      ")->result_array();

      $rsasaran = $dbSAKIP->query("
      select
        sakipv2_skpd_renstra_sasaran.*,
        sakipv2_skpd_renstra_tujuan.*,
        sakipv2_pemda_sasaran.SasaranUraian as SasaranPmd
      from sakipv2_skpd_renstra_sasaran
      left join sakipv2_pemda_sasaran on sakipv2_pemda_sasaran.SasaranId = sakipv2_skpd_renstra_sasaran.IdSasaranPmd
      left join sakipv2_skpd_renstra_tujuan on sakipv2_skpd_renstra_tujuan.TujuanId = sakipv2_skpd_renstra_sasaran.IdTujuan
      where
        sakipv2_skpd_renstra_tujuan.IdRenstra = $idSync
      order by
        sakipv2_skpd_renstra_tujuan.TujuanNo,
        sakipv2_skpd_renstra_sasaran.SasaranNo
      ")->result_array();
      $riku = array();
      if(!empty($rrenstra['RenstraIKU'])) {
        $arrIKU = json_decode($rrenstra['RenstraIKU']);
        foreach($arrIKU as $iku) {
          $riku[] = array(COL_URAIAN=>$iku->Uraian, COL_TARGET=>$iku->Target, COL_SATUAN=>$iku->Satuan);
        }
      }

      $this->db->trans_begin();
      try {
        /* Delete Existing */
        $rtujuan_ = $this->db
        ->where(COL_IDRENSTRA, $id)
        ->get(TBL_TRENSTRATUJUAN)
        ->result_array();
        foreach($rtujuan_ as $t) {
          $res = $this->db->where(COL_UNIQ, $t[COL_UNIQ])->update(TBL_TRENSTRATUJUAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
          if(!$res) {
            throw new Exception('Gagal mengosongkan data (Misi)!');
          }

          $rsasaran_ = $this->db
          ->where(COL_IDTUJUAN, $t[COL_UNIQ])
          ->get(TBL_TRENSTRASASARAN)
          ->result_array();
          foreach($rsasaran_ as $s) {
            $res = $this->db->where(COL_UNIQ, $s[COL_UNIQ])->update(TBL_TRENSTRASASARAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
            if(!$res) {
              throw new Exception('Gagal mengosongkan data (Sasaran)!');
            }

            $riku_ = $this->db
            ->where(COL_IDSASARAN, $s[COL_UNIQ])
            ->get(TBL_TRENSTRASASARANDET)
            ->result_array();
            foreach($riku_ as $iku) {
              $res = $this->db->where(COL_UNIQ, $iku[COL_UNIQ])->update(TBL_TRENSTRASASARANDET, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
              if(!$res) {
                throw new Exception('Gagal mengosongkan data (IKU)!');
              }
            }
          }
        }
        /* Delete Existing */

        foreach($rtujuan as $t) {
          $rtujuanPmd = $this->db
          ->select(TBL_TPMDTUJUAN.'.*')
          ->join(TBL_TPMDMISI,TBL_TPMDMISI.'.'.COL_UNIQ." = ".TBL_TPMDTUJUAN.".".COL_IDMISI,"inner")
          ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TPMDMISI.".".COL_IDPERIODE,"inner")
          ->where(TBL_TPMDPERIODE.'.'.COL_UNIQ, $rdata[COL_IDPERIODE])
          ->where(TBL_TPMDTUJUAN.'.'.COL_URAIAN, $t['TujuanPmd'])
          ->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
          ->where(TBL_TPMDMISI.'.'.COL_ISDELETED, 0)
          ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
          ->get(TBL_TPMDTUJUAN)
          ->row_array();
          if(empty($rtujuanPmd)) {
            throw new Exception('Gagal mencocokkan Tujuan RPJMD : '.$t['TujuanPmd'].'! '.$this->db->last_query());
          }

          $datTujuan = array(
            COL_IDRENSTRA=>$id,
            COL_IDTUJUANPMD=>$rtujuanPmd[COL_UNIQ],
            COL_URAIAN=>$t['TujuanUraian'],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
          $res = $this->db->insert(TBL_TRENSTRATUJUAN, $datTujuan);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $idTujuan = $this->db->insert_id();
          $idTujuan_ = $t['TujuanId'];
          $rsasaran = $dbSAKIP->query("
          select
            sakipv2_skpd_renstra_sasaran.*,
            sakipv2_skpd_renstra_tujuan.*,
            sakipv2_pemda_sasaran.SasaranUraian as SasaranPmd
          from sakipv2_skpd_renstra_sasaran
          left join sakipv2_pemda_sasaran on sakipv2_pemda_sasaran.SasaranId = sakipv2_skpd_renstra_sasaran.IdSasaranPmd
          left join sakipv2_skpd_renstra_tujuan on sakipv2_skpd_renstra_tujuan.TujuanId = sakipv2_skpd_renstra_sasaran.IdTujuan
          where
            sakipv2_skpd_renstra_tujuan.IdRenstra = $idSync
            and sakipv2_skpd_renstra_sasaran.IdTujuan = $idTujuan_
          order by
            sakipv2_skpd_renstra_tujuan.TujuanNo,
            sakipv2_skpd_renstra_sasaran.SasaranNo
          ")->result_array();
          /*$rsasaran = $dbSAKIP
          ->where('IdTujuan', $t['TujuanId'])
          ->order_by('SasaranNo', 'asc')
          ->get('sakipv2_skpd_renstra_sasaran')
          ->result_array();*/
          if(empty($rsasaran)) {
            throw new Exception('Sasaran tidak ditemukan (kosong)!');
          }

          foreach($rsasaran as $s) {
            $rsasaranPmd = $this->db
            ->select(TBL_TPMDSASARAN.'.*')
            ->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TPMDSASARAN.".".COL_IDTUJUAN,"inner")
            ->join(TBL_TPMDMISI,TBL_TPMDMISI.'.'.COL_UNIQ." = ".TBL_TPMDTUJUAN.".".COL_IDMISI,"inner")
            ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TPMDMISI.".".COL_IDPERIODE,"inner")
            ->where(TBL_TPMDPERIODE.'.'.COL_UNIQ, $rdata[COL_IDPERIODE])
            ->where(TBL_TPMDSASARAN.'.'.COL_URAIAN, $s['SasaranPmd'])
            ->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
            ->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
            ->where(TBL_TPMDMISI.'.'.COL_ISDELETED, 0)
            ->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
            ->get(TBL_TPMDSASARAN)
            ->row_array();
            if(empty($rsasaranPmd)) {
              throw new Exception('Gagal mencocokkan Sasaran RPJMD :'.$s['SasaranPmd'].' !');
            }

            $datSasaran = array(
              COL_IDTUJUAN=>$idTujuan,
              COL_IDSASARANPMD=>$rsasaranPmd[COL_UNIQ],
              COL_URAIAN=>$s['SasaranUraian'],
              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
            $res = $this->db->insert(TBL_TRENSTRASASARAN, $datSasaran);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }

            $idSasaran = $this->db->insert_id();
            $riku = $dbSAKIP
            ->where('IdSasaran', $s['SasaranId'])
            ->order_by('SsrIndikatorId', 'asc')
            ->get('sakipv2_skpd_renstra_sasarandet')
            ->result_array();
            if(empty($riku)) {
              throw new Exception('IKU tidak ditemukan (kosong)!');
            }

            foreach($riku as $iku) {
              $datIKU = array(
                COL_IDSASARAN=>$idSasaran,
                COL_URAIAN=>$iku['SsrIndikatorUraian'],
                COL_TARGET=>$iku['SsrIndikatorTarget'],
                COL_SATUAN=>$iku['SsrIndikatorSatuan'],
                COL_CREATEDBY=>$ruser[COL_USERNAME],
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );
              $res = $this->db->insert(TBL_TRENSTRASASARANDET, $datIKU);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception('Error: '.$err['message']);
              }
            }
          }
        }
        $this->db->trans_commit();
        ShowJsonSuccess('Sinkronisasi berhasil!');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      if(empty($dbSAKIP)) {
        echo 'Maaf, koneksi database gagal. Silakan hubungi Administrator.';
        exit();
      }

      $rtujuan = array();
      $rsasaran = array();
      $riku = array();
      $data['data'] = $rdata;
      $data['res'] = $rrenstra = $dbSAKIP
      ->join('sakipv2_pemda','sakipv2_pemda.PmdId=sakipv2_skpd_renstra.IdPemda',"inner")
      ->join('sakipv2_skpd','sakipv2_skpd.SkpdId=sakipv2_skpd_renstra.IdSkpd',"inner")
      ->where('sakipv2_pemda.PmdTahunMulai', $rdata[COL_PERIODFROM])
      ->where('sakipv2_pemda.PmdTahunAkhir', $rdata[COL_PERIODTO])
      ->where('sakipv2_pemda.PmdIsAktif', 1)
      ->where('sakipv2_skpd_renstra.RenstraIsAktif', 1)
      ->where('sakipv2_skpd_renstra.RenstraTahun', $rdata[COL_TAHUN])
      ->where('CONCAT(sakipv2_skpd.SkpdUrusan,\'.\',sakipv2_skpd.SkpdBidang,\'.\',sakipv2_skpd.SkpdUnit,\'.\',sakipv2_skpd.SkpdSubUnit)', $rdata[COL_OPDREF])
      ->get('sakipv2_skpd_renstra')
      ->row_array();
      if(!empty($rrenstra)) {
        $idRenstra = $rrenstra['RenstraId'];
        $rtujuan = $dbSAKIP->query("select * from sakipv2_skpd_renstra_tujuan where IdRenstra = $idRenstra order by sakipv2_skpd_renstra_tujuan.TujuanNo")->result_array();
        $rsasaran = $dbSAKIP->query("select * from sakipv2_skpd_renstra_sasaran left join sakipv2_skpd_renstra_tujuan on sakipv2_skpd_renstra_tujuan.TujuanId = sakipv2_skpd_renstra_sasaran.IdTujuan where sakipv2_skpd_renstra_tujuan.IdRenstra = $idRenstra order by sakipv2_skpd_renstra_tujuan.TujuanNo, sakipv2_skpd_renstra_sasaran.SasaranNo")->result_array();

        $arrIKU = array();
        if(!empty($rrenstra['RenstraIKU'])) {
          $arrIKU = json_decode($rrenstra['RenstraIKU']);
          foreach($arrIKU as $iku) {
            $riku[] = array(COL_URAIAN=>$iku->Uraian, COL_TARGET=>$iku->Target, COL_SATUAN=>$iku->Satuan);
          }
        }
      }

      $data['rtujuan'] = $rtujuan;
      $data['rsasaran'] = $rsasaran;
      $data['riku'] = $riku;
      $this->load->view('perencanaan/renstra-sync', $data);
    }
  }

  public function renstra_sync_renja($id) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }*/

    $rdata = $this->db
    ->select('trenstra.*, mopd.OPDRef, tpmdperiode.PeriodFrom, tpmdperiode.PeriodTo')
    ->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDOPD,"inner")
    ->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
    ->where(TBL_TRENSTRA.'.'.COL_UNIQ, $id)
    ->get(TBL_TRENSTRA)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rrisiko = $this->db
    ->where(COL_IDRENSTRA, $id)
    ->where(COL_ISDELETED, 0)
    ->get(TBL_TRISIKO)
    ->row_array();

    // Load db esakip
    $dbSAKIP = $this->load->database('esakip', TRUE);

    if(!empty($_POST)) {
      if(!empty($rrisiko)) {
        ShowJsonError('Maaf, sinkronisasi gagal dikarenakan dokumen Renstra saat ini sudah dipakai sebagai sumber dokumen Manajemen Risiko.');
        exit();
      }

      if(empty($dbSAKIP)) {
        ShowJsonError('Maaf, koneksi database gagal. Silakan hubungi Administrator.');
        exit();
      }

      $idSync = $this->input->post('SyncId');
      $rdpa = $dbSAKIP
      ->where('DPAId', $idSync)
      ->get('sakipv2_skpd_renstra_dpa')
      ->row_array();
      if(empty($rdpa)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $rprogram = $dbSAKIP
      ->select('sakipv2_bid_program.ProgramKode,sakipv2_bid_program.ProgramUraian, sakipv2_skpd_renstra_sasaran.SasaranUraian as PmdSasaranUraian')
      ->join('sakipv2_skpd_renstra_sasaran','sakipv2_skpd_renstra_sasaran.SasaranId=sakipv2_bid_program.IdSasaranSkpd',"inner")
      ->where('sakipv2_bid_program.IdDPA', $idSync)
      ->group_by(array('sakipv2_bid_program.ProgramKode'/*,'sakipv2_skpd_renstra_sasaran.SasaranUraian'*/))
      ->get('sakipv2_bid_program')
      ->result_array();

      $this->db->trans_begin();
      try {
        /* Delete Existing */
        $rprogram_ = $this->db
        ->where(COL_IDRENSTRA, $id)
        ->where(COL_TAHUN, $rdpa['DPATahun'])
        ->get(TBL_TRENJAPROGRAM)
        ->result_array();
        foreach($rprogram_ as $p) {
          $res = $this->db->where(COL_UNIQ, $p[COL_UNIQ])->update(TBL_TRENJAPROGRAM, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
          if(!$res) {
            throw new Exception('Gagal mengosongkan data (Program)!');
          }

          $rkegiatan_ = $this->db
          ->where(COL_IDPROGRAM, $p[COL_UNIQ])
          ->get(TBL_TRENJAKEGIATAN)
          ->result_array();
          foreach($rkegiatan_ as $k) {
            $res = $this->db->where(COL_UNIQ, $k[COL_UNIQ])->update(TBL_TRENJAKEGIATAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
            if(!$res) {
              throw new Exception('Gagal mengosongkan data (Kegiatan)!');
            }

            $rsubkegiatan_ = $this->db
            ->where(COL_IDKEGIATAN, $k[COL_UNIQ])
            ->get(TBL_TRENJAKEGIATANSUB)
            ->result_array();
            foreach($rsubkegiatan_ as $sub) {
              $res = $this->db->where(COL_UNIQ, $sub[COL_UNIQ])->update(TBL_TRENJAKEGIATANSUB, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
              if(!$res) {
                throw new Exception('Gagal mengosongkan data (Sub Kegiatan)!');
              }
            }
          }
        }
        /* Delete Existing */

        foreach($rprogram as $prog) {
          $rrenstrasasaran = $this->db
          ->select('trenstrasasaran.*')
          ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
          ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDRENSTRA,"inner")
          ->where(TBL_TRENSTRA.'.'.COL_UNIQ, $id)
          ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
          ->where(TBL_TRENSTRASASARAN.'.'.COL_URAIAN, $prog['PmdSasaranUraian'])
          ->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
          ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
          ->get(TBL_TRENSTRASASARAN)
          ->row_array();
          if(empty($rrenstrasasaran)) {
            ShowJsonError('Gagal mencocokkan Sasaran (Renstra)!');
            exit();
          }

          $rkegiatan = $dbSAKIP
          ->select('sakipv2_bid_kegiatan.KegiatanId,sakipv2_bid_kegiatan.KegiatanKode,sakipv2_bid_kegiatan.KegiatanUraian')
          ->join('sakipv2_bid_program','sakipv2_bid_program.ProgramId=sakipv2_bid_kegiatan.IdProgram',"inner")
          ->where('sakipv2_bid_program.IdDPA', $idSync)
          ->where('sakipv2_bid_program.ProgramKode', $prog['ProgramKode'])
          ->group_by('sakipv2_bid_kegiatan.KegiatanKode')
          ->get('sakipv2_bid_kegiatan')
          ->result_array();

          $datProgram = array(
            COL_IDRENSTRA=>$id,
            COL_IDSASARANSKPD=>$rrenstrasasaran[COL_UNIQ],
            COL_TAHUN=>$rdpa['DPATahun'],
            COL_PROGKODE=>$prog['ProgramKode'],
            COL_PROGURAIAN=>$prog['ProgramUraian'],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_TRENJAPROGRAM, $datProgram);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $idProgram = $this->db->insert_id();
          foreach($rkegiatan as $keg) {
            $rkegsasaran = $dbSAKIP
            ->select('sakipv2_bid_kegsasaran.SasaranUraian,sakipv2_bid_kegsasaran.SasaranIndikator,sakipv2_bid_kegsasaran.SasaranSatuan,sakipv2_bid_kegsasaran.SasaranTarget')
            ->where('sakipv2_bid_kegsasaran.IdKegiatan', $keg['KegiatanId'])
            ->get('sakipv2_bid_kegsasaran')
            ->row_array();

            $rsubkeg = $dbSAKIP
            ->select('sakipv2_subbid_subkegiatan.SubkegId,sakipv2_subbid_subkegiatan.SubkegKode,sakipv2_subbid_subkegiatan.SubkegUraian')
            ->join('sakipv2_bid_kegiatan','sakipv2_bid_kegiatan.KegiatanId=sakipv2_subbid_subkegiatan.IdKegiatan',"inner")
            ->join('sakipv2_bid_program','sakipv2_bid_program.ProgramId=sakipv2_bid_kegiatan.IdProgram',"inner")
            ->where('sakipv2_bid_program.IdDPA', $idSync)
            ->where('sakipv2_bid_kegiatan.KegiatanKode', $keg['KegiatanKode'])
            ->group_by('sakipv2_subbid_subkegiatan.SubkegKode')
            ->get('sakipv2_subbid_subkegiatan')
            ->result_array();

            $datKegiatan = array(
              COL_IDPROGRAM=>$idProgram,
              COL_KEGKODE=>$keg['KegiatanKode'],
              COL_KEGURAIAN=>$keg['KegiatanUraian'],
              COL_KEGINDIKATOR=>!empty($rkegsasaran)?$rkegsasaran['SasaranIndikator']:null,
              COL_KEGSATUAN=>!empty($rkegsasaran)?$rkegsasaran['SasaranSatuan']:null,
              COL_KEGTARGET=>!empty($rkegsasaran)?$rkegsasaran['SasaranTarget']:null,
              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_TRENJAKEGIATAN, $datKegiatan);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }

            $idKegiatan = $this->db->insert_id();
            foreach($rsubkeg as $sub) {
              $rsubkegsasaran = $dbSAKIP
              ->select('sakipv2_subbid_subkegsasaran.SasaranUraian,sakipv2_subbid_subkegsasaran.SasaranIndikator,sakipv2_subbid_subkegsasaran.SasaranSatuan,sakipv2_subbid_subkegsasaran.SasaranTarget')
              ->where('sakipv2_subbid_subkegsasaran.IdSubkeg', $sub['SubkegId'])
              ->get('sakipv2_subbid_subkegsasaran')
              ->row_array();

              $datSubKegiatan = array(
                COL_IDKEGIATAN=>$idKegiatan,
                COL_SUBKODE=>$sub['SubkegKode'],
                COL_SUBURAIAN=>$sub['SubkegUraian'],
                COL_SUBINDIKATOR=>!empty($rsubkegsasaran)?$rsubkegsasaran['SasaranIndikator']:null,
                COL_SUBSATUAN=>!empty($rsubkegsasaran)?$rsubkegsasaran['SasaranSatuan']:null,
                COL_SUBTARGET=>!empty($rsubkegsasaran)?$rsubkegsasaran['SasaranTarget']:null,
                COL_CREATEDBY=>$ruser[COL_USERNAME],
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );

              $res = $this->db->insert(TBL_TRENJAKEGIATANSUB, $datSubKegiatan);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception('Error: '.$err['message']);
              }
            }
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Sinkronisasi berhasil!', array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$id).'?tab=renja'));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      if(empty($dbSAKIP)) {
        echo 'Maaf, koneksi database gagal. Silakan hubungi Administrator.';
        exit();
      }

      $data['data'] = $rdata;
      $data['res'] = $rrenstra = $dbSAKIP
      ->join('sakipv2_skpd_renstra','sakipv2_skpd_renstra.RenstraId=sakipv2_skpd_renstra_dpa.IdRenstra',"inner")
      ->join('sakipv2_pemda','sakipv2_pemda.PmdId=sakipv2_skpd_renstra.IdPemda',"inner")
      ->join('sakipv2_skpd','sakipv2_skpd.SkpdId=sakipv2_skpd_renstra.IdSkpd',"inner")
      ->where('sakipv2_pemda.PmdTahunMulai', $rdata[COL_PERIODFROM])
      ->where('sakipv2_pemda.PmdTahunAkhir', $rdata[COL_PERIODTO])
      ->where('sakipv2_pemda.PmdIsAktif', 1)
      ->where('sakipv2_skpd_renstra.RenstraIsAktif', 1)
      ->where('sakipv2_skpd_renstra.RenstraTahun', $rdata[COL_TAHUN])
      ->where('sakipv2_skpd_renstra_dpa.DPAIsAktif', 1)
      ->where('CONCAT(sakipv2_skpd.SkpdUrusan,\'.\',sakipv2_skpd.SkpdBidang,\'.\',sakipv2_skpd.SkpdUnit,\'.\',sakipv2_skpd.SkpdSubUnit)', $rdata[COL_OPDREF])
      ->order_by('sakipv2_skpd_renstra_dpa.DPATahun', 'desc')
      ->get('sakipv2_skpd_renstra_dpa')
      ->result_array();

      $this->load->view('perencanaan/renstra-sync-renja', $data);
    }
  }

  public function renja_add_program($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRENSTRA.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENSTRA)->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_IDRENSTRA=>$id,
        COL_IDSASARANSKPD=>$this->input->post(COL_IDSASARANSKPD),
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_PROGKODE=>$this->input->post(COL_PROGKODE),
        COL_PROGURAIAN=>$this->input->post(COL_PROGURAIAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENJAPROGRAM, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.', array('redirect'=>site_url('site/perencanaan/renstra-detail/'.$rdata[COL_UNIQ]).'?tab=renja'));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrenstra'] = $rdata;
      $this->load->view('perencanaan/renja-form-program', $data);
    }
  }

  public function renja_edit_program($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRENJAPROGRAM.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENJAPROGRAM)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    $rrenstra = $this->db
    ->where(TBL_TRENSTRA.'.'.COL_UNIQ, $rdata[COL_IDRENSTRA])
    ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENSTRA)->row_array();
    if(empty($rrenstra)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    $rkonteks = $this->db
    ->where(COL_IDREF, $id)
    ->where(COL_LEVEL, 'PROGRAM')
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();

    if(!empty($_POST)) {
      if(!empty($rkonteks)) {
        ShowJsonError('Maaf, data tidak dapat diubah karena sudah digunakan sebagai Konteks Risiko');
        exit();
      }

      $datRenja = array(
        COL_IDSASARANSKPD=>$this->input->post(COL_IDSASARANSKPD),
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_PROGKODE=>$this->input->post(COL_PROGKODE),
        COL_PROGURAIAN=>$this->input->post(COL_PROGURAIAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENJAPROGRAM, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rrenstra'] = $rrenstra;
      $data['data'] = $rdata;
      $this->load->view('perencanaan/renja-form-program', $data);
    }
  }

  public function renja_delete_program($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRENJAPROGRAM)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rkonteks = $this->db
    ->where(COL_IDREF, $id)
    ->where(COL_LEVEL, 'PROGRAM')
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();

    if(!empty($rkonteks)) {
      ShowJsonError('Maaf, data tidak dapat diubah karena sudah digunakan sebagai Konteks Risiko');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENJAPROGRAM, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function renja_add_kegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRENJAPROGRAM.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENJAPROGRAM)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_IDPROGRAM=>$id,
        COL_KEGKODE=>$this->input->post(COL_KEGKODE),
        COL_KEGURAIAN=>$this->input->post(COL_KEGURAIAN),
        COL_KEGTUJUAN=>$this->input->post(COL_KEGTUJUAN),
        COL_KEGINDIKATOR=>$this->input->post(COL_KEGINDIKATOR),
        COL_KEGSATUAN=>$this->input->post(COL_KEGSATUAN),
        COL_KEGTARGET=>$this->input->post(COL_KEGTARGET),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENJAKEGIATAN, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rprogram'] = $rdata;
      $this->load->view('perencanaan/renja-form-kegiatan', $data);
    }
  }

  public function renja_edit_kegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRENJAKEGIATAN.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENJAKEGIATAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_KEGKODE=>$this->input->post(COL_KEGKODE),
        COL_KEGURAIAN=>$this->input->post(COL_KEGURAIAN),
        COL_KEGTUJUAN=>$this->input->post(COL_KEGTUJUAN),
        COL_KEGINDIKATOR=>$this->input->post(COL_KEGINDIKATOR),
        COL_KEGSATUAN=>$this->input->post(COL_KEGSATUAN),
        COL_KEGTARGET=>$this->input->post(COL_KEGTARGET),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENJAKEGIATAN, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/renja-form-kegiatan', $data);
    }
  }

  public function renja_delete_kegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRENJAKEGIATAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rkonteks = $this->db
    ->where(COL_IDREF, $id)
    ->where(COL_LEVEL, 'KEGIATAN')
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();

    if(!empty($rkonteks)) {
      ShowJsonError('Maaf, data tidak dapat diubah karena sudah digunakan sebagai Konteks Risiko');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENJAKEGIATAN, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function renja_add_subkegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRENJAKEGIATAN.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENJAKEGIATAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_IDKEGIATAN=>$id,
        COL_SUBKODE=>$this->input->post(COL_SUBKODE),
        COL_SUBURAIAN=>$this->input->post(COL_SUBURAIAN),
        COL_SUBTUJUAN=>$this->input->post(COL_SUBTUJUAN),
        COL_SUBINDIKATOR=>$this->input->post(COL_SUBINDIKATOR),
        COL_SUBSATUAN=>$this->input->post(COL_SUBSATUAN),
        COL_SUBTARGET=>$this->input->post(COL_SUBTARGET),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRENJAKEGIATANSUB, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['rkegiatan'] = $rdata;
      $this->load->view('perencanaan/renja-form-kegiatansub', $data);
    }
  }

  public function renja_edit_subkegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db
    ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_UNIQ, $id)
    ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
    ->get(TBL_TRENJAKEGIATANSUB)
    ->row_array();
    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $datRenja = array(
        COL_SUBKODE=>$this->input->post(COL_SUBKODE),
        COL_SUBURAIAN=>$this->input->post(COL_SUBURAIAN),
        COL_SUBTUJUAN=>$this->input->post(COL_SUBTUJUAN),
        COL_SUBINDIKATOR=>$this->input->post(COL_SUBINDIKATOR),
        COL_SUBSATUAN=>$this->input->post(COL_SUBSATUAN),
        COL_SUBTARGET=>$this->input->post(COL_SUBTARGET),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENJAKEGIATANSUB, $datRenja);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('perencanaan/renja-form-kegiatansub', $data);
    }
  }

  public function renja_delete_subkegiatan($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      echo 'Maaf, anda tidak memiliki hak akses.';
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TRENJAKEGIATANSUB)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $rkonteks = $this->db
    ->where(COL_IDREF, $id)
    ->where(COL_LEVEL, 'SUBKEGIATAN')
    ->get(TBL_TRISIKOKONTEKS)
    ->row_array();

    if(!empty($rkonteks)) {
      ShowJsonError('Maaf, data tidak dapat diubah karena sudah digunakan sebagai Konteks Risiko');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRENJAKEGIATANSUB, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil menghapus data.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }
}
