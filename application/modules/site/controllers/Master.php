<?php
class Master extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
  }

  public function opd() {
    $data['title'] = "OPD";
    $data['res'] = $this->db
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_OPDNAMA, 'asc')
    ->get(TBL_MOPD)
    ->result_array();
    $this->template->load('backend' , 'master/opd', $data);
  }

  public function opd_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_OPDREF=>$this->input->post(COL_OPDREF),
        COL_OPDNAMA=>$this->input->post(COL_OPDNAMA),
        COL_OPDPIMPINAN=>$this->input->post(COL_OPDPIMPINAN),
        COL_OPDPIMPINANNIP=>$this->input->post(COL_OPDPIMPINANNIP),
        COL_OPDPIMPINANJAB=>$this->input->post(COL_OPDPIMPINANJAB),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MOPD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('master/opd-form');
    }
  }

  public function opd_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MOPD)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_OPDREF=>$this->input->post(COL_OPDREF),
        COL_OPDNAMA=>$this->input->post(COL_OPDNAMA),
        COL_OPDPIMPINAN=>$this->input->post(COL_OPDPIMPINAN),
        COL_OPDPIMPINANNIP=>$this->input->post(COL_OPDPIMPINANNIP),
        COL_OPDPIMPINANJAB=>$this->input->post(COL_OPDPIMPINANJAB),
        COL_UPDATEDON=>$ruser[COL_USERNAME],
        COL_UPDATEDBY=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MOPD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('master/opd-form', $data);
    }
  }

  public function opd_delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MOPD)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MOPD, array(COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s'), COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }
}
