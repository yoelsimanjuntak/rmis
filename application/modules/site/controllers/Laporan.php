<?php
class Laporan extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }
  }

  public function risiko_pemda() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = "Rekapitulasi Risiko Pemerintah Daerah";
    $this->template->load('backend' , 'laporan/risiko-pemda', $data);
  }

  public function test() {
    echo 'Hello';
  }

  public function risiko_opd() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = "Rekapitulasi Risiko Perangkat Daerah";
    $this->template->load('backend' , 'laporan/risiko-opd', $data);
  }
}
