<?php
if(!empty($res)) {
  ?>
  <form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
    <div class="form-group">
      <label>Pilih DPA:</label>
      <select class="form-control" name="SyncId" style="width: 100%" required>
        <?php
        foreach($res as $opt) {
          ?>
          <option value="<?=$opt['DPAId']?>"><?=$opt['DPATahun'].' - '.$opt['DPAUraian']?></option>
          <?php
        }
        ?>
      </select>
    </div>
  </form>
  <div class="callout callout-danger">
    <p class="font-italic mb-0">Apakah anda yakin ingin melanjutkan sinkronisasi data?<br /><span class="font-weight-bold text-danger">proses sinkronisasi akan menghapus data yang sudah dientri sebelumnya (jika ada).</span></p>
  </div>
  <?php
} else {
  ?>
  <h4 class="mb-4">
    <span class="badge badge-danger">MAAF, DATA RENJA TIDAK DITEMUKAN!</span>
  </h4>
  <?php
}
?>
