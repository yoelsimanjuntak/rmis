<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Tujuan RPJMD</label>
    <select class="form-control" name="<?=COL_IDTUJUANPMD?>" style="width: 100%" required>
      <?=GetCombobox("select tpmdtujuan.Uraian, tpmdtujuan.Uniq from tpmdtujuan left join tpmdmisi on tpmdmisi.Uniq = tpmdtujuan.IdMisi where IdPeriode = $idPeriode and tpmdtujuan.IsDeleted=0 order by tpmdtujuan.IdMisi, tpmdtujuan.Uniq asc", COL_UNIQ, COL_URAIAN, (!empty($data)?$data[COL_IDTUJUANPMD]:null))?>
    </select>
  </div>
  <div class="form-group">
    <label>Uraian</label>
    <textarea name="<?=COL_URAIAN?>" class="form-control" rows="3" placeholder="Uraian Tujuan" required><?=!empty($data)?$data[COL_URAIAN]:''?></textarea>
  </div>
</form>
