<?php
$rOptTujuan = $this->db->query("
select
  trenstratujuan.Uraian,
  trenstratujuan.Uniq,
  trenstratujuan.IdTujuanPmd
from trenstratujuan
left join tpmdtujuan on tpmdtujuan.Uniq = trenstratujuan.IdTujuanPmd
where trenstratujuan.IdRenstra = $idRenstra and trenstratujuan.IsDeleted=0 and tpmdtujuan.IsDeleted=0
order by tpmdtujuan.IdMisi, tpmdtujuan.Uniq, trenstratujuan.Uniq asc"
)->result_array();
if(empty($rOptTujuan)) {
  echo 'Harap mengisi data TUJUAN terlebih dahulu!';
  exit();
}
?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Tujuan SKPD</label>
    <select class="form-control" name="<?=COL_IDTUJUAN?>" style="width: 100%" required>
      <?php
      foreach($rOptTujuan as $opt) {
        ?>
        <option value="<?=$opt[COL_UNIQ]?>" data-url="<?=site_url('site/perencanaan/opt-pmdsasaran-by-renstratujuan/'.$opt[COL_IDTUJUANPMD].(!empty($data)?'/'.$data[COL_IDSASARANPMD]:''))?>" <?=(!empty($data)&&$data[COL_IDTUJUAN]==$opt[COL_UNIQ]?'selected':'')?>><?=$opt[COL_URAIAN]?></option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>Sasaran RPJMD</label>
    <select class="form-control" name="<?=COL_IDSASARANPMD?>" style="width: 100%" required>
      <option value="">-- Pilih Sasaran --</option>
    </select>
  </div>
  <div class="form-group">
    <label>Uraian</label>
    <textarea name="<?=COL_URAIAN?>" class="form-control" rows="3" placeholder="Uraian Sasaran" required><?=!empty($data)?$data[COL_URAIAN]:''?></textarea>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('[name=IdTujuan]', $('#form-main')).change(function(){
    var url = $('option:selected', $('[name=IdTujuan]', $('#form-main'))).data('url');
    $('[name=IdSasaranPmd]', $('#form-main')).load(url, function(){
      $('[name=IdSasaranPmd]').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
  }).trigger('change');
});
</script>
