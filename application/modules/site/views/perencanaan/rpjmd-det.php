<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('site/perencanaan/rpjmd')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?php
    if($page=='misi') {
      $rmisi = $this->db
      ->where(COL_IDPERIODE, $data[COL_UNIQ])
      ->where(COL_ISDELETED, 0)
      ->get(TBL_TPMDMISI)
      ->result_array();
      ?>
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><?=$data[COL_VISI]?></h3>
              <div class="card-tools">
                <a href="<?=site_url('site/perencanaan/rpjmd-add-misi/'.$data[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-add">
                  <i class="fas fa-plus"></i>&nbsp;TAMBAH MISI
                </a>
              </div>
            </div>
            <div class="card-body p-0">
              <table class="table table-bordered w-100">
                <tbody>
                  <?php
                  if(!empty($rmisi)) {
                    $no=1;
                    foreach($rmisi as $m) {
                      ?>
                      <tr>
                        <td class="text-center" style="width: 10px; white-space: nowrap"><?=$no?></td>
                        <td><?=$m[COL_URAIAN]?></td>
                        <td class="text-center" style="width: 10px; white-space: nowrap">
                          <a href="<?=site_url('site/perencanaan/rpjmd-edit-misi/'.$m[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                            <i class="fas fa-edit"></i>&nbsp;UBAH
                          </a>
                          <a href="<?=site_url('site/perencanaan/rpjmd-delete-misi/'.$m[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                            <i class="fas fa-trash"></i>&nbsp;HAPUS
                          </a>
                        </td>
                      </tr>
                      <?php
                      $no++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="3" class="text-center font-italic">KOSONG</td>
                    </tr>
                    <?php
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php
    } else if($page=='tujuan') {
      ?>
      <div class="row">
        <div class="col-sm-12">
          <?php
          $rmisi = $this->db
          ->where(COL_IDPERIODE, $data[COL_UNIQ])
          ->where(COL_ISDELETED, 0)
          ->get(TBL_TPMDMISI)
          ->result_array();

          foreach($rmisi as $m) {
            $rtujuan = $this->db
            ->where(COL_IDMISI, $m[COL_UNIQ])
            ->where(COL_ISDELETED, 0)
            ->get(TBL_TPMDTUJUAN)
            ->result_array();
            ?>
            <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title font-weight-bold"><?=$m[COL_URAIAN]?></h3>
                <div class="card-tools">
                  <a href="<?=site_url('site/perencanaan/rpjmd-add-tujuan/'.$m[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-add">
                    <i class="fas fa-plus"></i>&nbsp;TAMBAH TUJUAN
                  </a>
                </div>
              </div>
              <div class="card-body p-0">
                <table class="table table-bordered w-100">
                  <tbody>
                    <?php
                    if(!empty($rtujuan)) {
                      $no=1;
                      foreach($rtujuan as $t) {
                        ?>
                        <tr>
                          <td class="text-center" style="width: 10px; white-space: nowrap"><?=$no?></td>
                          <td><?=$t[COL_URAIAN]?></td>
                          <td class="text-center" style="width: 10px; white-space: nowrap">
                            <a href="<?=site_url('site/perencanaan/rpjmd-edit-tujuan/'.$t[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                              <i class="fas fa-edit"></i>&nbsp;UBAH
                            </a>
                            <a href="<?=site_url('site/perencanaan/rpjmd-delete-tujuan/'.$t[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                              <i class="fas fa-trash"></i>&nbsp;HAPUS
                            </a>
                          </td>
                        </tr>
                        <?php
                        $no++;
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="3" class="text-center font-italic">KOSONG</td>
                      </tr>
                      <?php
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
      <?php
    } else if($page=='sasaran') {
      ?>
      <div class="row">
        <div class="col-sm-12">
          <?php
          $rtujuan = $this->db
          ->select('tpmdtujuan.Uniq, tpmdtujuan.Uraian as Tujuan, tpmdmisi.Uraian as Misi')
          ->join(TBL_TPMDMISI,TBL_TPMDMISI.'.'.COL_UNIQ." = ".TBL_TPMDTUJUAN.".".COL_IDMISI,"inner")
          ->where(TBL_TPMDMISI.'.'.COL_IDPERIODE, $data[COL_UNIQ])
          ->where(TBL_TPMDMISI.'.'.COL_ISDELETED, 0)
          ->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
          ->get(TBL_TPMDTUJUAN)
          ->result_array();

          foreach($rtujuan as $t) {
            $rsasaran = $this->db
            ->where(COL_IDTUJUAN, $t[COL_UNIQ])
            ->where(COL_ISDELETED, 0)
            ->get(TBL_TPMDSASARAN)
            ->result_array();
            ?>
            <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title">Tujuan: <strong><?=$t['Tujuan']?></strong></h3>
                <div class="card-tools">
                  <a href="<?=site_url('site/perencanaan/rpjmd-add-sasaran/'.$t[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-add">
                    <i class="fas fa-plus"></i>&nbsp;TAMBAH SASARAN
                  </a>
                </div>
              </div>
              <div class="card-body p-0">
                <div class="table-responsive mb-0">
                  <table class="table table-bordered w-100 mb-0">
                    <tbody>
                      <?php
                      if(!empty($rsasaran)) {
                        $no=1;
                        foreach($rsasaran as $s) {
                          $riku = $this->db
                          ->where(COL_IDSASARAN, $s[COL_UNIQ])
                          ->where(COL_ISDELETED, 0)
                          ->get(TBL_TPMDIKU)
                          ->result_array();
                          ?>
                          <tr>
                            <td class="text-center" style="width: 10px; white-space: nowrap" <?=count($riku)>0?'rowspan="'.(count($riku)+1).'"':''?>><?=$no?></td>
                            <td class="font-weight-bold">
                              <?=$s[COL_URAIAN]?>
                              <a href="<?=site_url('site/perencanaan/rpjmd-add-iku/'.$s[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-add pull-right">
                                <i class="fas fa-plus-circle"></i>&nbsp;INDIKATOR
                              </a>
                            </td>
                            <td class="text-center" style="width: 10px; white-space: nowrap">
                              <a href="<?=site_url('site/perencanaan/rpjmd-edit-sasaran/'.$s[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                                <i class="fas fa-edit"></i>&nbsp;UBAH
                              </a>
                              <a href="<?=site_url('site/perencanaan/rpjmd-delete-sasaran/'.$s[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                                <i class="fas fa-trash"></i>&nbsp;HAPUS
                              </a>
                            </td>
                          </tr>
                          <?php
                          foreach($riku as $iku) {
                            ?>
                            <tr>
                              <td class="text-sm font-italic"><?=$iku[COL_URAIAN].(!empty($iku[COL_TARGET])?' - '.$iku[COL_TARGET].' ('.$iku[COL_SATUAN].')':'')?></td>
                              <td class="text-center" style="width: 10px; white-space: nowrap">
                                <a href="<?=site_url('site/perencanaan/rpjmd-edit-iku/'.$iku[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                                  <i class="fas fa-edit"></i>&nbsp;UBAH
                                </a>
                                <a href="<?=site_url('site/perencanaan/rpjmd-delete-iku/'.$iku[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                                  <i class="fas fa-trash"></i>&nbsp;HAPUS
                                </a>
                              </td>
                            </tr>
                            <?php
                          }
                          $no++;
                        }
                      } else {
                        ?>
                        <tr>
                          <td colspan="3" class="text-center font-italic">KOSONG</td>
                        </tr>
                        <?php
                      }
                      ?>

                    </tbody>
                  </table>
                </div>

              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Rincian</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          location.reload();
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
