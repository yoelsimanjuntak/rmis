<?php
if(!empty($res)) {
  ?>
  <h4 class="mb-4">
    <span class="badge badge-success">DATA RPJMD DITEMUKAN!</span>
  </h4>

  <p class="mb-0">PERIODE:</p>
  <p class="font-weight-bold"><?=$res['PmdTahunMulai']?> - <?=$res['PmdTahunAkhir']?></p>
  <p class="mb-0">KEPALA DAERAH:</p>
  <p class="font-weight-bold"><?=$res['PmdPejabat']?></p>
  <p class="mb-0">VISI:</p>
  <p class="font-weight-bold"><?=$res['PmdVisi']?></p>
  <form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
    <input type="hidden" name="SyncId" value="<?=$res['PmdId']?>" />
  </form>
  <div class="callout callout-danger">
    <p class="font-italic mb-0">Apakah anda yakin ingin melanjutkan sinkronisasi data?<br /><span class="font-weight-bold text-danger">proses sinkronisasi akan menghapus data yang sudah dientri sebelumnya (jika ada).</span></p>
  </div>
  <?php
} else {
  ?>
  <h4 class="mb-4">
    <span class="badge badge-danger">MAAF, DATA RPJMD TIDAK DITEMUKAN!</span>
  </h4>
  <?php
}
?>
