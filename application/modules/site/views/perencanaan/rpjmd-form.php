<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Judul</label>
    <input type="text" class="form-control" name="<?=COL_PERIODNAMA?>" value="<?=!empty($data)?$data[COL_PERIODNAMA]:''?>" placeholder="RPJMD" required />
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>Mulai</label>
        <input type="number" class="form-control" name="<?=COL_PERIODFROM?>" value="<?=!empty($data)?$data[COL_PERIODFROM]:''?>" placeholder="Tahun Mulai" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Berakhir</label>
        <input type="number" class="form-control" name="<?=COL_PERIODTO?>" value="<?=!empty($data)?$data[COL_PERIODTO]:''?>" placeholder="Tahun Berakhir" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Kepala Daerah</label>
    <input type="text" class="form-control" name="<?=COL_PERIODKEPALADAERAH?>" value="<?=!empty($data)?$data[COL_PERIODKEPALADAERAH]:''?>" placeholder="Nama Kepala Daerah" required />
  </div>
  <div class="form-group">
    <label>Visi</label>
    <textarea name="<?=COL_VISI?>" class="form-control" rows="3" placeholder="Uraian Visi Kepala Daerah"><?=!empty($data)?$data[COL_VISI]:''?></textarea>
  </div>
</form>
