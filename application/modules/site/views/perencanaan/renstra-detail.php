<?php
$tab = 'tujuan';
if(!empty($this->input->get('tab'))) {
  $tab = $this->input->get('tab');
}

$rtujuan = $this->db
->select('trenstratujuan.*, tpmdtujuan.Uraian as TujuanPmd')
->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDTUJUANPMD,"inner")
->where(COL_IDRENSTRA, $data[COL_UNIQ])
->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
->order_by(TBL_TPMDTUJUAN.'.'.COL_IDMISI)
->order_by(TBL_TPMDTUJUAN.'.'.COL_UNIQ)
->get(TBL_TRENSTRATUJUAN)
->result_array();

$rsasaran = $this->db
->select('trenstrasasaran.*, tpmdsasaran.Uraian as SasaranPmd')
->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDSASARANPMD,"inner")
->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
->where(TBL_TRENSTRATUJUAN.'.'.COL_IDRENSTRA, $data[COL_UNIQ])
->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
->get(TBL_TRENSTRASASARAN)
->result_array();

$rprogram = $this->db
->where(TBL_TRENJAPROGRAM.'.'.COL_IDRENSTRA, $data[COL_UNIQ])
->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
->order_by(TBL_TRENJAPROGRAM.'.'.COL_TAHUN, 'desc')
->order_by(TBL_TRENJAPROGRAM.'.'.COL_PROGKODE, 'asc')
->get(TBL_TRENJAPROGRAM)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-md-6 col-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <p class="mb-0">
          <a href="<?=site_url('site/perencanaan/renstra-sync/'.$data[COL_UNIQ])?>" class="btn btn-primary btn-sm btn-sync"><i class="fas fa-sync-alt"></i>&nbsp; SINKRON RENSTRA</a>
          <a href="<?=site_url('site/perencanaan/renstra-sync-renja/'.$data[COL_UNIQ])?>" class="btn btn-primary btn-sm btn-sync"><i class="fas fa-sync-alt"></i>&nbsp; SINKRON RENJA</a>
          <?=anchor('site/perencanaan/renstra','<i class="far fa-arrow-circle-left"></i> KEMBALI',array('class'=>'btn btn-secondary btn-sm'))?>
        </p>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header d-flex p-0">
            <ul class="nav nav-pills p-2">
              <li class="nav-item"><a class="nav-link <?=!empty($tab)&&$tab=='tujuan'?'active':(empty($tab)?'active':'')?>" href="#tabTujuan" data-toggle="tab">Tujuan</a></li>
              <li class="nav-item"><a class="nav-link <?=!empty($tab)&&$tab=='sasaran'?'active':''?>" href="#tabSasaran" data-toggle="tab">Sasaran</a></li>
              <li class="nav-item"><a class="nav-link <?=!empty($tab)&&$tab=='renja'?'active':''?>" href="#tabRenja" data-toggle="tab">Renja</a></li>
            </ul>
            <div class="card-tools ml-auto p-3">

            </div>
          </div>
          <div class="card-body p-0">
            <div class="tab-content">
              <div class="tab-pane <?=!empty($tab)&&$tab=='tujuan'?'active':(empty($tab)?'active':'')?>" id="tabTujuan">
                <table class="table table-bordered mb-0" style="border-top: none !important">
                  <thead>
                    <tr>
                      <th class="no-border text-center" style="width: 10px">No</th>
                      <th class="no-border">Uraian</th>
                      <th class="no-border text-center" style="width: 100px">Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($rtujuan)) {
                      $no=1;
                      foreach($rtujuan as $t) {
                        ?>
                        <tr>
                          <td class="text-center" style="width: 10px"><?=$no?></td>
                          <td>
                            <?=$t['TujuanPmd'].'&nbsp;<span class="pl-2 pr-2"><i class="fas fa-arrow-alt-right"></i></span>&nbsp;<strong>'.$t[COL_URAIAN].'</strong>'?>
                          </td>
                          <td class="text-center" style="width: 100px; white-space: nowrap">
                            <a href="<?=site_url('site/perencanaan/renstra-edit-tujuan/'.$t[COL_UNIQ])?>?tab=sasaran" class="btn btn-outline-info btn-sm btn-edit">
                              <i class="fas fa-edit"></i>&nbsp;UBAH
                            </a>
                            <a href="<?=site_url('site/perencanaan/renstra-delete-tujuan/'.$t[COL_UNIQ])?>?tab=sasaran" class="btn btn-outline-danger btn-sm btn-delete">
                              <i class="fas fa-trash"></i>&nbsp;HAPUS
                            </a>
                          </td>
                        </tr>
                        <?php
                        $no++;
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="3" class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
                <p class="p-3 text-right mb-0">
                  <a href="<?=site_url('site/perencanaan/renstra-add-tujuan/'.$data[COL_UNIQ])?>?tab=tujuan" class="btn btn-sm btn-primary btn-add"><i class="fas fa-plus-circle"></i>&nbsp; TAMBAH TUJUAN</a>
                </p>
              </div>
              <div class="tab-pane <?=!empty($tab)&&$tab=='sasaran'?'active':''?>" id="tabSasaran">
                <table class="table table-bordered mb-0" style="border-top: none !important">
                  <thead>
                    <tr>
                      <th class="no-border text-center" style="width: 10px">No</th>
                      <th class="no-border">Uraian</th>
                      <th class="no-border text-center" style="width: 100px">Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($rsasaran)) {
                      $no=1;
                      foreach($rsasaran as $s) {
                        $riku = $this->db
                        ->where(COL_IDSASARAN, $s[COL_UNIQ])
                        ->where(COL_ISDELETED, 0)
                        ->get(TBL_TRENSTRASASARANDET)
                        ->result_array();
                        ?>
                        <tr>
                          <td class="text-center" style="width: 10px" <?=count($riku)>0?'rowspan="'.(count($riku)+1).'"':''?>><?=$no?></td>
                          <td>
                            <?=$s['SasaranPmd'].'&nbsp;<span class="pl-2 pr-2"><i class="fas fa-arrow-alt-right"></i></span>&nbsp;<strong>'.$s[COL_URAIAN].'</strong>'?>
                            <a href="<?=site_url('site/perencanaan/renstra-add-iku/'.$s[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-add pull-right">
                              <i class="fas fa-plus-circle"></i>&nbsp;INDIKATOR
                            </a>
                          </td>
                          <td class="text-center" style="width: 100px; white-space: nowrap">
                            <a href="<?=site_url('site/perencanaan/renstra-edit-sasaran/'.$s[COL_UNIQ])?>?tab=sasaran" class="btn btn-outline-info btn-sm btn-edit">
                              <i class="fas fa-edit"></i>&nbsp;UBAH
                            </a>
                            <a href="<?=site_url('site/perencanaan/renstra-delete-sasaran/'.$s[COL_UNIQ])?>?tab=sasaran" class="btn btn-outline-danger btn-sm btn-delete">
                              <i class="fas fa-trash"></i>&nbsp;HAPUS
                            </a>
                          </td>
                        </tr>
                        <?php
                        foreach($riku as $iku) {
                          ?>
                          <tr>
                            <td class="text-sm font-italic"><?=$iku[COL_URAIAN].(!empty($iku[COL_TARGET])?' - '.$iku[COL_TARGET].' ('.$iku[COL_SATUAN].')':'')?></td>
                            <td class="text-center" style="width: 10px; white-space: nowrap">
                              <a href="<?=site_url('site/perencanaan/renstra-edit-iku/'.$iku[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                                <i class="fas fa-edit"></i>&nbsp;UBAH
                              </a>
                              <a href="<?=site_url('site/perencanaan/renstra-delete-iku/'.$iku[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                                <i class="fas fa-trash"></i>&nbsp;HAPUS
                              </a>
                            </td>
                          </tr>
                          <?php
                        }
                        $no++;
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="3" class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
                <p class="p-3 text-right mb-0">
                  <a href="<?=site_url('site/perencanaan/renstra-add-sasaran/'.$data[COL_UNIQ])?>?tab=sasaran" class="btn btn-sm btn-primary btn-add"><i class="fas fa-plus-circle"></i>&nbsp; TAMBAH SASARAN</a>
                </p>
              </div>
              <div class="tab-pane <?=!empty($tab)&&$tab=='renja'?'active':''?>" id="tabRenja">
                <table class="table table-bordered mb-0" style="border-top: none !important">
                  <thead>
                    <tr>
                      <th class="no-border text-center" style="width: 10px; white-space: nowrap">Kode Rekening</th>
                      <th class="no-border">Program / Kegiatan / Sub Kegiatan</th>
                      <th class="no-border text-center" style="width: 100px">Tahun</th>
                      <th class="no-border text-center" style="width: 100px">Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($rprogram)) {
                      foreach ($rprogram as $p) {
                        $rkegiatan = $this->db
                        ->where(TBL_TRENJAKEGIATAN.'.'.COL_IDPROGRAM, $p[COL_UNIQ])
                        ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
                        ->order_by(TBL_TRENJAKEGIATAN.'.'.COL_KEGKODE)
                        ->get(TBL_TRENJAKEGIATAN)
                        ->result_array();
                        ?>
                        <tr class="bg-light">
                          <th class="text-left" style="width: 10px"><?=$p[COL_PROGKODE]?></th>
                          <th><?=$p[COL_PROGURAIAN]?></th>
                          <th class="text-center"><?=$p[COL_TAHUN]?></th>
                          <th class="text-left" style="width: 100px; white-space: nowrap">
                            <a href="<?=site_url('site/perencanaan/renja-edit-program/'.$p[COL_UNIQ])?>" class="btn btn-info btn-sm btn-edit">
                              <i class="fas fa-edit"></i>&nbsp;UBAH
                            </a>
                            <a href="<?=site_url('site/perencanaan/renja-delete-program/'.$p[COL_UNIQ])?>" class="btn btn-danger btn-sm btn-delete">
                              <i class="fas fa-trash"></i>&nbsp;HAPUS
                            </a>
                            <a href="<?=site_url('site/perencanaan/renja-add-kegiatan/'.$p[COL_UNIQ])?>" class="btn btn-success btn-sm btn-add">
                              <i class="fas fa-plus-circle"></i>&nbsp;RINCIAN
                            </a>
                          </th>
                        </tr>
                        <?php
                        foreach($rkegiatan as $k) {
                          $rsubkegiatan = $this->db
                          ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_IDKEGIATAN, $k[COL_UNIQ])
                          ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
                          ->order_by(TBL_TRENJAKEGIATANSUB.'.'.COL_SUBKODE)
                          ->get(TBL_TRENJAKEGIATANSUB)
                          ->result_array();
                          ?>
                          <tr>
                            <td class="text-left" style="width: 10px"><?=$k[COL_KEGKODE]?></td>
                            <td colspan="2">
                              <?=$k[COL_KEGURAIAN]?><br />
                              <small class="font-italic pl-3"><?=$k[COL_KEGINDIKATOR]?> - <?=$k[COL_KEGTARGET]?> (<?=$k[COL_KEGSATUAN]?>)</small>
                            </td>
                            <td class="text-left" style="width: 100px; white-space: nowrap">
                              <a href="<?=site_url('site/perencanaan/renja-edit-kegiatan/'.$k[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                                <i class="fas fa-edit"></i>&nbsp;UBAH
                              </a>
                              <a href="<?=site_url('site/perencanaan/renja-delete-kegiatan/'.$k[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                                <i class="fas fa-trash"></i>&nbsp;HAPUS
                              </a>
                              <a href="<?=site_url('site/perencanaan/renja-add-subkegiatan/'.$k[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-add">
                                <i class="fas fa-plus-circle"></i>&nbsp;RINCIAN
                              </a>
                            </td>
                          </tr>
                          <?php
                          foreach($rsubkegiatan as $s) {
                            ?>
                            <tr>
                              <td class="text-left" style="width: 10px"><?=$s[COL_SUBKODE]?></td>
                              <td colspan="2">
                                <?=$s[COL_SUBURAIAN]?><br />
                                <small class="font-italic pl-3"><?=$s[COL_SUBINDIKATOR]?> - <?=$s[COL_SUBTARGET]?> (<?=$s[COL_SUBSATUAN]?>)</small>
                              </td>
                              <td class="text-left" style="width: 100px; white-space: nowrap">
                                <a href="<?=site_url('site/perencanaan/renja-edit-subkegiatan/'.$s[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                                  <i class="fas fa-edit"></i>&nbsp;UBAH
                                </a>
                                <a href="<?=site_url('site/perencanaan/renja-delete-subkegiatan/'.$s[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                                  <i class="fas fa-trash"></i>&nbsp;HAPUS
                                </a>
                              </td>
                            </tr>
                            <?php
                          }
                        }
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="4" class="text-center font-italic">(KOSONG)</td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
                <p class="p-3 text-right mb-0">
                  <a href="<?=site_url('site/perencanaan/renja-add-program/'.$data[COL_UNIQ])?>?tab=renja" class="btn btn-sm btn-primary btn-add"><i class="fas fa-plus-circle"></i>&nbsp; TAMBAH PROGRAM</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Rincian Renstra</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-sync" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Sync E-SAKIP <strong>SITALAKBAJAKUN</strong></span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;LANJUT</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-renja" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Program / Kegiatan / Sub Kegiatan</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          if(data.redirect) location.href=data.redirect;
          else location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
  $('button[type=submit]', $('#modal-sync')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-sync')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          if(data.redirect) location.href = data.redirect;
          else location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          if(res.redirect) location.href=res.redirect;
          else location.reload();
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
  $('.btn-sync').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-sync');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });
});
</script>
