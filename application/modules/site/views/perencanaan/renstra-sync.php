<?php
if(!empty($res)) {
  ?>
  <h4 class="mb-4">
    <span class="badge badge-info">DATA RENSTRA DITEMUKAN!</span>
  </h4>
  <ul class="nav flex-column mb-4">
    <!--<li class="nav-item p-1">
      Judul <span class="font-weight-bold float-right"><?=$res['RenstraUraian']?></span>
    </li>
    <li class="nav-item p-1">
      Tahun <span class="font-weight-bold float-right"><?=$res['RenstraTahun']?></span>
    </li>-->
    <li class="nav-item p-1">
      Tujuan
      <?php
      if(!empty($rtujuan)) {
        ?>
        <ul>
          <?php
          foreach($rtujuan as $t) {
            ?>
            <li class="m-0 text-sm font-italic font-weight-bold"><?=$t['TujuanUraian']?></li>
            <?php
          }
          ?>
        </ul>
        <?php
      }
      ?>
    </li>
    <li class="nav-item p-1">
      Sasaran
      <?php
      if(!empty($rsasaran)) {
        ?>
        <ul>
          <?php
          foreach($rsasaran as $s) {
            ?>
            <li class="m-0 text-sm font-italic font-weight-bold"><?=$s['SasaranUraian']?></li>
            <?php
          }
          ?>
        </ul>
        <?php
      }
      ?>
    </li>
    <li class="nav-item p-1">
      IKU
      <?php
      if(!empty($riku)) {
        ?>
        <ul>
          <?php
          foreach($riku as $s) {
            ?>
            <li class="m-0 text-sm font-italic font-weight-bold"><?=$s[COL_URAIAN].' - '.$s[COL_TARGET].' ('.$s[COL_SATUAN].')'?></li>
            <?php
          }
          ?>
        </ul>
        <?php
      }
      ?>
    </li>
  </ul>
  <form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
    <input type="hidden" name="SyncId" value="<?=$res['RenstraId']?>" />
  </form>
  <div class="callout callout-danger">
    <p class="font-italic mb-0">Apakah anda yakin ingin melanjutkan sinkronisasi data?<br /><span class="font-weight-bold text-danger">proses sinkronisasi akan menghapus data yang sudah dientri sebelumnya (jika ada).</span></p>
  </div>
  <?php
} else {
  ?>
  <h4 class="mb-4">
    <span class="badge badge-danger">MAAF, DATA RENSTRA TIDAK DITEMUKAN!</span>
  </h4>
  <?php
}
?>
