<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Uraian</label>
    <textarea name="<?=COL_SUBURAIAN?>" class="form-control" rows="3" placeholder="Uraian Sub Kegiatan" required><?=!empty($data)?$data[COL_SUBURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Kode</label>
    <input type="text" class="form-control" name="<?=COL_SUBKODE?>" value="<?=!empty($data)?$data[COL_SUBKODE]:''?>" placeholder="Kode Rekening" required />
  </div>
  <div class="form-group">
    <label>Tujuan +</label>
    <textarea name="<?=COL_SUBTUJUAN?>" class="form-control" rows="3" placeholder="Uraian Tujuan" required><?=!empty($data)?$data[COL_SUBTUJUAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Indikator</label>
    <input type="text" class="form-control" name="<?=COL_SUBINDIKATOR?>" value="<?=!empty($data)?$data[COL_SUBINDIKATOR]:''?>" placeholder="Indikator Keberhasilan" required />
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-8">
        <label>Satuan</label>
        <input type="text" class="form-control" name="<?=COL_SUBSATUAN?>" value="<?=!empty($data)?$data[COL_SUBSATUAN]:''?>" placeholder="Satuan" required />
      </div>
      <div class="col-sm-4">
        <label>Target</label>
        <input type="number" class="form-control" name="<?=COL_SUBTARGET?>" value="<?=!empty($data)?$data[COL_SUBTARGET]:''?>" placeholder="Target" required />
      </div>
    </div>
  </div>
</form>
