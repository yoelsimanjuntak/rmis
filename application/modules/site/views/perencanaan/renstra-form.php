<?php
$ruser = GetLoggedUser();
?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Periode RPJMD</label>
    <select class="form-control" name="<?=COL_IDPERIODE?>" style="width: 100%" <?=(!empty($data)?'disabled':'')?>>
      <?=GetCombobox("select * from tpmdperiode where IsDeleted=0 and PeriodIsAktif=1 order by PeriodFrom desc", COL_UNIQ, COL_PERIODNAMA, (!empty($data)?$data[COL_IDPERIODE]:null))?>
    </select>
  </div>
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    ?>
    <div class="form-group">
      <label>OPD</label>
      <select class="form-control" name="<?=COL_IDOPD?>" style="width: 100%" <?=(!empty($data)?'disabled':'')?>>
        <?=GetCombobox("select * from mopd where IsDeleted=0 order by OPDNama asc", COL_UNIQ, COL_OPDNAMA, (!empty($data)?$data[COL_IDOPD]:null))?>
      </select>
    </div>
    <?php
  } else {
    ?>
    <input type="hidden" name="<?=COL_IDOPD?>" value="<?=$ruser[COL_IDENTITYNO]?>" />
    <?php
  }
  ?>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>Judul</label>
        <input type="text" class="form-control" name="<?=COL_RENSTRANAMA?>" value="<?=!empty($data)?$data[COL_RENSTRANAMA]:''?>" placeholder="Judul Renstra" required />
      </div>
      <div class="col-sm-6">
        <label>Tahun Berlaku</label>
        <input type="number" class="form-control" name="<?=COL_TAHUN?>" value="<?=!empty($data)?$data[COL_TAHUN]:''?>" placeholder="Tahun" required />
      </div>
    </div>

  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea name="<?=COL_RENSTRAKETERANGAN?>" class="form-control" rows="3" placeholder="Opsional"><?=!empty($data)?$data[COL_RENSTRAKETERANGAN]:''?></textarea>
  </div>
</form>
