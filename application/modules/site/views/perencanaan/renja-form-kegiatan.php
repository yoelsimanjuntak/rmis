<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Uraian</label>
    <textarea name="<?=COL_KEGURAIAN?>" class="form-control" rows="3" placeholder="Uraian Kegiatan" required><?=!empty($data)?$data[COL_KEGURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Kode</label>
    <input type="text" class="form-control" name="<?=COL_KEGKODE?>" value="<?=!empty($data)?$data[COL_KEGKODE]:''?>" placeholder="Kode Rekening" required />
  </div>
  <div class="form-group">
    <label>Tujuan +</label>
    <textarea name="<?=COL_KEGTUJUAN?>" class="form-control" rows="3" placeholder="Uraian Tujuan" required><?=!empty($data)?$data[COL_KEGTUJUAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Indikator</label>
    <input type="text" class="form-control" name="<?=COL_KEGINDIKATOR?>" value="<?=!empty($data)?$data[COL_KEGINDIKATOR]:''?>" placeholder="Indikator Keberhasilan" required />
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-8">
        <label>Satuan</label>
        <input type="text" class="form-control" name="<?=COL_KEGSATUAN?>" value="<?=!empty($data)?$data[COL_KEGSATUAN]:''?>" placeholder="Satuan" required />
      </div>
      <div class="col-sm-4">
        <label>Target</label>
        <input type="number" class="form-control" name="<?=COL_KEGTARGET?>" value="<?=!empty($data)?$data[COL_KEGTARGET]:''?>" placeholder="Target" required />
      </div>
    </div>
  </div>
</form>
