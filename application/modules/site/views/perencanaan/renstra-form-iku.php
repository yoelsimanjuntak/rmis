<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <textarea name="<?=COL_URAIAN?>" class="form-control" rows="3" placeholder="Uraian"><?=!empty($data)?$data[COL_URAIAN]:''?></textarea>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>Target</label>
        <input type="text" class="form-control" name="<?=COL_TARGET?>" value="<?=!empty($data)&&!empty($data[COL_TARGET])?$data[COL_TARGET]:''?>" placeholder="Target" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Satuan</label>
        <input type="text" class="form-control" name="<?=COL_SATUAN?>" value="<?=!empty($data)&&!empty($data[COL_SATUAN])?$data[COL_SATUAN]:''?>" placeholder="Satuan" required />
      </div>
    </div>
  </div>
</form>
