<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <p class="mb-0">
          <?=anchor('site/perencanaan/rpjmd-add','<i class="far fa-plus-circle"></i> TAMBAH DATA',array('class'=>'btn btn-primary btn-sm btn-add'))?>
        </p>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      foreach($res as $r) {
        $numMisi = $this->db
        ->where(TBL_TPMDMISI.'.'.COL_ISDELETED, 0)
        ->where(COL_IDPERIODE, $r[COL_UNIQ])
        ->count_all_results(TBL_TPMDMISI);

        $numTujuan = $this->db
        ->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
        ->where(TBL_TPMDMISI.'.'.COL_IDPERIODE, $r[COL_UNIQ])
        ->join(TBL_TPMDMISI,TBL_TPMDMISI.'.'.COL_UNIQ." = ".TBL_TPMDTUJUAN.".".COL_IDMISI,"inner")
        ->count_all_results(TBL_TPMDTUJUAN);

        $numSasaran = $this->db
        ->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
        ->where(TBL_TPMDMISI.'.'.COL_IDPERIODE, $r[COL_UNIQ])
        ->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TPMDSASARAN.".".COL_IDTUJUAN,"inner")
        ->join(TBL_TPMDMISI,TBL_TPMDMISI.'.'.COL_UNIQ." = ".TBL_TPMDTUJUAN.".".COL_IDMISI,"inner")
        ->count_all_results(TBL_TPMDSASARAN);

        $numIKU = $this->db
        ->where(TBL_TPMDIKU.'.'.COL_ISDELETED, 0)
        ->where(TBL_TPMDMISI.'.'.COL_IDPERIODE, $r[COL_UNIQ])
        ->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TPMDIKU.".".COL_IDSASARAN,"inner")
        ->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TPMDSASARAN.".".COL_IDTUJUAN,"inner")
        ->join(TBL_TPMDMISI,TBL_TPMDMISI.'.'.COL_UNIQ." = ".TBL_TPMDTUJUAN.".".COL_IDMISI,"inner")
        ->count_all_results(TBL_TPMDIKU);
        ?>
        <div class="col-sm-12">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title"><?=$r[COL_PERIODNAMA]?></h3>
              <div class="card-tools">
                <a href="<?=site_url('site/perencanaan/rpjmd-edit/'.$r[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-edit">
                  <i class="fas fa-edit"></i>
                </a>
                <a href="<?=site_url('site/perencanaan/rpjmd-sync/'.$r[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-sync">
                  <i class="fas fa-sync"></i>
                </a>
                <a href="<?=site_url('site/perencanaan/rpjmd-delete/'.$r[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-delete">
                  <i class="fas fa-trash"></i>
                </a>
              </div>
            </div>
            <div class="card-body p-0">
              <table class="table table-striped table-valign-middle">
                <tbody>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Periode</td>
                    <td style="width: 10px">:</td>
                    <td><strong><?=$r[COL_PERIODFROM]?></strong> s.d <strong><?=$r[COL_PERIODTO]?></strong></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Kepala Daerah</td>
                    <td style="width: 10px">:</td>
                    <td><strong><?=$r[COL_PERIODKEPALADAERAH]?></strong></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Visi</td>
                    <td style="width: 10px">:</td>
                    <td><strong><?=$r[COL_VISI]?></strong></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Status</td>
                    <td style="width: 10px">:</td>
                    <td><?=$r[COL_PERIODISAKTIF]==1?'<span class="badge badge-success">AKTIF</span>':'<span class="badge badge-danger">INAKTIF</span>'?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="card-footer">
              <a href="<?=site_url('site/perencanaan/rpjmd-det/'.$r[COL_UNIQ]).'?page=misi'?>" class="btn btn-info btn-sm"><i class="fas fa-list"></i>&nbsp; MISI : <strong><?=number_format($numMisi)?></strong></a>
              <a href="<?=site_url('site/perencanaan/rpjmd-det/'.$r[COL_UNIQ]).'?page=tujuan'?>" class="btn btn-info btn-sm"><i class="fas fa-list"></i>&nbsp; TUJUAN : <strong><?=number_format($numTujuan)?></strong></a>
              <a href="<?=site_url('site/perencanaan/rpjmd-det/'.$r[COL_UNIQ]).'?page=sasaran'?>" class="btn btn-info btn-sm"><i class="fas fa-list"></i>&nbsp; SASARAN : <strong><?=number_format($numSasaran)?></strong></a>
              <a href="<?=site_url('site/perencanaan/rpjmd-det/'.$r[COL_UNIQ]).'?page=sasaran'?>" class="btn btn-info btn-sm"><i class="fas fa-list"></i>&nbsp; IKU : <strong><?=number_format($numIKU)?></strong></a>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">RPJMD</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-sync" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Sync E-SAKIP <strong>SITALAKBAJAKUN</strong></span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;LANJUT</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('button[type=submit]', $('#modal-sync')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-sync')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          location.reload();
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
  $('.btn-sync').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-sync');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){

    });
    return false;
  });
});
</script>
