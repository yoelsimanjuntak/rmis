<?php
$rOptPmd = $this->db
->order_by(COL_PERIODISAKTIF,'desc')
->order_by(COL_PERIODFROM,'desc')
->get(TBL_TPMDPERIODE)
->result_array();
$getPmd = null;
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_UNIQ];

$getTahun = !empty($_GET['Tahun'])?$_GET['Tahun']:date('Y');

$rpmd = $this->db
->where(COL_UNIQ, $getPmd)
->get(TBL_TPMDPERIODE)
->row_array();

$rmatriks = $this->db
->order_by(COL_SKALAFROM)
->get(TBL_MMATRIKS)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header p-0 border-0">
            <table class="table table-bordered mb-0">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="idPmd">
                          <?php
                          foreach($rOptPmd as $opt) {
                            ?>
                            <option value="<?=site_url('site/laporan/risiko-pemda').'?idPmd='.$opt[COL_UNIQ].'&Tahun='.$getTahun?>" <?=$opt[COL_UNIQ]==$getPmd?'selected':''?>>
                              <?=$opt[COL_PERIODFROM].' s.d '.$opt[COL_PERIODTO].' - '.strtoupper($opt[COL_PERIODKEPALADAERAH])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">TAHUN :</label>
                      <div class="col-lg-2">
                        <select class="form-control" name="Tahun">
                          <?php
                          if(!empty($rpmd)) {
                            for($i=$rpmd[COL_PERIODFROM]; $i<=$rpmd[COL_PERIODTO]; $i++) {
                              ?>
                              <option value="<?=site_url('site/laporan/risiko-pemda').'?idPmd='.$opt[COL_UNIQ].'&Tahun='.$getTahun?>" <?=$i==$getTahun?'selected':''?>>
                                <?=$i?>
                              </option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
          <div class="card-body p-0">

          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <?php
        $rtujuan = $this->db
        ->select('tpmdtujuan.Uniq, tpmdtujuan.Uraian as Tujuan, tpmdmisi.Uraian as Misi')
        ->join(TBL_TPMDMISI,TBL_TPMDMISI.'.'.COL_UNIQ." = ".TBL_TPMDTUJUAN.".".COL_IDMISI,"inner")
        ->where(TBL_TPMDMISI.'.'.COL_IDPERIODE, $getPmd)
        ->where(TBL_TPMDMISI.'.'.COL_ISDELETED, 0)
        ->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
        ->get(TBL_TPMDTUJUAN)
        ->result_array();

        foreach($rtujuan as $t) {
          $rsasaran = $this->db
          ->where(COL_IDTUJUAN, $t[COL_UNIQ])
          ->where(COL_ISDELETED, 0)
          ->get(TBL_TPMDSASARAN)
          ->result_array();
          ?>
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">Tujuan: <strong><?=$t['Tujuan']?></strong></h3>
            </div>
            <div class="card-body p-0">
              <div class="table-responsive mb-0">
                <table class="table table-bordered table-condensed w-100 mb-0">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>SASARAN /IKU</th>
                      <?php
                      foreach($rmatriks as $m) {
                        ?>
                          <th class="<?=$m[COL_HEXCODE]?>" style="color: #fff !important"><?=strtoupper($m[COL_KATEGORI])?></th>
                        <?php
                      }
                      ?>
                      <th class="bg-primary">JLH. RISIKO</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($rsasaran)) {
                      $no=1;
                      foreach($rsasaran as $s) {
                        $riku = $this->db
                        ->where(COL_IDSASARAN, $s[COL_UNIQ])
                        ->where(COL_ISDELETED, 0)
                        ->get(TBL_TPMDIKU)
                        ->result_array();
                        ?>
                        <tr>
                          <td class="text-center" style="width: 10px; white-space: nowrap" <?=count($riku)>0?'rowspan="'.(count($riku)+1).'"':''?>><?=$no?></td>
                          <td class="font-weight-bold" colspan="2">
                            <?=$s[COL_URAIAN]?>
                          </td>
                        </tr>
                        <?php
                        foreach($riku as $iku) {
                          ?>
                          <tr>
                            <td class="text-sm font-italic"><?=$iku[COL_URAIAN].(!empty($iku[COL_TARGET])?' - '.$iku[COL_TARGET].' ('.$iku[COL_SATUAN].')':'')?></td>
                            <?php
                            $sum_ = 0;
                            foreach($rmatriks as $m) {
                              $rreg = $this->db
                              ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
                              ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
                              ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
                              ->where(TBL_TRENSTRA.'.'.COL_IDPERIODE, $getPmd)
                              ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
                              ->where(TBL_TRISIKO.'.'.COL_TAHUN, $getTahun)
                              ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
                              ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PMDIKU')
                              ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDREF, $iku[COL_UNIQ])
                              ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." >= ", $m[COL_SKALAFROM])
                              ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." <= ", $m[COL_SKALATO])
                              ->get(TBL_TRISIKOREG)
                              ->result_array();
                              ?>
                              <td class="text-center <?=$m[COL_HEXCODE]?>" style="width: 10px; white-space: nowrap">
                                <span class="badge bg-white"><?=number_format(count($rreg))?></span>
                              </td>
                              <?php
                              $sum_ += count($rreg);
                            }
                            ?>
                            <td class="text-center font-weight-bold bg-primary" style="width: 10px; white-space: nowrap">
                              <span class="badge bg-white"><?=number_format($sum_)?></span>
                            </td>
                          </tr>
                          <?php
                        }
                        $no++;
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="3" class="text-center font-italic">KOSONG</td>
                      </tr>
                      <?php
                    }
                    ?>

                  </tbody>
                </table>
              </div>

            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select[name=idPmd],input[name=Tahun]').change(function(){
    var url = $(this).val();
    location.href = url;
  });
});
</script>
