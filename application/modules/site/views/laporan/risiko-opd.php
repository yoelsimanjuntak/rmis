<?php
$rOptPmd = $this->db
->order_by(COL_PERIODISAKTIF,'desc')
->order_by(COL_PERIODFROM,'desc')
->get(TBL_TPMDPERIODE)
->result_array();
$getPmd = null;
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_UNIQ];

$getTahun = !empty($_GET['Tahun'])?$_GET['Tahun']:date('Y');

$rpmd = $this->db
->where(COL_UNIQ, $getPmd)
->get(TBL_TPMDPERIODE)
->row_array();

$rmatriks = $this->db
->order_by(COL_SKALAFROM)
->get(TBL_MMATRIKS)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header p-0 border-0">
            <table class="table table-bordered mb-0">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="idPmd">
                          <?php
                          foreach($rOptPmd as $opt) {
                            ?>
                            <option value="<?=site_url('site/laporan/risiko-pemda').'?idPmd='.$opt[COL_UNIQ].'&Tahun='.$getTahun?>" <?=$opt[COL_UNIQ]==$getPmd?'selected':''?>>
                              <?=$opt[COL_PERIODFROM].' s.d '.$opt[COL_PERIODTO].' - '.strtoupper($opt[COL_PERIODKEPALADAERAH])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">TAHUN :</label>
                      <div class="col-lg-2">
                        <select class="form-control" name="Tahun">
                          <?php
                          if(!empty($rpmd)) {
                            for($i=$rpmd[COL_PERIODFROM]; $i<=$rpmd[COL_PERIODTO]; $i++) {
                              ?>
                              <option value="<?=site_url('site/laporan/risiko-pemda').'?idPmd='.$opt[COL_UNIQ].'&Tahun='.$getTahun?>" <?=$i==$getTahun?'selected':''?>>
                                <?=$i?>
                              </option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
          <div class="card-body p-0">

          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-info">
          <?php
          $ropd = $this->db
          ->where(COL_ISDELETED,0)
          ->order_by(COL_OPDNAMA)
          ->get(TBL_MOPD)
          ->result_array();
          ?>
          <div class="card-body p-0">
            <div class="table-responsive mb-0">
              <table class="table table-bordered table-condensed w-100 mb-0">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NAMA OPD</th>
                    <?php
                    foreach($rmatriks as $m) {
                      ?>
                        <th class="<?=$m[COL_HEXCODE]?>" style="color: #fff !important"><?=strtoupper($m[COL_KATEGORI])?></th>
                      <?php
                    }
                    ?>
                    <th class="bg-primary">JLH. RISIKO</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($ropd)) {
                    $no=1;
                    $sumAll = 0;
                    $sumArr = array();
                    foreach($ropd as $opd) {
                      ?>
                      <tr>
                        <td class="text-center nowrap" style="width: 10px;"><?=$no?></td>
                        <td><?=$opd[COL_OPDNAMA]?></td>
                        <?php
                        $sum_ = 0;
                        $im = 0;
                        foreach($rmatriks as $m) {
                          $rreg = $this->db
                          ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
                          ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
                          ->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
                          ->where(TBL_TRENSTRA.'.'.COL_IDPERIODE, $getPmd)
                          ->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
                          ->where(TBL_TRISIKO.'.'.COL_TAHUN, $getTahun)
                          ->where(TBL_TRISIKO.'.'.COL_IDOPD, $opd[COL_UNIQ])
                          ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
                          ->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('TUJUAN','SASARAN','IKU','PROGRAM','KEGIATAN','SUBKEGIATAN'))
                          ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." >= ", $m[COL_SKALAFROM])
                          ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." <= ", $m[COL_SKALATO])
                          ->get(TBL_TRISIKOREG)
                          ->result_array();
                          ?>
                          <td class="text-center <?=$m[COL_HEXCODE]?>" style="width: 10px; white-space: nowrap">
                            <span class="badge bg-white"><?=number_format(count($rreg))?></span>
                          </td>
                          <?php
                          $sum_ += count($rreg);
                          $sumArr[$im] = isset($sumArr[$im]) ? $sumArr[$im]+count($rreg) : count($rreg);
                          $im++;
                        }
                        ?>
                        <td class="text-center font-weight-bold bg-primary" style="width: 10px; white-space: nowrap">
                          <span class="badge bg-white"><?=number_format($sum_)?></span>
                        </td>
                      </tr>
                      <?php
                      $no++;
                      $sumAll += $sum_;
                    }
                    ?>
                    <tr>
                      <td colspan="2" class="text-center font-weight-bold">TOTAL</td>
                      <?php
                      $im = 0;
                      foreach($rmatriks as $m) {
                        ?>
                        <td class="text-center <?=$m[COL_HEXCODE]?>" style="width: 10px; white-space: nowrap">
                          <span class="badge bg-white"><?=number_format((isset($sumArr[$im])?$sumArr[$im]:0))?></span>
                        </td>
                        <?php
                        $im++;
                      }
                      ?>
                      <td class="text-center font-weight-bold bg-primary" style="width: 10px; white-space: nowrap">
                        <span class="badge bg-white"><?=number_format($sumAll)?></span>
                      </td>
                    </tr>
                    <?php
                  } else {
                    ?>
                    <tr>
                      <td colspan="3" class="text-center font-italic">TIDAK ADA DATA TERSEDIA</td>
                    </tr>
                    <?php
                  }
                  ?>

                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select[name=idPmd],input[name=Tahun]').change(function(){
    var url = $(this).val();
    location.href = url;
  });
});
</script>
