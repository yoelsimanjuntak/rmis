<?php
$ruser = GetLoggedUser();
$numOPD = $this->db
->where(COL_ISDELETED, 0)
->count_all_results(TBL_MOPD);

$rperiod = $this->db
->where(COL_PERIODISAKTIF, 1)
->where(COL_ISDELETED, 0)
->order_by(COL_PERIODFROM,'desc')
->get(TBL_TPMDPERIODE)
->row_array();

$arrChartRisk1 = array();
$arrChartRisk2 = array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?php
    if($ruser[COL_ROLEID]==ROLEADMIN) {
      $rregister = $this->db
      ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
      ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
      ->where(TBL_TRISIKO.'.'.COL_TAHUN, date('Y'))
      ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
      ->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PMDTUJUAN','PMDSASARAN','PMDIKU'))
      ->get(TBL_TRISIKOREG)
      ->result_array();

      $rmatriks = $this->db->order_by(COL_SKALAFROM, 'asc')->get(TBL_MMATRIKS)->result_array();
      $arrLabelMatriks = array();
      $arrDtMatriks1 = array();
      $arrDtMatriks2 = array();
      $arrBgMatriks = array();
      foreach($rmatriks as $r) {
        $arrLabelMatriks[] = $r[COL_KATEGORI];
        $numreg1 = $this->db
        ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
        ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
        ->where(TBL_TRISIKO.'.'.COL_TAHUN, date('Y'))
        ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
        ->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PMDTUJUAN','PMDSASARAN','PMDIKU'))
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." >= ", $r[COL_SKALAFROM])
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." <= ", $r[COL_SKALATO])
        ->count_all_results(TBL_TRISIKOREG);

        $numreg2 = $this->db
        ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
        ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
        ->where(TBL_TRISIKO.'.'.COL_TAHUN, date('Y'))
        ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
        ->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('TUJUAN','SASARAN','IKU'))
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." >= ", $r[COL_SKALAFROM])
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." <= ", $r[COL_SKALATO])
        ->count_all_results(TBL_TRISIKOREG);

        $arrDtMatriks1[] = $numreg1;
        $arrDtMatriks2[] = $numreg2;
        $arrBgMatriks[] = $r[COL_RGB];
      }
      $arrChartRisk1 = array(
        'labels'=>$arrLabelMatriks,
        'datasets'=>array(
          array(
            'data'=>$arrDtMatriks1,
            'backgroundColor'=>$arrBgMatriks
          )
        )
      );
      $arrChartRisk2 = array(
        'labels'=>$arrLabelMatriks,
        'datasets'=>array(
          array(
            'data'=>$arrDtMatriks2,
            'backgroundColor'=>$arrBgMatriks
          )
        )
      );

      ?>
      <div class="row">
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($numOPD)?></h3>
              <p class="font-italic">PERANGKAT DAERAH</p>
            </div>
            <div class="icon">
              <i class="far fa-building"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=$rperiod[COL_PERIODFROM]?> <span class="text-sm">s.d</span> <?=$rperiod[COL_PERIODTO]?></h3>
              <p class="font-italic">PERIODE RPJMD</p>
            </div>
            <div class="icon">
              <i class="far fa-university"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format(count($rregister))?></h3>
              <p class="font-italic">RISK REGISTER PEMDA</p>
            </div>
            <div class="icon">
              <i class="far fa-analytics"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">SKALA RISIKO STRATEGIS PEMDA</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-8">
                  <div class="chart-responsive">
                    <canvas id="chartRisk1" height="150"></canvas>
                  </div>
                </div>
                <div class="col-lg-4">
                  <ul class="nav nav-pills flex-column" style="border: 1px solid rgba(0,0,0,.125) !important">
                    <?php
                    $i=0;
                    foreach($rmatriks as $m) {
                      ?>
                      <li class="nav-item">
                        <p class="nav-link mb-0" style="color: <?=$m[COL_RGB]?>">
                          <?=$m[COL_KATEGORI]?>
                          <span class="float-right font-weight-bold"><?=$arrDtMatriks1[$i]?></span>
                        </p>
                      </li>
                      <?php
                      $i++;
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">SKALA RISIKO STRATEGIS OPD</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-8">
                  <div class="chart-responsive">
                    <canvas id="chartRisk2" height="150"></canvas>
                  </div>
                </div>
                <div class="col-lg-4">
                  <ul class="nav nav-pills flex-column" style="border: 1px solid rgba(0,0,0,.125) !important">
                    <?php
                    $i=0;
                    foreach($rmatriks as $m) {
                      ?>
                      <li class="nav-item">
                        <p class="nav-link mb-0" style="color: <?=$m[COL_RGB]?>">
                          <?=$m[COL_KATEGORI]?>
                          <span class="float-right font-weight-bold"><?=$arrDtMatriks2[$i]?></span>
                        </p>
                      </li>
                      <?php
                      $i++;
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="col-lg-4 col-12">
          <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title font-weight-bold">MATRIKS RISIKO</h3>
              </div>
              <div class="card-body bg-white p-0">
                <ul class="nav nav-pills flex-column">
                  <?php
                  foreach($rmatriks as $m) {
                    ?>
                    <li class="nav-item">
                      <p class="nav-link mb-0" style="color: <?=$m[COL_RGB]?>">
                        <?=$m[COL_KATEGORI]?>
                        <span class="float-right"><?=$m[COL_SKALAFROM]?> - <?=$m[COL_SKALATO]?></span>
                      </p>
                    </li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
        </div>-->
      </div>
      <?php
    } else {
      $rregister = $this->db
      ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
      ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
      ->where(TBL_TRISIKO.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO])
      ->where(TBL_TRISIKO.'.'.COL_TAHUN, date('Y'))
      ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRISIKOREG)
      ->result_array();

      $rriskevent = $this->db
      ->join(TBL_TRISIKOREG,TBL_TRISIKOREG.'.'.COL_UNIQ." = ".TBL_TRISIKOLOG.".".COL_IDRISIKOREG,"inner")
      ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
      ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
      ->where(TBL_TRISIKO.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO])
      ->where(TBL_TRISIKO.'.'.COL_TAHUN, date('Y'))
      ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
      ->get(TBL_TRISIKOLOG)
      ->result_array();

      $rmatriks = $this->db->order_by(COL_SKALAFROM, 'asc')->get(TBL_MMATRIKS)->result_array();
      $arrLabelMatriks = array();
      $arrDtMatriks1 = array();
      $arrDtMatriks2 = array();
      $arrBgMatriks = array();
      foreach($rmatriks as $r) {
        $arrLabelMatriks[] = $r[COL_KATEGORI];
        $numreg1 = $this->db
        ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
        ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
        ->where(TBL_TRISIKO.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO])
        ->where(TBL_TRISIKO.'.'.COL_TAHUN, date('Y'))
        ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
        ->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('TUJUAN','SASARAN','IKU'))
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." >= ", $r[COL_SKALAFROM])
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." <= ", $r[COL_SKALATO])
        ->count_all_results(TBL_TRISIKOREG);

        $numreg2 = $this->db
        ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
        ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
        ->where(TBL_TRISIKO.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO])
        ->where(TBL_TRISIKO.'.'.COL_TAHUN, date('Y'))
        ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
        ->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PROGRAM','KEGIATAN','SUBKEGIATAN'))
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." >= ", $r[COL_SKALAFROM])
        ->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO." <= ", $r[COL_SKALATO])
        ->count_all_results(TBL_TRISIKOREG);

        $arrDtMatriks1[] = $numreg1;
        $arrDtMatriks2[] = $numreg2;
        $arrBgMatriks[] = $r[COL_RGB];
      }
      $arrChartRisk1 = array(
        'labels'=>$arrLabelMatriks,
        'datasets'=>array(
          array(
            'data'=>$arrDtMatriks1,
            'backgroundColor'=>$arrBgMatriks
          )
        )
      );
      $arrChartRisk2 = array(
        'labels'=>$arrLabelMatriks,
        'datasets'=>array(
          array(
            'data'=>$arrDtMatriks2,
            'backgroundColor'=>$arrBgMatriks
          )
        )
      );
      ?>
      <div class="row">
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=date('Y')?></h3>
              <p class="font-italic">TAHUN PENILAIAN</p>
            </div>
            <div class="icon">
              <i class="far fa-building"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=count($rregister)?></h3>
              <p class="font-italic">RISK REGISTER</p>
            </div>
            <div class="icon">
              <i class="far fa-list"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=count($rriskevent)?></h3>
              <p class="font-italic">RISK EVENT</p>
            </div>
            <div class="icon">
              <i class="far fa-chart-bar"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title font-weight-bold">SKALA RISIKO STRATEGIS OPD</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-8">
                    <div class="chart-responsive">
                      <canvas id="chartRisk1" height="150"></canvas>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <ul class="nav nav-pills flex-column" style="border: 1px solid rgba(0,0,0,.125) !important">
                      <?php
                      $i=0;
                      foreach($rmatriks as $m) {
                        ?>
                        <li class="nav-item">
                          <p class="nav-link mb-0" style="color: <?=$m[COL_RGB]?>">
                            <?=$m[COL_KATEGORI]?>
                            <span class="float-right font-weight-bold"><?=$arrDtMatriks1[$i]?></span>
                          </p>
                        </li>
                        <?php
                        $i++;
                      }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title font-weight-bold">SKALA RISIKO OPERASIONAL OPD</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-8">
                    <div class="chart-responsive">
                      <canvas id="chartRisk2" height="150"></canvas>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <ul class="nav nav-pills flex-column" style="border: 1px solid rgba(0,0,0,.125) !important">
                      <?php
                      $i=0;
                      foreach($rmatriks as $m) {
                        ?>
                        <li class="nav-item">
                          <p class="nav-link mb-0" style="color: <?=$m[COL_RGB]?>">
                            <?=$m[COL_KATEGORI]?>
                            <span class="float-right font-weight-bold"><?=$arrDtMatriks2[$i]?></span>
                          </p>
                        </li>
                        <?php
                        $i++;
                      }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <?php
    }
    ?>

  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  <?php
  if(!empty($arrChartRisk1)) {
    ?>
    var canvasRisk1 = $('#chartRisk1').get(0).getContext('2d');
    var chartRisk1 = new Chart(canvasRisk1, {
      type: 'doughnut',
      data: <?=json_encode($arrChartRisk1)?>,
      options: {
        legend: {
          display: false
        }
      }
    });
    <?php
  }
  ?>
  <?php
  if(!empty($arrChartRisk2)) {
    ?>
    var canvasRisk2 = $('#chartRisk2').get(0).getContext('2d');
    var chartRisk2 = new Chart(canvasRisk2, {
      type: 'doughnut',
      data: <?=json_encode($arrChartRisk2)?>,
      options: {
        legend: {
          display: false
        }
      }
    });
    <?php
  }
  ?>
});
</script>
