<?php

?>
<form id="form-user" action="<?=current_url()?>" method="post">
  <div class="form-group">
    <label>Nama Lengkap</label>
    <input type="text" name="<?=COL_NAME?>" class="form-control" value="<?=!empty($data)?$data[COL_NAME]:''?>" required />
  </div>
  <div class="form-group">
    <label>Username / Email</label>
    <input type="text" name="<?=COL_EMAIL?>" class="form-control" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" required />
  </div>
  <div class="form-group">
    <label>Password</label>
    <input type="password" name="<?=COL_PASSWORD?>" class="form-control" />
    <?=!empty($data)?'<small id="info-password" class="text-muted font-italic">Diisi hanya jika ingin mengubah password</small>':''?>
  </div>
  <div class="form-group">
    <label>Kategori</label>
    <select name="<?=COL_ROLEID?>" class="form-control" style="width: 100% !important" required>
      <option value="<?=ROLEADMIN?>" <?=!empty($data)&&$data[COL_ROLEID]==ROLEADMIN?'selected':''?>>Administrator</option>
      <option value="<?=ROLEUSER?>" <?=!empty($data)&&$data[COL_ROLEID]==ROLEUSER?'selected':''?>>Operator</option>
    </select>
  </div>
  <div class="form-group" id="div-opd">
    <label>OPD</label>
    <select class="form-control" name="<?=COL_IDENTITYNO?>" style="width: 100%">
      <?=GetCombobox("select * from mopd where IsDeleted=0 order by OPDNama asc", COL_UNIQ, COL_OPDNAMA, (!empty($data)?$data[COL_IDENTITYNO]:null))?>
    </select>
  </div>
  <div class="form-group text-right mt-3">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $("select", $('#form-user')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $("[name=RoleID]", $('#form-user')).change(function(){
    var _role = $("[name=RoleID]", $('#form-user')).val();
    if(_role==<?=ROLEADMIN?>) {
      $("div#div-opd").hide();
    } else {
      $("div#div-opd").show();
    }
  }).trigger('change');
  $('#form-user').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
