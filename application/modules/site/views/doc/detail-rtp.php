<?php
$ruser = GetLoggedUser();
$rregpemda = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->join(TBL_TPMDIKU,TBL_TPMDIKU.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PMDIKU'))
->where("tpmdiku.IdSasaran in (select distinct(trenstrasasaran.IdSasaranPmd) from trenstrasasaran left join trenstratujuan on trenstratujuan.Uniq = trenstrasasaran.IdTujuan where trenstratujuan.IdRenstra = ".$data[COL_IDRENSTRA].")")
//->where(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();

$rreg1 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('TUJUAN','SASARAN','IKU'))
->where_in(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();

$rreg2 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PROGRAM','KEGIATAN','SUBKEGIATAN'))
->where_in(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();
?>
<?php
if($ruser[COL_ROLEID]==ROLEADMIN) {
  ?>
  <div class="form-group p-3 bg-light" style="border: 1px solid #dedede; border-radius: .5rem;">
    <h6 class="font-weight-bold mb-0">Status Dokumen</h6>
    <ul class="text-sm text-muted font-italic pl-4">
      <li><strong>BARU</strong> = Dokumen dalam tahap entri data oleh operator</li>
      <li><strong>VERIFIKASI</strong> = Dokumen sedang diperiksa / dievaluasi oleh APIP</li>
      <li><strong>FINAL</strong> = Dokumen selesai diperiksa / dievaluasi</li>
    </ul>
    <button type="button" class="btn btn-status btn<?=empty($data[COL_STATRTP])?'':'-outline'?>-secondary" style="opacity:1" <?=empty($data[COL_STATRTP])?'disabled':''?> data-url="<?=site_url('site/doc/status/StatRTP/'.$data[COL_UNIQ])?>" data-prompt="Apakah anda yakin mengubah status dokumen?">
      <i class="far fa<?=empty($data[COL_STATRTP])?'-check':''?>-circle"></i>&nbsp;BARU
    </button>
    <button type="button" class="btn btn-status btn<?=$data[COL_STATRTP]=='VERIFIKASI'?'':'-outline'?>-success" style="opacity:1" <?=$data[COL_STATRTP]=='VERIFIKASI'?'disabled':''?> data-url="<?=site_url('site/doc/status/StatRTP/'.$data[COL_UNIQ].'/verifikasi')?>" data-prompt="Apakah anda yakin mengubah status dokumen?">
      <i class="far fa<?=$data[COL_STATRTP]=='VERIFIKASI'?'-check':''?>-circle"></i>&nbsp;VERIFIKASI
    </button>
    <button type="button" class="btn btn-status btn<?=$data[COL_STATRTP]=='FINAL'?'':'-outline'?>-primary" style="opacity:1" <?=$data[COL_STATRTP]=='FINAL'?'disabled':''?> data-url="<?=site_url('site/doc/status/StatRTP/'.$data[COL_UNIQ].'/final')?>" data-prompt="Apakah anda yakin mengubah status dokumen?">
      <i class="far fa<?=$data[COL_STATRTP]=='FINAL'?'-check':''?>-circle"></i>&nbsp;FINAL
    </button>
  </div>
  <?php
}
?>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO STRATEGIS PEMDA</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm text-center">
            <th class="no-border text-center" style="width: 10px">No.</th>
            <th class="no-border">Uraian Pengendalian</th>
            <th class="no-border">Celah Pengendalian</th>
            <th class="no-border">RTP</th>
            <th class="no-border">Penanggung Jawab</th>
            <th class="no-border">Target Waktu</th>
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rregpemda)) {
            foreach($rregpemda as $reg) {
              $rrtp = $this->db
              ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
              ->get(TBL_TRISIKORTP)
              ->result_array();
              ?>
              <tr class="text-sm font-weight-bold bg-light">
                <td colspan="6">
                  <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
                </td>
                <td style="width: 10px; white-space: nowrap">
                  <a href="<?=site_url('site/doc/register-add-rtp/'.$reg[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-block btn-add">
                    <i class="fas fa-plus-circle"></i>&nbsp;RTP
                  </a>
                </td>
              </tr>
              <?php
              $no=1;
              foreach($rrtp as $rtp) {
                $numFeeds = $this->db
                ->where(COL_FEEDGROUP, 'RTP')
                ->where(COL_FEEDTYPE, 'COMMENT')
                ->where(COL_FEEDREF, $rtp[COL_UNIQ])
                ->where(COL_FEEDISREAD, 0)
                ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                ->count_all_results(TBL_TRISIKOFEEDS);
                ?>
                <tr class="text-sm">
                  <td><?=$no?></td>
                  <td><?=$rtp[COL_URAIANPENGENDALIAN]?></td>
                  <td><?=$rtp[COL_URAIANCELAH]?></td>
                  <td><?=$rtp[COL_URAIANRTP]?></td>
                  <td><?=$rtp[COL_RTPPENANGGUNGJAWAB]?></td>
                  <td><?=$rtp[COL_RTPWAKTU]?></td>
                  <td class="text-center" style="white-space: nowrap">
                    <a href="<?=site_url('site/doc/register-edit-rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/register-delete-rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/comment/rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                      <?php
                      if($numFeeds>0) {
                        ?>
                        <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                        <?php
                      }
                      ?>
                      <i class="fas fa-comment-alt-dots"></i>
                    </a>
                  </td>
                </tr>
                <?php
                $no++;
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="7" class="text-center font-italic text-danger">HARAP ISI REGISTER RISIKO STRATEGIS TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO STRATEGIS OPD</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm text-center">
            <th class="no-border text-center" style="width: 10px">No.</th>
            <th class="no-border">Uraian Pengendalian</th>
            <th class="no-border">Celah Pengendalian</th>
            <th class="no-border">RTP</th>
            <th class="no-border">Penanggung Jawab</th>
            <th class="no-border">Target Waktu</th>
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rreg1)) {
            foreach($rreg1 as $reg) {
              $rrtp = $this->db
              ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
              ->get(TBL_TRISIKORTP)
              ->result_array();
              ?>
              <tr class="text-sm font-weight-bold bg-light">
                <td colspan="6">
                  <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
                </td>
                <td style="width: 10px; white-space: nowrap">
                  <a href="<?=site_url('site/doc/register-add-rtp/'.$reg[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-block btn-add">
                    <i class="fas fa-plus-circle"></i>&nbsp;RTP
                  </a>
                </td>
              </tr>
              <?php
              $no=1;
              foreach($rrtp as $rtp) {
                $numFeeds = $this->db
                ->where(COL_FEEDGROUP, 'RTP')
                ->where(COL_FEEDTYPE, 'COMMENT')
                ->where(COL_FEEDREF, $rtp[COL_UNIQ])
                ->where(COL_FEEDISREAD, 0)
                ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                ->count_all_results(TBL_TRISIKOFEEDS);
                ?>
                <tr class="text-sm">
                  <td><?=$no?></td>
                  <td><?=$rtp[COL_URAIANPENGENDALIAN]?></td>
                  <td><?=$rtp[COL_URAIANCELAH]?></td>
                  <td><?=$rtp[COL_URAIANRTP]?></td>
                  <td><?=$rtp[COL_RTPPENANGGUNGJAWAB]?></td>
                  <td><?=$rtp[COL_RTPWAKTU]?></td>
                  <td class="text-center" style="white-space: nowrap">
                    <a href="<?=site_url('site/doc/register-edit-rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/register-delete-rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/comment/rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                      <?php
                      if($numFeeds>0) {
                        ?>
                        <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                        <?php
                      }
                      ?>
                      <i class="fas fa-comment-alt-dots"></i>
                    </a>
                  </td>
                </tr>
                <?php
                $no++;
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="7" class="text-center font-italic text-danger">HARAP ISI REGISTER RISIKO STRATEGIS TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO OPERASIONAL OPD</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm text-center">
            <th class="no-border text-center" style="width: 10px">No.</th>
            <th class="no-border">Uraian Pengendalian</th>
            <th class="no-border">Celah Pengendalian</th>
            <th class="no-border">RTP</th>
            <th class="no-border">Penanggung Jawab</th>
            <th class="no-border">Target Waktu</th>
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rreg2)) {
            foreach($rreg2 as $reg) {
              $rrtp = $this->db
              ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
              ->get(TBL_TRISIKORTP)
              ->result_array();
              ?>
              <tr class="text-sm font-weight-bold bg-light">
                <td colspan="6">
                  <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
                </td>
                <td style="width: 10px; white-space: nowrap">
                  <a href="<?=site_url('site/doc/register-add-rtp/'.$reg[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-block btn-add">
                    <i class="fas fa-plus-circle"></i>&nbsp;RTP
                  </a>
                </td>
              </tr>
              <?php
              $no=1;
              foreach($rrtp as $rtp) {
                $numFeeds = $this->db
                ->where(COL_FEEDGROUP, 'RTP')
                ->where(COL_FEEDTYPE, 'COMMENT')
                ->where(COL_FEEDREF, $rtp[COL_UNIQ])
                ->where(COL_FEEDISREAD, 0)
                ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                ->count_all_results(TBL_TRISIKOFEEDS);
                ?>
                <tr class="text-sm">
                  <td><?=$no?></td>
                  <td><?=$rtp[COL_URAIANPENGENDALIAN]?></td>
                  <td><?=$rtp[COL_URAIANCELAH]?></td>
                  <td><?=$rtp[COL_URAIANRTP]?></td>
                  <td><?=$rtp[COL_RTPPENANGGUNGJAWAB]?></td>
                  <td><?=$rtp[COL_RTPWAKTU]?></td>
                  <td class="text-center" style="white-space: nowrap">
                    <a href="<?=site_url('site/doc/register-edit-rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/register-delete-rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/comment/rtp/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                      <?php
                      if($numFeeds>0) {
                        ?>
                        <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                        <?php
                      }
                      ?>
                      <i class="fas fa-comment-alt-dots"></i>
                    </a>
                  </td>
                </tr>
                <?php
                $no++;
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="7" class="text-center font-italic text-danger">HARAP ISI REGISTER RISIKO STRATEGIS TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">RTP Risiko</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-feed" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Komentar / Feedback</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer d-block">
        <form id="form-feedback" action="" method="post">
          <div class="input-group">
            <textarea name="<?=COL_FEEDTEXT?>" placeholder="Komentar / Feedback" class="form-control"></textarea>
            <span class="input-group-append">
              <button type="submit" class="btn btn-primary">KIRIM&nbsp;<i class="far fa-paper-plane"></i></button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modalForm = $('#modal-form');
  var modalFeed = $('#modal-feed');

  $('#modal-form').on('hidden.bs.modal', function (e) {
    $(this).closest('#card-main').find('.nav-link.active').trigger('click');
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          $('#modal-form').modal('hide');
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var dis = $(this);
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          dis.closest('#card-main').find('.nav-link.active').trigger('click');
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });

  $('.btn-feed').click(function() {
    var href = $(this).attr('href');

    $('.modal-body', modalFeed).html('<p class="text-center">MEMUAT...</p>');
    modalFeed.modal('show');
    $('.modal-body', modalFeed).load(href, function(){
      $("select", modalFeed).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $("form", modalFeed).attr('action', href);
    });
    return false;
  });

  $('button[type=submit]', modalFeed).click(function(){
    var thisBtn = $(this);
    var thisForm = $(this).closest('form');

    if(!$("textarea", modalFeed).val()) {
      return false;
    }

    thisBtn.attr("disabled", true);
    $('form', modalFeed).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          $('.modal-body', modalFeed).html('<p>Memuat...</p>');
          $('.modal-body', modalFeed).load(thisForm.attr('action'), function(){
            $("select", modalFeed).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
          });
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        thisBtn.attr("disabled", false);
        $("textarea", modalFeed).val('');
      }
    });
  });

  modalFeed.on('hidden.bs.modal', function (e) {
    $(this).closest('#card-main').find('.nav-link.active').trigger('click');
  });

  $('.btn-status').click(function() {
    var thisBtn = $(this);
    var url = $(this).data('url');
    var prompt = ($(this).data('prompt') || 'Apakah anda yakin?');
    if(confirm(prompt)) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          thisBtn.closest('#card-main').find('.nav-link.active').trigger('click');
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
