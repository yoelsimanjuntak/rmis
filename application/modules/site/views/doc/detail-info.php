<?php
$ruser = GetLoggedUser();

$htmlStatus = '<span class="badge badge-secondary">BARU</span>';
if($data[COL_STATUS]=='VERIFIKASI') $htmlStatus = '<span class="badge badge-danger">VERIFIKASI</span>';
else if($data[COL_STATUS]=='FINAL') $htmlStatus = '<span class="badge badge-info">FINAL</span>';
?>
<div class="row">
  <div class="col-sm-8">
    <div class="card card-info">
      <div class="card-header">
        <div class="card-title"><h5 class="mb-0"><?=$data[COL_KETERANGAN]?></h5></div>
      </div>
      <div class="card-body p-0">
        <div class="table-responsive" style="border: 1px solid #dedede !important">
          <table class="table table-striped mb-0" style="border-top: none !important">
            <tr>
              <td style="border-top: 0 !important; width: 10px; white-space: nowrap">Judul</td>
              <td style="border-top: 0 !important; width: 10px; white-space: nowrap">:</td>
              <td style="border-top: 0 !important" class="font-weight-bold"><?=$data[COL_KETERANGAN]?></td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">OPD</td>
              <td style="width: 10px; white-space: nowrap">:</td>
              <td class="font-weight-bold"><?=$data[COL_OPDNAMA]?></td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">Periode Pemerintahan</td>
              <td style="width: 10px; white-space: nowrap">:</td>
              <td class="font-weight-bold"><?=$data[COL_PERIODNAMA]?></td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">Sumber Dokumen</td>
              <td style="width: 10px; white-space: nowrap">:</td>
              <td class="font-weight-bold"><?=$data[COL_RENSTRANAMA]?></td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">Tahun Penilaian</td>
              <td style="width: 10px; white-space: nowrap">:</td>
              <td class="font-weight-bold"><?=$data[COL_TAHUN]?></td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">Dibuat Pada</td>
              <td style="width: 10px; white-space: nowrap">:</td>
              <td class="font-weight-bold"><?=date('d-m-Y H:i', strtotime($data[COL_CREATEDON]))?></td>
            </tr>
          </table>
        </div>
      </div>
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        ?>
        <!--<div class="card-footer text-right">
          <a href="#" class="btn btn-sm btn-outline-info"><i class="fas fa-sync"></i>&nbsp; UBAH STATUS</a>
        </div>-->
        <?php
      }
      ?>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card card-info">
      <div class="card-header">
        <div class="card-title"><h5 class="mb-0">Status Dokumen</h5></div>
      </div>
      <div class="card-body p-0">
        <div class="table-responsive" style="border: 1px solid #dedede !important">
          <table class="table table-striped mb-0" style="border-top: none !important">
            <tr>
              <td style="border-top: 0 !important; width: 10px; white-space: nowrap">
                <div class="d-flex justify-content-between">
                  <strong>Risk Register</strong>
                  <span><?=!empty($data['regLabel'])?'<span class="badge bg-'.$data['regLabel'].'">'.$data['regIcon'].'&nbsp;'.$data['StatRegister'].'</span>':'-'?></span>
                </div>
              </td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">
                <div class="d-flex justify-content-between">
                  <strong>Risk Analysis</strong>
                  <span><?=!empty($data['analysisLabel'])?'<span class="badge bg-'.$data['analysisLabel'].'">'.$data['analysisIcon'].'&nbsp;'.$data['StatAnalisis'].'</span>':'-'?></span>
                </div>
              </td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">
                <div class="d-flex justify-content-between">
                  <strong>RTP</strong>
                  <span><?=!empty($data['rtpLabel'])?'<span class="badge bg-'.$data['rtpLabel'].'">'.$data['rtpIcon'].'&nbsp;'.$data['StatRTP'].'</span>':'-'?></span>
                </div>
              </td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">
                <div class="d-flex justify-content-between">
                  <strong>Infokom</strong>
                  <span><?=!empty($data['infokomLabel'])?'<span class="badge bg-'.$data['infokomLabel'].'">'.$data['infokomIcon'].'&nbsp;'.$data['StatInfokom'].'</span>':'-'?></span>
                </div>
              </td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">
                <div class="d-flex justify-content-between">
                  <strong>Rencana Monitoring</strong>
                  <span><?=!empty($data['monitorLabel'])?'<span class="badge bg-'.$data['monitorLabel'].'">'.$data['monitorIcon'].'&nbsp;'.$data['StatMonitoring'].'</span>':'-'?></span>
                </div>
              </td>
            </tr>
            <tr>
              <td style="width: 10px; white-space: nowrap">
                <div class="d-flex justify-content-between">
                  <strong>Risk Event</strong>
                  <span><?=!empty($data['eventLabel'])?'<span class="badge bg-'.$data['eventLabel'].'">'.$data['eventIcon'].'&nbsp;'.$data['StatEvent'].'</span>':'-'?></span>
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
