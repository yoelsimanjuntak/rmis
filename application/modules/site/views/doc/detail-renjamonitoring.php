<?php
$rmonitoring = $this->db
->select('trisikomonitoring.*')
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOMONITORING.".".COL_IDRISIKO,"inner")
->where(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->get(TBL_TRISIKOMONITORING)
->result_array();
?>
<div class="row">
  <div class="col-sm-12 mb-3">
    <a href="<?=site_url('site/doc/monitoring-add/'.$data[COL_UNIQ])?>" class="btn btn-info btn-sm btn-add">
      <i class="fas fa-plus-circle"></i>&nbsp;TAMBAH DATA
    </a>
  </div>
  <div class="col-sm-12">
    <div class="table-responsive">
      <table class="table table-bordered mb-0 text-sm" style="border-top: none !important">
        <thead>
          <tr>
            <th style="width: 10px">No</th>
            <th>Kegiatan</th>
            <th>Bentuk / Metode</th>
            <th>Penanggung Jawab</th>
            <th>Rencana Waktu Pelaksanaan</th>
            <th>Realisasi Waktu Pelaksanaan</th>
            <th>Keterangan</th>
            <th>Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rmonitoring)) {
            $no=1;
            foreach($rmonitoring as $r) {
              ?>
              <tr>
                <td style="width: 10px; white-space: nowrap"><?=$no?></td>
                <td><?=$r[COL_MONKEGIATAN]?></td>
                <td><?=$r[COL_MONMETODE]?></td>
                <td><?=$r[COL_MONPIC]?></td>
                <td><?=$r[COL_MONWAKTU]?></td>
                <td><?=$r[COL_MONREALISASI]?></td>
                <td><?=$r[COL_MONKETERANGAN]?></td>
                <td class="text-center" style="white-space: nowrap">
                  <a href="<?=site_url('site/doc/monitoring-edit/'.$r[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                  <a href="<?=site_url('site/doc/monitoring-delete/'.$r[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>
                </td>
              </tr>
              <?php
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="8" class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Monitoring atas Pengendalian Intern</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#modal-form').on('hidden.bs.modal', function (e) {
    $(this).closest('#card-main').find('.nav-link.active').trigger('click');
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          $('#modal-form').modal('hide');
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var dis = $(this);
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          dis.closest('#card-main').find('.nav-link.active').trigger('click');
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
