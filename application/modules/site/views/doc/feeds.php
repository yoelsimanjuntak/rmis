<?php
$ruser = GetLoggedUser();
?>
<div class="direct-chat-success direct-chat-messages">
  <?php
  if(!empty($feeds)) {
    foreach($feeds as $f) {
      if($f[COL_CREATEDBY]==$ruser[COL_USERNAME]) {
        ?>
        <div class="direct-chat-msg right">
          <div class="direct-chat-infos clearfix">
            <span class="direct-chat-name float-right"><?=$f[COL_NAME]?></span>
            <span class="direct-chat-timestamp float-left"><?=date('d-m-Y H:i', strtotime($f[COL_CREATEDON]))?></span>
          </div>
          <div class="direct-chat-text mr-0 text-right"><?=$f[COL_FEEDTEXT]?></div>
        </div>
        <?php
      } else {
        ?>
        <div class="direct-chat-msg">
          <div class="direct-chat-infos clearfix">
            <span class="direct-chat-name float-left"><?=$f[COL_NAME]?></span>
            <span class="direct-chat-timestamp float-right"><?=date('d-m-Y H:i', strtotime($f[COL_CREATEDON]))?></span>
          </div>
          <div class="direct-chat-text ml-0">
            <?=$f[COL_FEEDTEXT]?>
          </div>
        </div>
        <?php
      }
    }
  } else {
    ?>
    <p class="text-center text-muted font-italic">Belum ada riwayat.<p>
    <?php
  }
  ?>
</div>
