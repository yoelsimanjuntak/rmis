<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Uraian</label>
    <textarea name="<?=COL_RISIKOURAIAN?>" class="form-control" rows="3" placeholder="Uraian Risiko" required><?=!empty($data)?$data[COL_RISIKOURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>Kode</label>
        <input type="text" class="form-control" name="<?=COL_RISIKOKODE?>" value="<?=!empty($data)?$data[COL_RISIKOKODE]:''?>" placeholder="Kode Risiko" required />
      </div>
      <div class="col-sm-6">
        <label>Pemilik</label>
        <input type="text" class="form-control" name="<?=COL_RISIKOPEMILIK?>" value="<?=!empty($data)?$data[COL_RISIKOPEMILIK]:''?>" placeholder="Pemilik Risiko" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Penyebab</label>
    <textarea name="<?=COL_SEBABURAIAN?>" class="form-control" rows="3" placeholder="Uraian Penyebab" required><?=!empty($data)?$data[COL_SEBABURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>Sumber</label>
        <select class="form-control" name="<?=COL_SEBABSUMBER?>" style="width: 100%" required>
          <option value="INTERNAL" <?=!empty($data)&&$data[COL_SEBABSUMBER]=='INTERNAL'?'selected':''?>>Internal</option>
          <option value="EKSTERNAL" <?=!empty($data)&&$data[COL_SEBABSUMBER]=='EKSTERNAL'?'selected':''?>>Eksternal</option>
        </select>
      </div>
      <div class="col-sm-6">
        <label>C / UC</label>
        <select class="form-control" name="<?=COL_CONTROL?>" style="width: 100%" required>
          <option value="C" <?=!empty($data)&&$data[COL_CONTROL]=='C'?'selected':''?>>C (Controlled)</option>
          <option value="UC" <?=!empty($data)&&$data[COL_CONTROL]=='UC'?'selected':''?>>UC (Uncontrolled)</option>
        </select>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Dampak</label>
    <textarea name="<?=COL_DAMPAKURAIAN?>" class="form-control" rows="3" placeholder="Uraian Dampak" required><?=!empty($data)?$data[COL_DAMPAKURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Pihak yang Terkena</label>
    <input type="text" class="form-control" name="<?=COL_DAMPAKPIHAK?>" value="<?=!empty($data)?$data[COL_DAMPAKPIHAK]:''?>" placeholder="Pihak Terkena Dampak" required />
  </div>
</form>
