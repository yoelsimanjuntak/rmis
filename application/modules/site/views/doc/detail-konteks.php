<?php
$ruser = GetLoggedUser();

$rpmdtujuan = $this->db
->select('trisikokonteks.*, tpmdtujuan.Uniq as IdTujuanPmd, tpmdtujuan.Uraian as TujuanPmd')
->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PMDTUJUAN')
->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
->order_by(TBL_TPMDTUJUAN.'.'.COL_IDMISI)
->order_by(TBL_TPMDTUJUAN.'.'.COL_UNIQ)
->get(TBL_TRISIKOKONTEKS)
->result_array();

$rtujuan = $this->db
->select('trisikokonteks.*, trenstratujuan.Uniq as IdTujuanRenstra, trenstratujuan.Uraian TujuanRenstra, tpmdtujuan.Uraian as TujuanPmd')
->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDTUJUANPMD,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'TUJUAN')
->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
->order_by(TBL_TPMDTUJUAN.'.'.COL_IDMISI)
->order_by(TBL_TPMDTUJUAN.'.'.COL_UNIQ)
->get(TBL_TRISIKOKONTEKS)
->result_array();

$rprogram = $this->db
->select('trisikokonteks.*, trenjaprogram.ProgKode, trenjaprogram.Uniq as IdProgram')
->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PROGRAM')
->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
->order_by(TBL_TRENJAPROGRAM.'.'.COL_PROGKODE)
->get(TBL_TRISIKOKONTEKS)
->result_array();
?>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">KONTEKS RISIKO STRATEGIS PEMDA</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr>
            <th class="no-border text-center" style="width: 10px">No</th>
            <th class="no-border">Tujuan / Sasaran</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rpmdtujuan)) {
            $no=1;
            foreach($rpmdtujuan as $t) {
              $rsasaran = $this->db
              ->select('trisikokonteks.*, tpmdsasaran.Uniq as IdSasaranPmd, tpmdsasaran.Uraian as SasaranPmd')
              ->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PMDSASARAN')
              ->where(TBL_TPMDSASARAN.'.'.COL_IDTUJUAN, $t['IdTujuanPmd'])
              ->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
              ->get(TBL_TRISIKOKONTEKS)
              ->result_array();
              ?>
              <tr class="bg-light">
                <td class="text-left" style="width: 10px"><?=$no?></td>
                <td class="font-weight-bold">
                  <?=$t['TujuanPmd']?>
                </td>
              </tr>
              <?php

              $no_=1;
              foreach($rsasaran as $s) {
                $riku = $this->db
                ->select('trisikokonteks.*')
                ->join(TBL_TPMDIKU,TBL_TPMDIKU.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PMDIKU')
                ->where(TBL_TPMDIKU.'.'.COL_IDSASARAN, $s['IdSasaranPmd'])
                ->where(TBL_TPMDIKU.'.'.COL_ISDELETED, 0)
                ->get(TBL_TRISIKOKONTEKS)
                ->result_array();
                ?>
                <tr>
                  <td class="text-left" style="width: 10px" <?=count($riku)>0?'rowspan="'.(count($riku)+1).'"':''?>><?=$no.'.'.$no_?></td>
                  <td class="pl-4">
                    <?=$s['SasaranPmd']?>
                  </td>
                </tr>
                <?php
                foreach($riku as $iku) {
                  ?>
                  <tr>
                    <td class="text-sm font-italic" style="padding-left: 2.5rem !important"><?=$iku[COL_KONTEKSURAIAN].(!empty($iku[COL_KONTEKSTARGET])?' - '.$iku[COL_KONTEKSTARGET].' ('.$iku[COL_KONTEKSSATUAN].')':'')?></td>
                  </tr>
                  <?php
                }
                $no_++;
              }
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="3" class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">KONTEKS RISIKO STRATEGIS OPD</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr>
            <th class="no-border text-center" style="width: 10px">No</th>
            <th class="no-border">Tujuan / Sasaran</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rtujuan)) {
            $no=1;
            foreach($rtujuan as $t) {
              $rsasaran = $this->db
              ->select('trisikokonteks.*, trenstrasasaran.Uniq as IdSasaranRenstra, trenstrasasaran.Uraian as SasaranRenstra, tpmdsasaran.Uraian as SasaranPmd')
              ->join(TBL_TRENSTRASASARAN,TBL_TRENSTRASASARAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
              ->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDSASARANPMD,"inner")
              ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SASARAN')
              ->where(TBL_TRENSTRASASARAN.'.'.COL_IDTUJUAN, $t['IdTujuanRenstra'])
              ->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
              ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
              ->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
              ->get(TBL_TRISIKOKONTEKS)
              ->result_array();
              ?>
              <tr class="bg-light">
                <td class="text-left" style="width: 10px"><?=$no?></td>
                <td>
                  <?=$t['TujuanPmd'].'&nbsp;<span class="pl-2 pr-2"><i class="fas fa-arrow-alt-right"></i></span>&nbsp;<strong>'.$t[COL_KONTEKSURAIAN].'</strong>'?>
                </td>
              </tr>
              <?php

              $no_=1;
              foreach($rsasaran as $s) {
                $riku = $this->db
                ->select('trisikokonteks.*')
                ->join(TBL_TRENSTRASASARANDET,TBL_TRENSTRASASARANDET.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'IKU')
                ->where(TBL_TRENSTRASASARANDET.'.'.COL_IDSASARAN, $s['IdSasaranRenstra'])
                ->where(TBL_TRENSTRASASARANDET.'.'.COL_ISDELETED, 0)
                ->get(TBL_TRISIKOKONTEKS)
                ->result_array();
                ?>
                <tr>
                  <td class="text-left" style="width: 10px" <?=count($riku)>0?'rowspan="'.(count($riku)+1).'"':''?>><?=$no.'.'.$no_?></td>
                  <td class="pl-4">
                    <?=$s['SasaranPmd'].'&nbsp;<span class="pl-2 pr-2"><i class="fas fa-arrow-alt-right"></i></span>&nbsp;<strong>'.$s[COL_KONTEKSURAIAN].'</strong>'?>
                  </td>
                </tr>
                <?php
                foreach($riku as $iku) {
                  ?>
                  <tr>
                    <td class="text-sm font-italic" style="padding-left: 2.5rem !important"><?=$iku[COL_KONTEKSURAIAN].(!empty($iku[COL_KONTEKSTARGET])?' - '.$iku[COL_KONTEKSTARGET].' ('.$iku[COL_KONTEKSSATUAN].')':'')?></td>
                  </tr>
                  <?php
                }
                $no_++;
              }
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="3" class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">KONTEKS RISIKO OPERASIONAL OPD</h6></div>
    <div class="card-tools">
      <!--<a href="<?=site_url('site/doc/konteks-add-program/'.$data[COL_UNIQ])?>" class="btn btn-tool btn-add"><i class="fas fa-plus-circle"></i>&nbsp; TAMBAH</a>-->
      <a href="<?=site_url('site/doc/konteks-add-operasional/'.$data[COL_UNIQ])?>" class="btn btn-tool btn-add"><i class="fas fa-plus-circle"></i>&nbsp; TAMBAH</a>
    </div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered mb-0" style="border-top: none !important">
        <thead>
          <tr>
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Kode Rekening</th>
            <th class="no-border">Program / Kegiatan / Sub Kegiatan</th>
            <th class="no-border text-center" style="width: 100px">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rprogram)) {
            foreach ($rprogram as $p) {
              $rkegiatan = $this->db
              ->select('trisikokonteks.*, trenjakegiatan.KegKode, trenjakegiatan.Uniq as IdKegiatan')
              ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'KEGIATAN')
              ->where(TBL_TRENJAKEGIATAN.'.'.COL_IDPROGRAM, $p[COL_IDPROGRAM])
              ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
              ->order_by(TBL_TRENJAKEGIATAN.'.'.COL_KEGKODE)
              ->get(TBL_TRISIKOKONTEKS)
              ->result_array();
              ?>
              <tr class="bg-light" style="color: #28a745 !important">
                <th class="text-left" style="width: 10px"><?=$p[COL_PROGKODE]?></th>
                <th>
                  <?=$p[COL_KONTEKSURAIAN]?>
                </th>
                <th class="text-left" style="width: 100px; white-space: nowrap">
                  <!--<a href="<?=site_url('site/doc/konteks-edit-program/'.$p[COL_UNIQ])?>" class="btn btn-info btn-sm btn-edit">
                    <i class="fas fa-edit"></i>&nbsp;UBAH
                  </a>-->
                  <a href="<?=site_url('site/doc/konteks-delete-program/'.$p[COL_UNIQ])?>" class="btn btn-danger btn-sm btn-delete">
                    <i class="fas fa-trash"></i>&nbsp;HAPUS
                  </a>
                  <!--<a href="<?=site_url('site/doc/konteks-add-kegiatan/'.$p[COL_UNIQ])?>" class="btn btn-success btn-sm btn-add">
                    <i class="fas fa-plus-circle"></i>&nbsp;RINCIAN
                  </a>-->
                </th>
              </tr>
              <?php
              foreach($rkegiatan as $k) {
                $rsubkegiatan = $this->db
                ->select('trisikokonteks.*, trenjakegiatansub.SubKode')
                ->join(TBL_TRENJAKEGIATANSUB,TBL_TRENJAKEGIATANSUB.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SUBKEGIATAN')
                ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_IDKEGIATAN, $k[COL_IDKEGIATAN])
                ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
                ->order_by(TBL_TRENJAKEGIATANSUB.'.'.COL_SUBKODE)
                ->get(TBL_TRISIKOKONTEKS)
                ->result_array();
                ?>
                <tr class="text-blue">
                  <td class="text-left" style="width: 10px"><?=$k[COL_KEGKODE]?></td>
                  <td>
                    <?=$k[COL_KONTEKSURAIAN]?><br />
                    <small class="font-italic pl-3"><?=$k[COL_KONTEKSINDIKATOR]?> - <?=$k[COL_KONTEKSTARGET]?> (<?=$k[COL_KONTEKSSATUAN]?>)</small>
                  </td>
                  <td class="text-left" style="width: 100px; white-space: nowrap">
                    <!--<a href="<?=site_url('site/doc/konteks-edit-kegiatan/'.$k[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                      <i class="fas fa-edit"></i>&nbsp;UBAH
                    </a>-->
                    <a href="<?=site_url('site/doc/konteks-delete-kegiatan/'.$k[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                      <i class="fas fa-trash"></i>&nbsp;HAPUS
                    </a>
                    <!--<a href="<?=site_url('site/doc/konteks-add-subkegiatan/'.$k[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-add">
                      <i class="fas fa-plus-circle"></i>&nbsp;RINCIAN
                    </a>-->
                  </td>
                </tr>
                <?php
                foreach($rsubkegiatan as $s) {
                  ?>
                  <tr>
                    <td class="text-left" style="width: 10px"><?=$s[COL_SUBKODE]?></td>
                    <td>
                      <?=$s[COL_KONTEKSURAIAN]?><br />
                      <small class="font-italic pl-3"><?=$s[COL_KONTEKSINDIKATOR]?> - <?=$s[COL_KONTEKSTARGET]?> (<?=$s[COL_KONTEKSSATUAN]?>)</small>
                    </td>
                    <td class="text-left" style="width: 100px; white-space: nowrap">
                      <!--<a href="<?=site_url('site/doc/konteks-edit-subkegiatan/'.$s[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-edit">
                        <i class="fas fa-edit"></i>&nbsp;UBAH
                      </a>-->
                      <a href="<?=site_url('site/doc/konteks-delete-subkegiatan/'.$s[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete">
                        <i class="fas fa-trash"></i>&nbsp;HAPUS
                      </a>
                    </td>
                  </tr>
                  <?php
                }
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="3" class="text-center font-italic">(KOSONG)</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Konteks Risiko</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#modal-form').on('hidden.bs.modal', function (e) {
    $(this).closest('#card-main').find('.nav-link.active').trigger('click');
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          $('#modal-form').modal('hide');
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var dis = $(this);
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          dis.closest('#card-main').find('.nav-link.active').trigger('click');
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
