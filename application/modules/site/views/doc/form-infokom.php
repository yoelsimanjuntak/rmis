<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Kegiatan</label>
    <textarea name="<?=COL_KOMKEGIATAN?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_KOMKEGIATAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Media / Bentuk Pengkomunikasian</label>
    <input type="text" class="form-control" name="<?=COL_KOMMEDIA?>" value="<?=!empty($data)?$data[COL_KOMMEDIA]:''?>" />
  </div>
  <div class="form-group">
    <label>Penyedia Informasi</label>
    <input type="text" class="form-control" name="<?=COL_KOMPENYEDIA?>" value="<?=!empty($data)?$data[COL_KOMPENYEDIA]:''?>" />
  </div>
  <div class="form-group">
    <label>Penerima Informasi</label>
    <input type="text" class="form-control" name="<?=COL_KOMPENERIMA?>" value="<?=!empty($data)?$data[COL_KOMPENERIMA]:''?>" />
  </div>
  <div class="form-group">
    <label>Rencana Waktu Pelaksanaan</label>
    <input type="text" class="form-control" name="<?=COL_KOMWAKTU?>" value="<?=!empty($data)?$data[COL_KOMWAKTU]:''?>" />
  </div>
  <div class="form-group">
    <label>Realisasi Waktu Pelaksanaan</label>
    <input type="text" class="form-control" name="<?=COL_KOMREALISASI?>" value="<?=!empty($data)?$data[COL_KOMREALISASI]:''?>" />
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea name="<?=COL_KOMKETERANGAN?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_KOMKETERANGAN]:''?></textarea>
  </div>
</form>
