<style>
.nav-tabs .nav-link {
  background: #fff !important;
  border: 0 !important;
  border-top-left-radius: .5rem !important;
  border-top-right-radius: .5rem !important;
  margin: 0 3px !important;
}
.nav-tabs .nav-link.active {
  background: #17a2b8 !important;
  color: #fff !important;
  font-weight: bold !important;
}
.nav-tabs .nav-item {
  margin-bottom: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-md-6 col-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <div class="btn-group">
          <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="far fa-print"></i>&nbsp;CETAK&nbsp;<span class="caret"></span>
          </button>
          <div class="dropdown-menu" x-placement="bottom-start">
            <a class="dropdown-item" href="<?=site_url('site/doc/cetak/konteks/'.$data[COL_UNIQ])?>" target="_blank">Konteks Risiko</a>
            <a class="dropdown-item" href="<?=site_url('site/doc/cetak/register/'.$data[COL_UNIQ])?>" target="_blank">Register Risiko</a>
            <a class="dropdown-item" href="<?=site_url('site/doc/cetak/analisis/'.$data[COL_UNIQ])?>" target="_blank">Analisis Risiko</a>
            <a class="dropdown-item" href="<?=site_url('site/doc/cetak/rtp/'.$data[COL_UNIQ])?>" target="_blank">RTP</a>
            <!--<a class="dropdown-item" href="<?=site_url('site/doc/cetak/infokom/'.$data[COL_UNIQ])?>" target="_blank">Infokom</a>
            <a class="dropdown-item" href="<?=site_url('site/doc/cetak/monitoring/'.$data[COL_UNIQ])?>" target="_blank">Rencana Monitoring</a>
            <a class="dropdown-item" href="<?=site_url('site/doc/cetak/log/'.$data[COL_UNIQ])?>" target="_blank">Risk Event</a>-->
          </div>
        </div>
        <?=anchor('site/doc/index','<i class="far fa-arrow-circle-left"></i> KEMBALI',array('class'=>'btn btn-secondary btn-sm'))?>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div id="card-main" class="card card-default bg-transparent" style="box-shadow: 0 0 0 rgba(0,0,0,.125),0 1px 0 rgba(0,0,0,.2) !important">
          <div class="card-header d-flex p-0" style="border-bottom: 0 !important">
            <ul class="nav nav-tabs" style="border-bottom: 0 !important;">
              <li class="nav-item"><a class="nav-link active" href="#tab-main" data-url="<?=current_url().'?tab=info'?>" data-toggle="tab" style="margin-left: 0 !important">I. Rincian</a></li>
              <li class="nav-item"><a class="nav-link" href="#tab-main" data-url="<?=current_url().'?tab=konteks'?>" data-toggle="tab">II. Konteks</a></li>
              <li class="nav-item"><a class="nav-link" href="#tab-main" data-url="<?=current_url().'?tab=register'?>" data-toggle="tab">III. Identifikasi</a></li>
              <li class="nav-item"><a class="nav-link" href="#tab-main" data-url="<?=current_url().'?tab=analisis'?>" data-toggle="tab">IV. Analisis</a></li>
              <li class="nav-item"><a class="nav-link" href="#tab-main" data-url="<?=current_url().'?tab=rtp'?>" data-toggle="tab">V. RTP</a></li>
              <li class="nav-item"><a class="nav-link" href="#tab-main" data-url="<?=current_url().'?tab=infokom'?>" data-toggle="tab">VI. Infokom</a></li>
              <li class="nav-item"><a class="nav-link" href="#tab-main" data-url="<?=current_url().'?tab=renmonitor'?>" data-toggle="tab">VII. Rencana Monitoring</a></li>
              <li class="nav-item"><a class="nav-link" href="#tab-main" data-url="<?=current_url().'?tab=monitoring'?>" data-toggle="tab">VIII. Risk Event</a></li>
            </ul>
          </div>
          <div class="card-body bg-white">
            <div class="tab-content">
              <div id="tab-main" class="tab-pane active">

              </div>
            </div>
          </div>
          <div class="overlay">
            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('.nav-link', $('#card-main')).click(function(){
    var url = $(this).data('url');

    $('div.overlay', $('#card-main')).show();
    $('#tab-main', $('#card-main')).load(url, function(){
      $('div.overlay', $('#card-main')).hide();
    });
  });

  $('.nav-link.active', $('#card-main')).trigger('click');
});
</script>
