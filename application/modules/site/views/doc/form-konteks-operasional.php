<?php
$idRenstra = $rrisiko[COL_IDRENSTRA];
$idRisiko = $rrisiko[COL_UNIQ];
$Tahun = $rrisiko[COL_TAHUN];
$rOptSasaran = $this->db->query("
select
  trenstrasasaran.*
from trenstrasasaran
left join trenstratujuan on trenstratujuan.Uniq = trenstrasasaran.IdTujuan
where trenstratujuan.IdRenstra = $idRenstra and trenstrasasaran.IsDeleted=0 and trenstratujuan.IsDeleted=0
order by trenstratujuan.Uniq, trenstrasasaran.Uniq asc"
)->result_array();
if(empty($rOptSasaran)) {
  echo 'Harap mengisi data SASARAN terlebih dahulu!';
  exit();
}

$rOptProgram = $this->db->query("
select
  trenjaprogram.*
from trenjaprogram
where
  trenjaprogram.IdRenstra = $idRenstra
  and trenjaprogram.Tahun = $Tahun
  and trenjaprogram.IsDeleted=0
  and trenjaprogram.Uniq not in (select trisikokonteks.IdRef from trisikokonteks where trisikokonteks.IdRisiko=$idRisiko and trisikokonteks.Level = 'PROGRAM')
order by trenjaprogram.ProgKode asc"
)->result_array();
?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Program</label>
    <select class="form-control" name="<?=COL_IDPROGRAM?>" style="width: 100%" required>
      <?php
      foreach($rOptProgram as $opt) {
        ?>
        <option value="<?=$opt[COL_UNIQ]?>"><?=$opt[COL_PROGKODE].' - '.$opt[COL_PROGURAIAN]?></option>
        <?php
      }
      ?>
    </select>
  </div>
</form>
