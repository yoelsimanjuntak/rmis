<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <div class="row">
      <div class="col-sm-4">
        <label>Tanggal</label>
        <input type="text" class="form-control datepicker" name="<?=COL_LOGTANGGAL?>" value="<?=!empty($data)?$data[COL_LOGTANGGAL]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Sebab</label>
    <textarea name="<?=COL_LOGSEBAB?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_LOGSEBAB]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Dampak</label>
    <textarea name="<?=COL_LOGDAMPAK?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_LOGDAMPAK]:''?></textarea>
  </div>
  <div class="form-group">
    <label>RTP</label>
    <textarea name="<?=COL_LOGRTP?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_LOGRTP]:''?></textarea>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>Rencana</label>
        <input type="text" class="form-control" name="<?=COL_LOGRTPWAKTU?>" value="<?=!empty($data)?$data[COL_LOGRTPWAKTU]:''?>" placeholder="Rencana RTP" />
      </div>
      <div class="col-sm-6">
        <label>Realisasi</label>
        <input type="text" class="form-control" name="<?=COL_LOGRTPREALISASI?>" value="<?=!empty($data)?$data[COL_LOGRTPREALISASI]:''?>" placeholder="Realisasi RTP" />
      </div>
    </div>
  </div>
</form>
