<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Kegiatan</label>
    <textarea name="<?=COL_MONKEGIATAN?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_MONKEGIATAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Metode / Bentuk Pemantauan</label>
    <input type="text" class="form-control" name="<?=COL_MONMETODE?>" value="<?=!empty($data)?$data[COL_MONMETODE]:''?>" />
  </div>
  <div class="form-group">
    <label>Penanggung Jawab</label>
    <input type="text" class="form-control" name="<?=COL_MONPIC?>" value="<?=!empty($data)?$data[COL_MONPIC]:''?>" />
  </div>
  <div class="form-group">
    <label>Rencana Waktu Pelaksanaan</label>
    <input type="text" class="form-control" name="<?=COL_MONWAKTU?>" value="<?=!empty($data)?$data[COL_MONWAKTU]:''?>" />
  </div>
  <div class="form-group">
    <label>Realisasi Waktu Pelaksanaan</label>
    <input type="text" class="form-control" name="<?=COL_MONREALISASI?>" value="<?=!empty($data)?$data[COL_MONREALISASI]:''?>" />
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea name="<?=COL_MONKETERANGAN?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_MONKETERANGAN]:''?></textarea>
  </div>
</form>
