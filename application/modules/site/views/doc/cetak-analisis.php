<?php
$rreg1 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('TUJUAN','SASARAN','IKU'))
->where(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();

$rreg2 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PROGRAM','KEGIATAN','SUBKEGIATAN'))
->where(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();

$rmatriks = $this->db
->order_by(COL_SKALAFROM, 'desc')
->get(TBL_MMATRIKS)
->result_array();
?>
<head>
  <html>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: .75rem;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
    font-size: 10pt !important;
  }
  .font-weight-bold {
    font-weight: bold;
  }
  </style>
</head>
<body>
  <h5 style="margin: 0 !important; text-align: center">HASIL ANALISIS RISIKO</h5>
  <h4 style="margin: 0 !important; text-align: center"><?=strtoupper($data[COL_OPDNAMA])?></h4>
  <h5 style="margin-top: 0 !important; text-align: center"><?=$data[COL_TAHUN]?></h5>
  <table style="margin-bottom: 40px">
    <thead>
      <tr>
        <th colspan="5" style="text-align: left;">1. RISIKO STRATEGIS</th>
      </tr>
      <tr class="text-sm text-center">
        <th class="no-border text-center" style="width: 100px">Kode</th>
        <th class="no-border">Uraian Risiko</th>
        <th class="no-border" style="width: 100px;">Skala Dampak</th>
        <th class="no-border" style="width: 100px;">Skala Kemungkinan</th>
        <th class="no-border" style="width: 100px;">Skala Risiko</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rreg1)) {
        foreach($rreg1 as $reg) {
          $bgClass = '';
          foreach($rmatriks as $r) {
            if(!empty($reg) && ($reg[COL_NUMRISIKO]>=$r[COL_SKALAFROM] && $reg[COL_NUMRISIKO]<=$r[COL_SKALATO])) {
              $bgClass = $r[COL_RGB];
              break;
            }
          }
          ?>
          <tr>
            <td><?=$reg[COL_RISIKOKODE]?></td>
            <td><?=$reg[COL_RISIKOURAIAN]?></td>
            <td style="text-align: center">
              <?=!empty($reg[COL_NUMDAMPAK])?'<span class="font-weight-bold">'.number_format($reg[COL_NUMDAMPAK]).'</span>':'<span class="text-danger font-italic">(kosong)</span>'?>
            </td>
            <td style="text-align: center">
              <?=!empty($reg[COL_NUMFREQ])?'<span class="font-weight-bold">'.number_format($reg[COL_NUMFREQ]).'</span>':'<span class="text-danger font-italic">(kosong)</span>'?>
            </td>
            <td style="color: <?=$bgClass?>; text-align: center">
              <?=!empty($reg[COL_NUMRISIKO])?'<span class="font-weight-bold">'.number_format($reg[COL_NUMRISIKO]).'</span>':'<span class="text-danger font-italic">(kosong)</span>'?>
            </td>
          </tr>
          <?php
        }
      } else {
        ?>
        <tr>
          <td colspan="5" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>

  <table style="margin-bottom: 40px">
    <thead>
      <tr>
        <th colspan="5" style="text-align: left;">2. RISIKO OPERASIONAL</th>
      </tr>
      <tr class="text-sm text-center">
        <th class="no-border text-center" style="width: 100px">Kode</th>
        <th class="no-border">Uraian Risiko</th>
        <th class="no-border" style="width: 100px;">Skala Dampak</th>
        <th class="no-border" style="width: 100px;">Skala Kemungkinan</th>
        <th class="no-border" style="width: 100px;">Skala Risiko</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rreg2)) {
        foreach($rreg2 as $reg) {
          $bgClass = '';
          foreach($rmatriks as $r) {
            if(!empty($reg) && ($reg[COL_NUMRISIKO]>=$r[COL_SKALAFROM] && $reg[COL_NUMRISIKO]<=$r[COL_SKALATO])) {
              $bgClass = $r[COL_RGB];
              break;
            }
          }
          ?>
          <tr>
            <td><?=$reg[COL_RISIKOKODE]?></td>
            <td><?=$reg[COL_RISIKOURAIAN]?></td>
            <td style="text-align: center">
              <?=!empty($reg[COL_NUMDAMPAK])?'<span class="font-weight-bold">'.number_format($reg[COL_NUMDAMPAK]).'</span>':'<span class="text-danger font-italic">(kosong)</span>'?>
            </td>
            <td style="text-align: center">
              <?=!empty($reg[COL_NUMFREQ])?'<span class="font-weight-bold">'.number_format($reg[COL_NUMFREQ]).'</span>':'<span class="text-danger font-italic">(kosong)</span>'?>
            </td>
            <td style="color: <?=$bgClass?>; text-align: center">
              <?=!empty($reg[COL_NUMRISIKO])?'<span class="font-weight-bold">'.number_format($reg[COL_NUMRISIKO]).'</span>':'<span class="text-danger font-italic">(kosong)</span>'?>
            </td>
          </tr>
          <?php
        }
      } else {
        ?>
        <tr>
          <td colspan="5" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <br />
  <table width="100%" style="border: 0 !important">
    <tr>
      <td style="border: 0 !important; width: 100px; white-space: nowrap">
        Tebing Tinggi,
      </td>
      <td style="border: 0 !important; white-space: nowrap; padding-left: 100px;">
        <?=date("Y")?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; font-weight: bold">
        <?=$data[COL_OPDPIMPINANJAB]?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; padding-top: 100px !important;">
        <span style="font-weight: bold; text-decoration: underline"><?=$data[COL_OPDPIMPINAN]?></span><br />
        NIP. <?=$data[COL_OPDPIMPINANNIP]?>
      </td>
    </tr>
  </table>
</body>
