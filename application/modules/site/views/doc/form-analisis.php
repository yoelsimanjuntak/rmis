<div class="row mb-3" style="margin-left: -15px !important; margin-right: -15px !important; margin-top: -15px !important">
  <div class="col-sm-12 p-3 bg-light">
    <p class="font-weight-bold font-italic mb-0" style="text-decoration: underline">
      <?=$data[COL_RISIKOKODE]?>
    </p>
    <p class="font-italic text-sm">
      <?=$data[COL_RISIKOURAIAN]?>
    </p>
  </div>
</div>

<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Skala Dampak</label>
    <input type="number" class="form-control" name="<?=COL_NUMDAMPAK?>" value="<?=!empty($data)?$data[COL_NUMDAMPAK]:''?>" placeholder="1 - 5" required />
  </div>
  <div class="form-group">
    <label>Skala Kemungkinan</label>
    <input type="number" class="form-control" name="<?=COL_NUMFREQ?>" value="<?=!empty($data)?$data[COL_NUMFREQ]:''?>" placeholder="1 - 5" required />
  </div>
  <div class="form-group">
    <label>Skala Risiko</label>
    <input type="number" class="form-control" name="<?=COL_NUMRISIKO?>" value="<?=!empty($data)?$data[COL_NUMFREQ]:''?>" placeholder="1 - 25" required readonly />
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $('[name=NumDampak],[name=NumFreq]', $('#form-main')).change(function(){
    var numDampak = $('[name=NumDampak]').val();
    var numFreq = $('[name=NumFreq]').val();
    var numRisiko = numDampak*numFreq;

    $('[name=NumRisiko]', $('#form-main')).val(numRisiko);
  });
});
</script>
