<?php
$ruser = GetLoggedUser();

$rsasaran = $this->db
->select('trisikokonteks.*, trenstrasasaran.Uniq as IdSasaranRenstra, trenstrasasaran.Uraian as SasaranRenstra, tpmdsasaran.Uraian as SasaranPmd')
->join(TBL_TRENSTRASASARAN,TBL_TRENSTRASASARAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDSASARANPMD,"inner")
->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SASARAN')
->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
->get(TBL_TRISIKOKONTEKS)
->result_array();

$rprogram = $this->db
->select('trisikokonteks.*, trenjaprogram.ProgKode, trenjaprogram.Uniq as IdProgram')
->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PROGRAM')
->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
->order_by(TBL_TRENJAPROGRAM.'.'.COL_PROGKODE)
->get(TBL_TRISIKOKONTEKS)
->result_array();
?>
<head>
  <html>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: .75rem;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
    font-size: 10pt !important;
  }
  </style>
</head>
<body>
  <h5 style="margin: 0 !important; text-align: center">IDENTIFIKASI RISIKO</h5>
  <h4 style="margin: 0 !important; text-align: center"><?=strtoupper($data[COL_OPDNAMA])?></h4>
  <h5 style="margin-top: 0 !important; text-align: center"><?=$data[COL_TAHUN]?></h5>
  <table style="margin-bottom: 40px">
    <thead>
      <tr>
        <th colspan="8" style="text-align: left;">1. RISIKO STRATEGIS</th>
      </tr>
      <tr class="text-sm">
        <th class="no-border text-center" style="width: 10px">Kode</th>
        <th class="no-border">Uraian Risiko</th>
        <th class="no-border">Pemilik</th>
        <th class="no-border">Uraian Sebab</th>
        <th class="no-border">Sumber</th>
        <th class="no-border" style="white-space: nowrap">C / UC</th>
        <th class="no-border">Uraian Dampak</th>
        <th class="no-border">Pihak Terdampak</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rsasaran)) {
        $no=1;
        foreach($rsasaran as $s) {
          $riku = $this->db
          ->select('trisikokonteks.*')
          ->join(TBL_TRENSTRASASARANDET,TBL_TRENSTRASASARANDET.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'IKU')
          ->where(TBL_TRENSTRASASARANDET.'.'.COL_IDSASARAN, $s['IdSasaranRenstra'])
          ->where(TBL_TRENSTRASASARANDET.'.'.COL_ISDELETED, 0)
          ->get(TBL_TRISIKOKONTEKS)
          ->result_array();
          ?>
          <tr style="background-color: #deefff!important">
            <td colspan="8">
              <strong><?=$s[COL_KONTEKSURAIAN]?></strong>
            </td>
          </tr>
          <?php
          foreach($riku as $iku) {
            $rreg = $this->db
            ->where(COL_IDKONTEKS, $iku[COL_UNIQ])
            ->get(TBL_TRISIKOREG)
            ->result_array();
            ?>
            <tr style="background-color: #deefff!important">
              <td colspan="8">
                <?=$iku[COL_KONTEKSURAIAN].(!empty($iku[COL_KONTEKSTARGET])?' - '.$iku[COL_KONTEKSTARGET].' ('.$iku[COL_KONTEKSSATUAN].')':'')?>
              </td>
            </tr>
            <?php
            if(!empty($rreg)) {
              foreach($rreg as $reg) {
                ?>
                <tr style="font-style: italic">
                  <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                  <td><?=$reg[COL_RISIKOURAIAN]?></td>
                  <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                  <td><?=$reg[COL_SEBABURAIAN]?></td>
                  <td><?=$reg[COL_SEBABSUMBER]?></td>
                  <td style="text-align: center"><?=$reg[COL_CONTROL]?></td>
                  <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                  <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                </tr>
                <?php
              }
            }
          }
          $no++;
        }
      } else {
        ?>
        <tr>
          <td colspan="8" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>

  <table>
    <thead>
      <tr>
        <th colspan="8" style="text-align: left;">2. RISIKO OPERASIONAL</th>
      </tr>
      <tr class="text-sm">
        <th class="no-border text-center" style="width: 10px">Kode</th>
        <th class="no-border">Uraian Risiko</th>
        <th class="no-border">Pemilik</th>
        <th class="no-border">Uraian Sebab</th>
        <th class="no-border">Sumber</th>
        <th class="no-border" style="white-space: nowrap">C / UC</th>
        <th class="no-border">Uraian Dampak</th>
        <th class="no-border">Pihak Terdampak</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rprogram)) {
        foreach ($rprogram as $p) {
          $rkegiatan = $this->db
          ->select('trisikokonteks.*, trenjakegiatan.KegKode, trenjakegiatan.Uniq as IdKegiatan')
          ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'KEGIATAN')
          ->where(TBL_TRENJAKEGIATAN.'.'.COL_IDPROGRAM, $p[COL_IDPROGRAM])
          ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
          ->order_by(TBL_TRENJAKEGIATAN.'.'.COL_KEGKODE)
          ->get(TBL_TRISIKOKONTEKS)
          ->result_array();

          $rreg = $this->db
          ->where(COL_IDKONTEKS, $p[COL_UNIQ])
          ->get(TBL_TRISIKOREG)
          ->result_array();
          ?>
          <tr style="background-color: #deefff!important">
            <td style="font-weight: bold" colspan="8">
              <?=$p[COL_KONTEKSURAIAN]?>
            </td>
          </tr>
          <?php
          if(!empty($rreg)) {
            foreach($rreg as $reg) {
              ?>
              <tr style="font-style: italic">
                <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                <td><?=$reg[COL_RISIKOURAIAN]?></td>
                <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                <td><?=$reg[COL_SEBABURAIAN]?></td>
                <td><?=$reg[COL_SEBABSUMBER]?></td>
                <td style="text-align: center"><?=$reg[COL_CONTROL]?></td>
                <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                <td><?=$reg[COL_DAMPAKPIHAK]?></td>
              </tr>
              <?php
            }
          }
          foreach($rkegiatan as $k) {
            $rsubkegiatan = $this->db
            ->select('trisikokonteks.*, trenjakegiatansub.SubKode')
            ->join(TBL_TRENJAKEGIATANSUB,TBL_TRENJAKEGIATANSUB.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
            ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
            ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SUBKEGIATAN')
            ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_IDKEGIATAN, $k[COL_IDKEGIATAN])
            ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
            ->order_by(TBL_TRENJAKEGIATANSUB.'.'.COL_SUBKODE)
            ->get(TBL_TRISIKOKONTEKS)
            ->result_array();

            $rreg = $this->db
            ->where(COL_IDKONTEKS, $k[COL_UNIQ])
            ->get(TBL_TRISIKOREG)
            ->result_array();
            ?>
            <tr style="background-color: #deefff!important">
              <td colspan="8">
                <?=$k[COL_KONTEKSURAIAN]?> - <?=$k[COL_KONTEKSTARGET]?> (<?=$k[COL_KONTEKSSATUAN]?>)
              </td>
            </tr>
            <?php
            if(!empty($rreg)) {
              foreach($rreg as $reg) {
                ?>
                <tr style="font-style: italic">
                  <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                  <td><?=$reg[COL_RISIKOURAIAN]?></td>
                  <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                  <td><?=$reg[COL_SEBABURAIAN]?></td>
                  <td><?=$reg[COL_SEBABSUMBER]?></td>
                  <td style="text-align: center"><?=$reg[COL_CONTROL]?></td>
                  <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                  <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                </tr>
                <?php
              }
            }

            foreach($rsubkegiatan as $s) {
              $rreg = $this->db
              ->where(COL_IDKONTEKS, $s[COL_UNIQ])
              ->get(TBL_TRISIKOREG)
              ->result_array();

              ?>
              <tr style="background-color: #deefff!important">
                <td colspan="8">
                  <?=$s[COL_KONTEKSURAIAN]?> - <?=$s[COL_KONTEKSTARGET]?> (<?=$s[COL_KONTEKSSATUAN]?>)
                </td>
              </tr>
              <?php
              if(!empty($rreg)) {
                foreach($rreg as $reg) {
                  ?>
                  <tr style="font-style: italic">
                    <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                    <td><?=$reg[COL_RISIKOURAIAN]?></td>
                    <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                    <td><?=$reg[COL_SEBABURAIAN]?></td>
                    <td><?=$reg[COL_SEBABSUMBER]?></td>
                    <td style="text-align: center"><?=$reg[COL_CONTROL]?></td>
                    <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                    <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                  </tr>
                  <?php
                }
              }
            }
          }
        }
      } else {
        ?>
        <tr>
          <td colspan="8" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <br />
  <table width="100%" style="border: 0 !important">
    <tr>
      <td style="border: 0 !important; width: 100px; white-space: nowrap">
        Tebing Tinggi,
      </td>
      <td style="border: 0 !important; white-space: nowrap; padding-left: 100px;">
        <?=date("Y")?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; font-weight: bold">
        <?=$data[COL_OPDPIMPINANJAB]?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; padding-top: 100px !important;">
        <span style="font-weight: bold; text-decoration: underline"><?=$data[COL_OPDPIMPINAN]?></span><br />
        NIP. <?=$data[COL_OPDPIMPINANNIP]?>
      </td>
    </tr>
  </table>
</body>
