<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Uraian</label>
    <textarea name="<?=COL_KONTEKSURAIAN?>" class="form-control" rows="3" placeholder="Uraian Sub Kegiatan" required><?=!empty($data)?$data[COL_KONTEKSURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Kode</label>
    <input type="text" class="form-control" name="<?=COL_SUBKODE?>" value="<?=!empty($data)?$data[COL_SUBKODE]:''?>" placeholder="Kode Rekening" required />
  </div>
  <div class="form-group">
    <label>Indikator</label>
    <input type="text" class="form-control" name="<?=COL_KONTEKSINDIKATOR?>" value="<?=!empty($data)?$data[COL_KONTEKSINDIKATOR]:''?>" placeholder="Indikator Keberhasilan" required />
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-8">
        <label>Satuan</label>
        <input type="text" class="form-control" name="<?=COL_KONTEKSSATUAN?>" value="<?=!empty($data)?$data[COL_KONTEKSSATUAN]:''?>" placeholder="Satuan" required />
      </div>
      <div class="col-sm-4">
        <label>Target</label>
        <input type="number" class="form-control" name="<?=COL_KONTEKSTARGET?>" value="<?=!empty($data)?$data[COL_KONTEKSTARGET]:''?>" placeholder="Target" required />
      </div>
    </div>
  </div>
</form>
