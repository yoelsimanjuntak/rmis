<?php
$rregpemda = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->join(TBL_TPMDIKU,TBL_TPMDIKU.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PMDIKU'))
->where("tpmdiku.IdSasaran in (select distinct(trenstrasasaran.IdSasaranPmd) from trenstrasasaran left join trenstratujuan on trenstratujuan.Uniq = trenstrasasaran.IdTujuan where trenstratujuan.IdRenstra = ".$data[COL_IDRENSTRA].")")
//->where(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();

$rreg1 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('TUJUAN','SASARAN','IKU'))
->where_in(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();

$rreg2 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PROGRAM','KEGIATAN','SUBKEGIATAN'))
->where_in(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();
?>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO STRATEGIS PEMDA</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm text-center">
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Tanggal</th>
            <th class="no-border">Sebab</th>
            <th class="no-border">Dampak</th>
            <th class="no-border">RTP</th>
            <th class="no-border">Target Waktu RTP</th>
            <th class="no-border">Realisasi RTP</th>
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rregpemda)) {
            foreach($rregpemda as $reg) {
              $rrtp = $this->db
              ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
              ->order_by(COL_LOGTANGGAL, 'desc')
              ->get(TBL_TRISIKOLOG)
              ->result_array();
              ?>
              <tr class="text-sm font-weight-bold bg-light">
                <td colspan="6">
                  <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
                </td>
                <td style="width: 10px; white-space: nowrap">
                  <a href="<?=site_url('site/doc/register-add-log/'.$reg[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-block btn-add">
                    <i class="fas fa-plus-circle"></i>&nbsp;RISK EVENT
                  </a>
                </td>
              </tr>
              <?php
              $no=1;
              foreach($rrtp as $rtp) {
                ?>
                <tr class="text-sm">
                  <td class="text-center" style="white-space: nowrap"><?=$rtp[COL_LOGTANGGAL]?></td>
                  <td><?=$rtp[COL_LOGSEBAB]?></td>
                  <td><?=$rtp[COL_LOGDAMPAK]?></td>
                  <td><?=$rtp[COL_LOGRTP]?></td>
                  <td><?=$rtp[COL_LOGRTPWAKTU]?></td>
                  <td><?=$rtp[COL_LOGRTPREALISASI]?></td>
                  <td class="text-center" style="white-space: nowrap">
                    <a href="<?=site_url('site/doc/register-edit-log/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/register-delete-log/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>
                  </td>
                </tr>
                <?php
                $no++;
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="7" class="text-center font-italic text-danger">HARAP ISI REGISTER RISIKO STRATEGIS TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO STRATEGIS OPD</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm text-center">
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Tanggal</th>
            <th class="no-border">Sebab</th>
            <th class="no-border">Dampak</th>
            <th class="no-border">RTP</th>
            <th class="no-border">Target Waktu RTP</th>
            <th class="no-border">Realisasi RTP</th>
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rreg1)) {
            foreach($rreg1 as $reg) {
              $rrtp = $this->db
              ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
              ->order_by(COL_LOGTANGGAL, 'desc')
              ->get(TBL_TRISIKOLOG)
              ->result_array();
              ?>
              <tr class="text-sm font-weight-bold bg-light">
                <td colspan="6">
                  <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
                </td>
                <td style="width: 10px; white-space: nowrap">
                  <a href="<?=site_url('site/doc/register-add-log/'.$reg[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-block btn-add">
                    <i class="fas fa-plus-circle"></i>&nbsp;RISK EVENT
                  </a>
                </td>
              </tr>
              <?php
              $no=1;
              foreach($rrtp as $rtp) {
                ?>
                <tr class="text-sm">
                  <td class="text-center" style="white-space: nowrap"><?=$rtp[COL_LOGTANGGAL]?></td>
                  <td><?=$rtp[COL_LOGSEBAB]?></td>
                  <td><?=$rtp[COL_LOGDAMPAK]?></td>
                  <td><?=$rtp[COL_LOGRTP]?></td>
                  <td><?=$rtp[COL_LOGRTPWAKTU]?></td>
                  <td><?=$rtp[COL_LOGRTPREALISASI]?></td>
                  <td class="text-center" style="white-space: nowrap">
                    <a href="<?=site_url('site/doc/register-edit-log/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/register-delete-log/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>
                  </td>
                </tr>
                <?php
                $no++;
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="7" class="text-center font-italic text-danger">HARAP ISI REGISTER RISIKO STRATEGIS TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO OPERASIONAL OPD</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm text-center">
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Tanggal</th>
            <th class="no-border">Sebab</th>
            <th class="no-border">Dampak</th>
            <th class="no-border">RTP</th>
            <th class="no-border">Target Waktu RTP</th>
            <th class="no-border">Realisasi RTP</th>
            <th class="no-border text-center" style="width: 10px; white-space: nowrap">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rreg2)) {
            foreach($rreg2 as $reg) {
              $rrtp = $this->db
              ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
              ->order_by(COL_LOGTANGGAL, 'desc')
              ->get(TBL_TRISIKOLOG)
              ->result_array();
              ?>
              <tr class="text-sm font-weight-bold bg-light">
                <td colspan="6">
                  <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
                </td>
                <td style="width: 10px; white-space: nowrap">
                  <a href="<?=site_url('site/doc/register-add-log/'.$reg[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-block btn-add">
                    <i class="fas fa-plus-circle"></i>&nbsp;RISK EVENT
                  </a>
                </td>
              </tr>
              <?php
              $no=1;
              foreach($rrtp as $rtp) {
                ?>
                <tr class="text-sm">
                  <td class="text-center" style="white-space: nowrap"><?=$rtp[COL_LOGTANGGAL]?></td>
                  <td><?=$rtp[COL_LOGSEBAB]?></td>
                  <td><?=$rtp[COL_LOGDAMPAK]?></td>
                  <td><?=$rtp[COL_LOGRTP]?></td>
                  <td><?=$rtp[COL_LOGRTPWAKTU]?></td>
                  <td><?=$rtp[COL_LOGRTPREALISASI]?></td>
                  <td class="text-center" style="white-space: nowrap">
                    <a href="<?=site_url('site/doc/register-edit-log/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                    <a href="<?=site_url('site/doc/register-delete-log/'.$rtp[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>
                  </td>
                </tr>
                <?php
                $no++;
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="7" class="text-center font-italic text-danger">HARAP ISI REGISTER RISIKO STRATEGIS TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Risk Event</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#modal-form').on('hidden.bs.modal', function (e) {
    $(this).closest('#card-main').find('.nav-link.active').trigger('click');
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          $('#modal-form').modal('hide');
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var dis = $(this);
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          dis.closest('#card-main').find('.nav-link.active').trigger('click');
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
