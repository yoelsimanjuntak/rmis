<?php
$ruser = GetLoggedUser();
$rdata = $this->db
->select("trisiko.*, mopd.OPDNama, trenstra.RenstraNama")
->join(TBL_MOPD,TBL_MOPD.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDOPD,"inner")
->join(TBL_TRENSTRA,TBL_TRENSTRA.'.'.COL_UNIQ." = ".TBL_TRISIKO.".".COL_IDRENSTRA,"inner")
->join(TBL_TPMDPERIODE,TBL_TPMDPERIODE.'.'.COL_UNIQ." = ".TBL_TRENSTRA.".".COL_IDPERIODE,"inner")
->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRA.'.'.COL_ISDELETED, 0)
->where(TBL_MOPD.'.'.COL_ISDELETED, 0)
->where(TBL_TPMDPERIODE.'.'.COL_ISDELETED, 0)
->where(TBL_TRISIKO.'.'.COL_IDOPD, $ruser[COL_IDENTITYNO])
->order_by(TBL_MOPD.'.'.COL_OPDNAMA, 'asc')
->order_by(TBL_TRISIKO.'.'.COL_TAHUN, 'asc')
->order_by(TBL_TRISIKO.'.'.COL_KETERANGAN, 'asc')
->get(TBL_TRISIKO)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-md-6 col-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <p class="mb-0">
          <a href="<?=site_url('site/doc/add')?>" type="button" class="btn btn-sm btn-add btn-info"><i class="fas fa-plus-circle"></i>&nbsp;TAMBAH DATA</a>
        </p>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if(!empty($rdata)) {
        foreach($rdata as $dat) {
          $htmlStatus = 'secondary';
          if($dat[COL_STATUS]=='VERIFIKASI') $htmlStatus = 'danger';
          else if($dat[COL_STATUS]=='FINAL') $htmlStatus = 'info';

          $rkonteks = $this->db
          ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
          ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
          ->where(TBL_TRISIKO.'.'.COL_UNIQ, $dat[COL_UNIQ])
          ->get(TBL_TRISIKOKONTEKS)
          ->result_array();

          $rregister = $this->db
          ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
          ->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
          ->where(TBL_TRISIKO.'.'.COL_ISDELETED, 0)
          ->where(TBL_TRISIKO.'.'.COL_UNIQ, $dat[COL_UNIQ])
          ->get(TBL_TRISIKOREG)
          ->result_array();
          ?>
          <div class="col-sm-6">
            <div class="card card-info">
              <div class="card-header">
                <h5 class="card-title font-weight-bold"><?=$dat[COL_KETERANGAN]?></h5>
                <div class="card-tools">
                  <a href="<?=site_url('site/doc/edit/'.$dat[COL_UNIQ])?>" class="btn btn-tool btn-edit">
                    <i class="fas fa-edit"></i>
                  </a>
                  <a href="<?=site_url('site/doc/detail/'.$dat[COL_UNIQ])?>" class="btn btn-tool">
                    <i class="fas fa-search"></i>
                  </a>
                  <a href="<?=site_url('site/doc/delete/'.$dat[COL_UNIQ])?>" class="btn btn-tool btn-action">
                    <i class="fas fa-trash"></i>
                  </a>
                </div>
              </div>
              <div class="card-body p-0">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <span class="nav-link">
                      <i>Tahun</i> <span class="font-weight-bold pull-right"><?=$dat[COL_TAHUN]?></span>
                    </span>
                  </li>
                  <li class="nav-item">
                    <span class="nav-link">
                      <i>Sumber Dokumen</i> <span class="font-weight-bold pull-right"><?=$dat[COL_RENSTRANAMA]?></span>
                    </span>
                  </li>
                  <li class="nav-item">
                    <span class="nav-link">
                      <i>Status</i> <span class="badge badge-<?=$htmlStatus?> pull-right"><?=$dat[COL_STATUS]?></span>
                    </span>
                  </li>
                  <li class="nav-item">
                    <span class="nav-link">
                      <i>Konteks Risiko</i> <span class="badge badge-info pull-right"><?=number_format(count($rkonteks))?></span>
                    </span>
                  </li>
                  <li class="nav-item">
                    <span class="nav-link">
                      <i>Register Risiko</i> <span class="badge badge-info pull-right"><?=number_format(count($rregister))?></span>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-sm-12">
          <p class="text-center font-italic">TIDAK ADA DATA TERSEDIA</p>
        </div>

        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Dokumen Manajemen Risiko</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('.btn-action').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          setTimeout(function(){
            location.reload();
          },2000);
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    var modal = $('#modal-form');
    dis.html("Loading...").attr("disabled", true);
    $('form', modal).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        //dt.DataTable().ajax.reload();
        setTimeout(function(){
          location.reload();
        },2000);
        if(data.error==0) {

        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        modal.modal('hide');
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
});
</script>
