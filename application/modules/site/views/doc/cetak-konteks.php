<?php
$rtujuan = $this->db
->select('trisikokonteks.*, trenstratujuan.Uniq as IdTujuanRenstra, trenstratujuan.Uraian TujuanRenstra, tpmdtujuan.Uraian as TujuanPmd')
->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->join(TBL_TPMDTUJUAN,TBL_TPMDTUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRATUJUAN.".".COL_IDTUJUANPMD,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'TUJUAN')
->where(TBL_TPMDTUJUAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
->order_by(TBL_TPMDTUJUAN.'.'.COL_IDMISI)
->order_by(TBL_TPMDTUJUAN.'.'.COL_UNIQ)
->get(TBL_TRISIKOKONTEKS)
->result_array();

$rprogram = $this->db
->select('trisikokonteks.*, trenjaprogram.ProgKode, trenjaprogram.Uniq as IdProgram')
->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PROGRAM')
->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
->order_by(TBL_TRENJAPROGRAM.'.'.COL_PROGKODE)
->get(TBL_TRISIKOKONTEKS)
->result_array();
?>
<head>
  <html>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: .75rem;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
    font-size: 10pt !important;
  }
  </style>
</head>
<body>
  <h5 style="margin: 0 !important; text-align: center">PENETAPAN KONTEKS RISIKO</h5>
  <h4 style="margin: 0 !important; text-align: center"><?=strtoupper($data[COL_OPDNAMA])?></h4>
  <h5 style="margin-top: 0 !important; text-align: center"><?=$data[COL_TAHUN]?></h5>
  <table style="margin-bottom: 40px">
    <thead>
      <tr>
        <th colspan="2" style="text-align: left;">1. RISIKO STRATEGIS</th>
      </tr>
      <tr>
        <th style="width: 10px">No</th>
        <th>Tujuan / Sasaran</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rtujuan)) {
        $no=1;
        foreach($rtujuan as $t) {
          $rsasaran = $this->db
          ->select('trisikokonteks.*, trenstrasasaran.Uniq as IdSasaranRenstra, trenstrasasaran.Uraian as SasaranRenstra, tpmdsasaran.Uraian as SasaranPmd')
          ->join(TBL_TRENSTRASASARAN,TBL_TRENSTRASASARAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
          ->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDSASARANPMD,"inner")
          ->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SASARAN')
          ->where(TBL_TRENSTRASASARAN.'.'.COL_IDTUJUAN, $t['IdTujuanRenstra'])
          ->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
          ->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
          ->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
          ->get(TBL_TRISIKOKONTEKS)
          ->result_array();
          ?>
          <tr style="background-color: #deefff!important">
            <td style="width: 10px; text-align: left"><?=$no?></td>
            <td>
              <?='<strong>'.$t[COL_KONTEKSURAIAN].'</strong>'?>
            </td>
          </tr>
          <?php

          $no_=1;
          foreach($rsasaran as $s) {
            $riku = $this->db
            ->select('trisikokonteks.*')
            ->join(TBL_TRENSTRASASARANDET,TBL_TRENSTRASASARANDET.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
            ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
            ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'IKU')
            ->where(TBL_TRENSTRASASARANDET.'.'.COL_IDSASARAN, $s['IdSasaranRenstra'])
            ->where(TBL_TRENSTRASASARANDET.'.'.COL_ISDELETED, 0)
            ->get(TBL_TRISIKOKONTEKS)
            ->result_array();
            ?>
            <tr>
              <td style="width: 10px; text-align: left" <?=count($riku)>0?'rowspan="'.(count($riku)+1).'"':''?>><?=$no.'.'.$no_?></td>
              <td style="padding-left: 2rem !important">
                <?='<strong>'.$s[COL_KONTEKSURAIAN].'</strong>'?>
              </td>
            </tr>
            <?php
            foreach($riku as $iku) {
              ?>
              <tr>
                <td class="text-sm font-italic" style="padding-left: 2.5rem !important"><?=$iku[COL_KONTEKSURAIAN].(!empty($iku[COL_KONTEKSTARGET])?' - '.$iku[COL_KONTEKSTARGET].' ('.$iku[COL_KONTEKSSATUAN].')':'')?></td>
              </tr>
              <?php
            }
            $no_++;
          }
          $no++;
        }
      } else {
        ?>
        <tr>
          <td colspan="3" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>

  <table style="margin-bottom: 40px">
    <thead>
      <tr>
        <th colspan="2" style="text-align: left;">2. RISIKO OPERASIONAL</th>
      </tr>
      <tr>
        <th style="width: 10px; white-space: nowrap">Kode Rekening</th>
        <th>Program / Kegiatan / Sub Kegiatan</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rprogram)) {
        foreach ($rprogram as $p) {
          $rkegiatan = $this->db
          ->select('trisikokonteks.*, trenjakegiatan.KegKode, trenjakegiatan.Uniq as IdKegiatan')
          ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
          ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'KEGIATAN')
          ->where(TBL_TRENJAKEGIATAN.'.'.COL_IDPROGRAM, $p[COL_IDPROGRAM])
          ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
          ->order_by(TBL_TRENJAKEGIATAN.'.'.COL_KEGKODE)
          ->get(TBL_TRISIKOKONTEKS)
          ->result_array();
          ?>
          <tr style="background-color: #deefff!important">
            <td style="font-weight: bold; width: 10px; white-space: nowrap;"><?=$p[COL_PROGKODE]?></td>
            <td style="font-weight: bold">
              <?=$p[COL_KONTEKSURAIAN]?>
            </td>
          </tr>
          <?php
          foreach($rkegiatan as $k) {
            $rsubkegiatan = $this->db
            ->select('trisikokonteks.*, trenjakegiatansub.SubKode')
            ->join(TBL_TRENJAKEGIATANSUB,TBL_TRENJAKEGIATANSUB.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
            ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
            ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SUBKEGIATAN')
            ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_IDKEGIATAN, $k[COL_IDKEGIATAN])
            ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
            ->order_by(TBL_TRENJAKEGIATANSUB.'.'.COL_SUBKODE)
            ->get(TBL_TRISIKOKONTEKS)
            ->result_array();
            ?>
            <tr>
              <td style="width: 10px; white-space: nowrap"><?=$k[COL_KEGKODE]?></td>
              <td>
                <?=$k[COL_KONTEKSURAIAN]?><br />
                <small style="font-style: italic; padding-left: 1.5rem"><?=$k[COL_KONTEKSINDIKATOR]?> - <?=$k[COL_KONTEKSTARGET]?> (<?=$k[COL_KONTEKSSATUAN]?>)</small>
              </td>
            </tr>
            <?php
            foreach($rsubkegiatan as $s) {
              ?>
              <tr>
                <td style="width: 10px; white-space: nowrap"><?=$s[COL_SUBKODE]?></td>
                <td>
                  <?=$s[COL_KONTEKSURAIAN]?><br />
                  <small style="font-style: italic; padding-left: 1.5rem"><?=$s[COL_KONTEKSINDIKATOR]?> - <?=$s[COL_KONTEKSTARGET]?> (<?=$s[COL_KONTEKSSATUAN]?>)</small>
                </td>
              </tr>
              <?php
            }
          }
        }
      } else {
        ?>
        <tr>
          <td colspan="3" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <br />
  <table width="100%" style="border: 0 !important">
    <tr>
      <td style="border: 0 !important; width: 100px; white-space: nowrap">
        Tebing Tinggi,
      </td>
      <td style="border: 0 !important; white-space: nowrap; padding-left: 100px;">
        <?=date("Y")?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; font-weight: bold">
        <?=$data[COL_OPDPIMPINANJAB]?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; padding-top: 100px !important;">
        <span style="font-weight: bold; text-decoration: underline"><?=$data[COL_OPDPIMPINAN]?></span><br />
        NIP. <?=$data[COL_OPDPIMPINANNIP]?>
      </td>
    </tr>
  </table>
</body>
