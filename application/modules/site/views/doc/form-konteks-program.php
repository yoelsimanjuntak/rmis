<?php
$idRenstra = $rrisiko[COL_IDRENSTRA];
$rOptSasaran = $this->db->query("
select
  trenstrasasaran.*
from trenstrasasaran
left join trenstratujuan on trenstratujuan.Uniq = trenstrasasaran.IdTujuan
where trenstratujuan.IdRenstra = $idRenstra and trenstrasasaran.IsDeleted=0 and trenstratujuan.IsDeleted=0
order by trenstratujuan.Uniq, trenstrasasaran.Uniq asc"
)->result_array();
if(empty($rOptSasaran)) {
  echo 'Harap mengisi data SASARAN terlebih dahulu!';
  exit();
}
?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Sasaran SKPD</label>
    <select class="form-control" name="<?=COL_IDSASARANSKPD?>" style="width: 100%" required>
      <?php
      foreach($rOptSasaran as $opt) {
        ?>
        <option value="<?=$opt[COL_UNIQ]?>" <?=(!empty($data)&&$data[COL_IDSASARANSKPD]==$opt[COL_UNIQ]?'selected':'')?>><?=$opt[COL_URAIAN]?></option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>Uraian</label>
    <textarea name="<?=COL_KONTEKSURAIAN?>" class="form-control" rows="3" placeholder="Uraian Program" required><?=!empty($data)?$data[COL_KONTEKSURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-8">
        <label>Kode</label>
        <input type="text" class="form-control" name="<?=COL_PROGKODE?>" value="<?=!empty($data)?$data[COL_PROGKODE]:''?>" placeholder="Kode Rekening" required />
      </div>
      <div class="col-sm-4">
        <label>Tahun</label>
        <input type="number" class="form-control" name="<?=COL_TAHUN?>" value="<?=!empty($data)?$data[COL_TAHUN]:$rrisiko[COL_TAHUN]?>" placeholder="Tahun" required readonly />
      </div>
    </div>
  </div>
</form>
