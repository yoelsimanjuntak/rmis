<?php
$ruser = GetLoggedUser();
?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Dokumen RPJMD</label>
    <select class="form-control" name="<?=COL_IDPERIODE?>" style="width: 100%" <?=(!empty($data)?'disabled':'')?>>
      <?=GetCombobox("select * from tpmdperiode where IsDeleted=0 and PeriodIsAktif=1 order by PeriodFrom desc", COL_UNIQ, COL_PERIODNAMA, (!empty($data)?$data[COL_IDPERIODE]:null))?>
    </select>
  </div>
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    ?>
    <div class="form-group">
      <label>OPD</label>
      <select class="form-control" name="<?=COL_IDOPD?>" style="width: 100%" <?=(!empty($data)?'disabled':'')?>>
        <?=GetCombobox("select * from mopd where IsDeleted=0 order by OPDNama asc", COL_UNIQ, COL_OPDNAMA, (!empty($data)?$data[COL_IDOPD]:null))?>
      </select>
    </div>
    <?php
  } else {
    ?>
    <input type="hidden" name="<?=COL_IDOPD?>" value="<?=$ruser[COL_IDENTITYNO]?>" />
    <?php
  }
  ?>
  <div class="form-group">
    <label>Dokumen Renstra</label>
    <select class="form-control" name="<?=COL_IDRENSTRA?>" style="width: 100%" <?=(!empty($data)?'disabled':'')?>>
      <?php
      if(!empty($data)) {
        echo GetCombobox("select * from trenstra where IdOPD = ".$data[COL_IDOPD]." and IsDeleted=0 order by Tahun desc", COL_UNIQ, COL_RENSTRANAMA, (!empty($data)?$data[COL_IDRENSTRA]:null));
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-8">
        <label>Judul</label>
        <input type="text" class="form-control" name="<?=COL_KETERANGAN?>" value="<?=!empty($data)?$data[COL_KETERANGAN]:''?>" placeholder="Judul Dokumen" required />
      </div>
      <div class="col-sm-4">
        <label>Tahun</label>
        <input type="number" class="form-control" name="<?=COL_TAHUN?>" value="<?=!empty($data)?$data[COL_TAHUN]:''?>" placeholder="Tahun" required />
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('[name=IdPeriode],[name=IdOPD]', $('#form-main')).change(function() {
    var valPeriod = $('[name=IdPeriode]', $('#form-main')).val();
    var valOpd = $('[name=IdOPD]', $('#form-main')).val();

    $('[name=IdRenstra]', $('#form-main')).load('<?=site_url('site/perencanaan/opt-renstra-by-rpjmd')?>', {IdPeriode: valPeriod, IdOPD: valOpd}, function(){
      $('[name=IdRenstra]').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
  });

  <?php
  if(empty($data)) {
    ?>
    $('[name=IdOPD]', $('#form-main')).trigger('change');
    <?php
  }
  ?>

});
</script>
