<?php
$ruser = GetLoggedUser();
$rperiod = $this->db
->where(COL_ISDELETED, 0)
->where(COL_PERIODISAKTIF, 1)
->order_by(COL_PERIODFROM,'desc')
->get(TBL_TPMDPERIODE)
->result_array();

$ropd = $this->db
->where(COL_ISDELETED, 0)
->order_by(COL_OPDNAMA,'asc')
->get(TBL_MOPD)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-md-6 col-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/doc/add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH DATA</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">AKSI</th>
                    <th>OPD</th>
                    <th>TAHUN</th>
                    <th>JUDUL</th>
                    <th>STATUS</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter-period" class="d-none">
  <select class="form-control" name="filterPeriod" style="width: 25vw">
    <?php
    foreach($rperiod as $p) {
      ?>
      <option value="<?=$p[COL_UNIQ]?>"><?=$p[COL_PERIODNAMA]?></option>
      <?php
    }
    ?>
  </select>
</div>
<div id="dom-filter-opd" class="d-none">
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    ?>
    <select class="form-control" name="filterOPD" style="width: 40vw">
      <option value="">- SEMUA OPD -</option>
      <?php
      foreach($ropd as $opd) {
        ?>
        <option value="<?=$opd[COL_UNIQ]?>"><?=$opd[COL_OPDNAMA]?></option>
        <?php
      }
      ?>
    </select>
    <?php
  } else {
    $ropd = $this->db
    ->where(COL_UNIQ, $ruser[COL_IDENTITYNO])
    ->get(TBL_MOPD)
    ->row_array();
    ?>
    <input type="hidden" name="filterOPD" value="<?=$ruser[COL_IDENTITYNO]?>" />
    <input type="text" class="form-control" value="<?=!empty($ropd)?$ropd[COL_OPDNAMA]:'-'?>" style="width: 40vw" readonly />
    <?php
  }
  ?>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Dokumen Manajemen Risiko</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/doc/index-load')?>",
      "type": 'POST',
      "data": function(data){
        data.filterPeriod = $('[name=filterPeriod]', $('.filtering1')).val();
        data.filterOPD = $('[name=filterOPD]', $('.filtering2')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering1'><'filtering2'><'filtering3'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [],
    "columnDefs": [
      {"targets":[0,2,4], "className":'nowrap text-center'},
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true,"width": "50px"},
      {"orderable": true},
      {"orderable": false,"width": "100px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-edit', $(row)).click(function() {
        var href = $(this).attr('href');
        var modal = $('#modal-form');

        $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
        modal.modal('show');
        $('.modal-body', modal).load(href, function(){
          $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering1").html($('#dom-filter-period').html()).addClass('d-inline-block ml-2');
  $("div.filtering2").html($('#dom-filter-opd').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering1")).change(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering2")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    var modal = $('#modal-form');
    dis.html("Loading...").attr("disabled", true);
    $('form', modal).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        dt.DataTable().ajax.reload();
        if(data.error==0) {

        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        modal.modal('hide');
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
});
</script>
