<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Uraian Pengendalian</label>
    <textarea name="<?=COL_URAIANPENGENDALIAN?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_URAIANPENGENDALIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Celah Pengendalian</label>
    <textarea name="<?=COL_URAIANCELAH?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_URAIANCELAH]:''?></textarea>
  </div>
  <div class="form-group">
    <label>RTP</label>
    <textarea name="<?=COL_URAIANRTP?>" class="form-control" rows="2" required><?=!empty($data)?$data[COL_URAIANCELAH]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Penanggung Jawab</label>
    <input type="text" class="form-control" name="<?=COL_RTPPENANGGUNGJAWAB?>" value="<?=!empty($data)?$data[COL_RTPPENANGGUNGJAWAB]:''?>" placeholder="PIC" required />
  </div>
  <div class="form-group">
    <label>Target Waktu</label>
    <input type="text" class="form-control" name="<?=COL_RTPWAKTU?>" value="<?=!empty($data)?$data[COL_RTPWAKTU]:''?>" placeholder="Target Waktu" required />
  </div>
</form>
