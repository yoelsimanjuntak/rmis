<?php
$rreg1 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('TUJUAN','SASARAN','IKU'))
->where_in(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();

$rreg2 = $this->db
->select('trisikoreg.*')
->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
->join(TBL_TRISIKO,TBL_TRISIKO.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDRISIKO,"inner")
->where_in(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, array('PROGRAM','KEGIATAN','SUBKEGIATAN'))
->where_in(TBL_TRISIKO.'.'.COL_UNIQ, $data[COL_UNIQ])
->where(TBL_TRISIKOREG.'.'.COL_NUMRISIKO.' >= ', 11)
->order_by(TBL_TRISIKOREG.'.'.COL_NUMRISIKO, 'desc')
->get(TBL_TRISIKOREG)
->result_array();
?>
<head>
  <html>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: .75rem;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
    font-size: 10pt !important;
  }
  .font-weight-bold {
    font-weight: bold;
  }
  </style>
</head>
<body>
  <h5 style="margin: 0 !important; text-align: center">RTP ATAS HASIL IDENTIFIKASI RISIKO</h5>
  <h4 style="margin: 0 !important; text-align: center"><?=strtoupper($data[COL_OPDNAMA])?></h4>
  <h5 style="margin-top: 0 !important; text-align: center"><?=$data[COL_TAHUN]?></h5>

  <table style="margin-bottom: 40px">
    <thead>
      <tr>
        <th colspan="6" style="text-align: left;">1. RISIKO STRATEGIS</th>
      </tr>
      <tr class="text-sm text-center">
        <th class="no-border text-center" style="width: 10px">No.</th>
        <th class="no-border">Uraian Pengendalian</th>
        <th class="no-border">Celah Pengendalian</th>
        <th class="no-border">RTP</th>
        <th class="no-border">Penanggung Jawab</th>
        <th class="no-border">Target Waktu</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rreg1)) {
        foreach($rreg1 as $reg) {
          $rrtp = $this->db
          ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
          ->get(TBL_TRISIKORTP)
          ->result_array();
          ?>
          <tr style="background-color: #deefff!important">
            <td colspan="6" style="font-weight: bold">
              <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
            </td>
          </tr>
          <?php
          $no=1;
          foreach($rrtp as $rtp) {
            ?>
            <tr>
              <td><?=$no?></td>
              <td><?=$rtp[COL_URAIANPENGENDALIAN]?></td>
              <td><?=$rtp[COL_URAIANCELAH]?></td>
              <td><?=$rtp[COL_URAIANRTP]?></td>
              <td><?=$rtp[COL_RTPPENANGGUNGJAWAB]?></td>
              <td><?=$rtp[COL_RTPWAKTU]?></td>
            </tr>
            <?php
            $no++;
          }
        }
      } else {
        ?>
        <tr>
          <td colspan="6" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>

  <table style="margin-bottom: 40px">
    <thead>
      <tr>
        <th colspan="6" style="text-align: left;">2. RISIKO OPERASIONAL</th>
      </tr>
      <tr class="text-sm text-center">
        <th class="no-border text-center" style="width: 10px">No.</th>
        <th class="no-border">Uraian Pengendalian</th>
        <th class="no-border">Celah Pengendalian</th>
        <th class="no-border">RTP</th>
        <th class="no-border">Penanggung Jawab</th>
        <th class="no-border">Target Waktu</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rreg2)) {
        foreach($rreg2 as $reg) {
          $rrtp = $this->db
          ->where(COL_IDRISIKOREG, $reg[COL_UNIQ])
          ->get(TBL_TRISIKORTP)
          ->result_array();
          ?>
          <tr style="background-color: #deefff!important">
            <td colspan="6" style="font-weight: bold">
              <?=$reg[COL_RISIKOKODE]?> : <?=$reg[COL_RISIKOURAIAN]?>
            </td>
          </tr>
          <?php
          $no=1;
          foreach($rrtp as $rtp) {
            ?>
            <tr>
              <td><?=$no?></td>
              <td><?=$rtp[COL_URAIANPENGENDALIAN]?></td>
              <td><?=$rtp[COL_URAIANCELAH]?></td>
              <td><?=$rtp[COL_URAIANRTP]?></td>
              <td><?=$rtp[COL_RTPPENANGGUNGJAWAB]?></td>
              <td><?=$rtp[COL_RTPWAKTU]?></td>
            </tr>
            <?php
            $no++;
          }
        }
      } else {
        ?>
        <tr>
          <td colspan="6" style="font-style: italic; text-align: center">BELUM ADA DATA TERSEDIA</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <br />
  <table width="100%" style="border: 0 !important">
    <tr>
      <td style="border: 0 !important; width: 100px; white-space: nowrap">
        Tebing Tinggi,
      </td>
      <td style="border: 0 !important; white-space: nowrap; padding-left: 100px;">
        <?=date("Y")?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; font-weight: bold">
        <?=$data[COL_OPDPIMPINANJAB]?>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border: 0 !important; padding-top: 100px !important;">
        <span style="font-weight: bold; text-decoration: underline"><?=$data[COL_OPDPIMPINAN]?></span><br />
        NIP. <?=$data[COL_OPDPIMPINANNIP]?>
      </td>
    </tr>
  </table>
</body>
