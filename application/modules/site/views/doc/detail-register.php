<?php
$ruser = GetLoggedUser();

$rpmdsasaran = $this->db
->select('trisikokonteks.*, tpmdsasaran.Uniq as IdSasaran, tpmdsasaran.Uraian as SasaranPmd')
->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PMDSASARAN')
->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
->get(TBL_TRISIKOKONTEKS)
->result_array();

$rsasaran = $this->db
->select('trisikokonteks.*, trenstrasasaran.Uniq as IdSasaranRenstra, trenstrasasaran.Uraian as SasaranRenstra, tpmdsasaran.Uraian as SasaranPmd')
->join(TBL_TRENSTRASASARAN,TBL_TRENSTRASASARAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->join(TBL_TPMDSASARAN,TBL_TPMDSASARAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDSASARANPMD,"inner")
->join(TBL_TRENSTRATUJUAN,TBL_TRENSTRATUJUAN.'.'.COL_UNIQ." = ".TBL_TRENSTRASASARAN.".".COL_IDTUJUAN,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SASARAN')
->where(TBL_TPMDSASARAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRATUJUAN.'.'.COL_ISDELETED, 0)
->where(TBL_TRENSTRASASARAN.'.'.COL_ISDELETED, 0)
->get(TBL_TRISIKOKONTEKS)
->result_array();

$rprogram = $this->db
->select('trisikokonteks.*, trenjaprogram.ProgKode, trenjaprogram.Uniq as IdProgram')
->join(TBL_TRENJAPROGRAM,TBL_TRENJAPROGRAM.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PROGRAM')
->where(TBL_TRENJAPROGRAM.'.'.COL_ISDELETED, 0)
->order_by(TBL_TRENJAPROGRAM.'.'.COL_PROGKODE)
->get(TBL_TRISIKOKONTEKS)
->result_array();
?>
<?php
if($ruser[COL_ROLEID]==ROLEADMIN) {
  ?>
  <div class="form-group p-3 bg-light" style="border: 1px solid #dedede; border-radius: .5rem;">
    <h6 class="font-weight-bold mb-0">Status Dokumen</h6>
    <ul class="text-sm text-muted font-italic pl-4">
      <li><strong>BARU</strong> = Dokumen dalam tahap entri data oleh operator</li>
      <li><strong>VERIFIKASI</strong> = Dokumen sedang diperiksa / dievaluasi oleh APIP</li>
      <li><strong>FINAL</strong> = Dokumen selesai diperiksa / dievaluasi</li>
    </ul>
    <button type="button" class="btn btn-status btn<?=empty($data[COL_STATREGISTER])?'':'-outline'?>-secondary" style="opacity:1" <?=empty($data[COL_STATREGISTER])?'disabled':''?> data-url="<?=site_url('site/doc/status/StatRegister/'.$data[COL_UNIQ])?>" data-prompt="Apakah anda yakin mengubah status dokumen?">
      <i class="far fa<?=empty($data[COL_STATREGISTER])?'-check':''?>-circle"></i>&nbsp;BARU
    </button>
    <button type="button" class="btn btn-status btn<?=$data[COL_STATREGISTER]=='VERIFIKASI'?'':'-outline'?>-success" style="opacity:1" <?=$data[COL_STATREGISTER]=='VERIFIKASI'?'disabled':''?> data-url="<?=site_url('site/doc/status/StatRegister/'.$data[COL_UNIQ].'/verifikasi')?>" data-prompt="Apakah anda yakin mengubah status dokumen?">
      <i class="far fa<?=$data[COL_STATREGISTER]=='VERIFIKASI'?'-check':''?>-circle"></i>&nbsp;VERIFIKASI
    </button>
    <button type="button" class="btn btn-status btn<?=$data[COL_STATREGISTER]=='FINAL'?'':'-outline'?>-primary" style="opacity:1" <?=$data[COL_STATREGISTER]=='FINAL'?'disabled':''?> data-url="<?=site_url('site/doc/status/StatRegister/'.$data[COL_UNIQ].'/final')?>" data-prompt="Apakah anda yakin mengubah status dokumen?">
      <i class="far fa<?=$data[COL_STATREGISTER]=='FINAL'?'-check':''?>-circle"></i>&nbsp;FINAL
    </button>
  </div>
  <?php
}
?>

<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO STRATEGIS PEMDA</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm">
            <th class="no-border text-center" style="width: 10px">Kode</th>
            <th class="no-border">Uraian Risiko</th>
            <th class="no-border">Pemilik</th>
            <th class="no-border">Uraian Sebab</th>
            <th class="no-border">Sumber</th>
            <th class="no-border" style="white-space: nowrap">C / UC</th>
            <th class="no-border">Uraian Dampak</th>
            <th class="no-border">Pihak Terdampak</th>
            <th class="no-border">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rpmdsasaran)) {
            $no=1;
            foreach($rpmdsasaran as $s) {
              $riku = $this->db
              ->select('trisikokonteks.*')
              ->join(TBL_TPMDIKU,TBL_TPMDIKU.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PMDIKU')
              ->where(TBL_TPMDIKU.'.'.COL_IDSASARAN, $s['IdSasaran'])
              ->where(TBL_TPMDIKU.'.'.COL_ISDELETED, 0)
              ->get(TBL_TRISIKOKONTEKS)
              ->result_array();
              ?>
              <tr class="bg-light">
                <td colspan="9">
                  <strong><?=$s[COL_KONTEKSURAIAN]?></strong>
                </td>
              </tr>
              <?php
              foreach($riku as $iku) {
                $rreg = $this->db
                ->select('trisikoreg.*')
                ->join(TBL_TRISIKOKONTEKS,TBL_TRISIKOKONTEKS.'.'.COL_UNIQ." = ".TBL_TRISIKOREG.".".COL_IDKONTEKS,"inner")
                ->join(TBL_TPMDIKU,TBL_TPMDIKU.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
                //->where(COL_IDKONTEKS, $iku[COL_UNIQ])
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'PMDIKU')
                ->where(TBL_TPMDIKU.'.'.COL_UNIQ, $iku[COL_IDREF])
                ->where(TBL_TPMDIKU.'.'.COL_IDSASARAN, $s['IdSasaran'])
                ->where(TBL_TPMDIKU.'.'.COL_ISDELETED, 0)
                ->get(TBL_TRISIKOREG)
                ->result_array();
                ?>
                <tr class="bg-light">
                  <td colspan="9" class="text-sm">
                    <?=$iku[COL_KONTEKSURAIAN].(!empty($iku[COL_KONTEKSTARGET])?' - '.$iku[COL_KONTEKSTARGET].' ('.$iku[COL_KONTEKSSATUAN].')':'')?>
                    <a href="<?=site_url('site/doc/register-add/'.$iku[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-add pull-right">
                      <i class="fas fa-plus-circle"></i>&nbsp;RISIKO
                    </a>
                  </td>
                </tr>
                <?php
                if(!empty($rreg)) {
                  foreach($rreg as $reg) {
                    $numFeeds = $this->db
                    ->where(COL_FEEDGROUP, 'REGISTER')
                    ->where(COL_FEEDTYPE, 'COMMENT')
                    ->where(COL_FEEDREF, $reg[COL_UNIQ])
                    ->where(COL_FEEDISREAD, 0)
                    ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                    ->count_all_results(TBL_TRISIKOFEEDS);
                    ?>
                    <tr class="text-sm font-italic">
                      <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                      <td><?=$reg[COL_RISIKOURAIAN]?></td>
                      <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                      <td><?=$reg[COL_SEBABURAIAN]?></td>
                      <td><?=$reg[COL_SEBABSUMBER]?></td>
                      <td class="text-center"><?=$reg[COL_CONTROL]?></td>
                      <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                      <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                      <td class="text-center" style="white-space: nowrap">
                        <a href="<?=site_url('site/doc/register-edit/'.$reg[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                        <a href="<?=site_url('site/doc/register-delete/'.$reg[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                        <a href="<?=site_url('site/doc/comment/register/'.$reg[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                          <?php
                          if($numFeeds>0) {
                            ?>
                            <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                            <?php
                          }
                          ?>
                          <i class="fas fa-comment-alt-dots"></i>
                        </a>
                      </td>
                    </tr>
                    <?php
                  }
                }
              }
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="9" class="text-center font-italic">HARAP ISI KONTEKS RISIKO TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO STRATEGIS OPD</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered table-condensed mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm">
            <th class="no-border text-center" style="width: 10px">Kode</th>
            <th class="no-border">Uraian Risiko</th>
            <th class="no-border">Pemilik</th>
            <th class="no-border">Uraian Sebab</th>
            <th class="no-border">Sumber</th>
            <th class="no-border" style="white-space: nowrap">C / UC</th>
            <th class="no-border">Uraian Dampak</th>
            <th class="no-border">Pihak Terdampak</th>
            <th class="no-border">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rsasaran)) {
            $no=1;
            foreach($rsasaran as $s) {
              $riku = $this->db
              ->select('trisikokonteks.*')
              ->join(TBL_TRENSTRASASARANDET,TBL_TRENSTRASASARANDET.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'IKU')
              ->where(TBL_TRENSTRASASARANDET.'.'.COL_IDSASARAN, $s['IdSasaranRenstra'])
              ->where(TBL_TRENSTRASASARANDET.'.'.COL_ISDELETED, 0)
              ->get(TBL_TRISIKOKONTEKS)
              ->result_array();
              ?>
              <tr class="bg-light">
                <td colspan="9">
                  <strong><?=$s[COL_KONTEKSURAIAN]?></strong>
                </td>
              </tr>
              <?php
              foreach($riku as $iku) {
                $rreg = $this->db
                ->where(COL_IDKONTEKS, $iku[COL_UNIQ])
                ->get(TBL_TRISIKOREG)
                ->result_array();
                ?>
                <tr class="bg-light">
                  <td colspan="9" class="text-sm">
                    <?=$iku[COL_KONTEKSURAIAN].(!empty($iku[COL_KONTEKSTARGET])?' - '.$iku[COL_KONTEKSTARGET].' ('.$iku[COL_KONTEKSSATUAN].')':'')?>
                    <a href="<?=site_url('site/doc/register-add/'.$iku[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-add pull-right">
                      <i class="fas fa-plus-circle"></i>&nbsp;RISIKO
                    </a>
                  </td>
                </tr>
                <?php
                if(!empty($rreg)) {
                  foreach($rreg as $reg) {
                    $numFeeds = $this->db
                    ->where(COL_FEEDGROUP, 'REGISTER')
                    ->where(COL_FEEDTYPE, 'COMMENT')
                    ->where(COL_FEEDREF, $reg[COL_UNIQ])
                    ->where(COL_FEEDISREAD, 0)
                    ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                    ->count_all_results(TBL_TRISIKOFEEDS);
                    ?>
                    <tr class="text-sm font-italic">
                      <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                      <td><?=$reg[COL_RISIKOURAIAN]?></td>
                      <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                      <td><?=$reg[COL_SEBABURAIAN]?></td>
                      <td><?=$reg[COL_SEBABSUMBER]?></td>
                      <td class="text-center"><?=$reg[COL_CONTROL]?></td>
                      <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                      <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                      <td class="text-center" style="white-space: nowrap">
                        <a href="<?=site_url('site/doc/register-edit/'.$reg[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                        <a href="<?=site_url('site/doc/register-delete/'.$reg[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                        <a href="<?=site_url('site/doc/comment/register/'.$reg[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                          <?php
                          if($numFeeds>0) {
                            ?>
                            <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                            <?php
                          }
                          ?>
                          <i class="fas fa-comment-alt-dots"></i>
                        </a>
                      </td>
                    </tr>
                    <?php
                  }
                }
              }
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="9" class="text-center font-italic">HARAP ISI KONTEKS RISIKO TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-info">
  <div class="card-header">
    <div class="card-title"><h6 class="mb-0">RISIKO OPERASIONAL OPD</h6></div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive" style="border: 1px solid #dedede !important">
      <table class="table table-bordered mb-0" style="border-top: none !important">
        <thead>
          <tr class="text-sm">
            <th class="no-border text-center" style="width: 10px">Kode</th>
            <th class="no-border">Uraian Risiko</th>
            <th class="no-border">Pemilik</th>
            <th class="no-border">Uraian Sebab</th>
            <th class="no-border">Sumber</th>
            <th class="no-border" style="white-space: nowrap">C / UC</th>
            <th class="no-border">Uraian Dampak</th>
            <th class="no-border">Pihak Terdampak</th>
            <th class="no-border">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rprogram)) {
            foreach ($rprogram as $p) {
              $rkegiatan = $this->db
              ->select('trisikokonteks.*, trenjakegiatan.KegKode, trenjakegiatan.Uniq as IdKegiatan, trenjakegiatan.KegTujuan')
              ->join(TBL_TRENJAKEGIATAN,TBL_TRENJAKEGIATAN.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
              ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'KEGIATAN')
              ->where(TBL_TRENJAKEGIATAN.'.'.COL_IDPROGRAM, $p[COL_IDPROGRAM])
              ->where(TBL_TRENJAKEGIATAN.'.'.COL_ISDELETED, 0)
              ->order_by(TBL_TRENJAKEGIATAN.'.'.COL_KEGKODE)
              ->get(TBL_TRISIKOKONTEKS)
              ->result_array();

              $rreg = $this->db
              ->where(COL_IDKONTEKS, $p[COL_UNIQ])
              ->get(TBL_TRISIKOREG)
              ->result_array();
              ?>
              <tr class="bg-light">
                <th colspan="9">
                  <?=$p[COL_KONTEKSURAIAN]?>
                  <!--<a href="<?=site_url('site/doc/register-add/'.$p[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-add pull-right">
                    <i class="fas fa-plus-circle"></i>&nbsp;RISIKO
                  </a>-->
                </th>
              </tr>
              <?php
              if(!empty($rreg)) {
                foreach($rreg as $reg) {
                  $numFeeds = $this->db
                  ->where(COL_FEEDGROUP, 'REGISTER')
                  ->where(COL_FEEDTYPE, 'COMMENT')
                  ->where(COL_FEEDREF, $reg[COL_UNIQ])
                  ->where(COL_FEEDISREAD, 0)
                  ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                  ->count_all_results(TBL_TRISIKOFEEDS);
                  ?>
                  <tr class="text-sm font-italic">
                    <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                    <td><?=$reg[COL_RISIKOURAIAN]?></td>
                    <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                    <td><?=$reg[COL_SEBABURAIAN]?></td>
                    <td><?=$reg[COL_SEBABSUMBER]?></td>
                    <td class="text-center"><?=$reg[COL_CONTROL]?></td>
                    <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                    <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                    <td class="text-center" style="white-space: nowrap">
                      <a href="<?=site_url('site/doc/register-edit/'.$reg[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                      <a href="<?=site_url('site/doc/register-delete/'.$reg[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                      <a href="<?=site_url('site/doc/comment/register/'.$reg[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                        <?php
                        if($numFeeds>0) {
                          ?>
                          <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                          <?php
                        }
                        ?>
                        <i class="fas fa-comment-alt-dots"></i>
                      </a>
                    </td>
                  </tr>
                  <?php
                }
              }
              foreach($rkegiatan as $k) {
                $rsubkegiatan = $this->db
                ->select('trisikokonteks.*, trenjakegiatansub.SubKode, trenjakegiatansub.SubTujuan')
                ->join(TBL_TRENJAKEGIATANSUB,TBL_TRENJAKEGIATANSUB.'.'.COL_UNIQ." = ".TBL_TRISIKOKONTEKS.".".COL_IDREF,"inner")
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_IDRISIKO, $data[COL_UNIQ])
                ->where(TBL_TRISIKOKONTEKS.'.'.COL_LEVEL, 'SUBKEGIATAN')
                ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_IDKEGIATAN, $k[COL_IDKEGIATAN])
                ->where(TBL_TRENJAKEGIATANSUB.'.'.COL_ISDELETED, 0)
                ->order_by(TBL_TRENJAKEGIATANSUB.'.'.COL_SUBKODE)
                ->get(TBL_TRISIKOKONTEKS)
                ->result_array();

                $rreg = $this->db
                ->where(COL_IDKONTEKS, $k[COL_UNIQ])
                ->get(TBL_TRISIKOREG)
                ->result_array();
                ?>
                <tr class="bg-light text-sm">
                  <td colspan="8">
                    <div class="row d-flex align-items-center justify-content-between mx-1" style="flex-wrap: nowrap">
                      <p class="m-0">
                        <?=$k[COL_KONTEKSURAIAN]?> / <?=!empty($k[COL_KEGTUJUAN])?'<strong>'.$k[COL_KEGTUJUAN].'</strong>':'<span class="text-danger font-italic">(TUJUAN+ KOSONG)</span>'?> / <?=$k[COL_KONTEKSTARGET]?> (<?=$k[COL_KONTEKSSATUAN]?>)
                      </p>
                    </div>
                  </td>
                  <td style="white-space: nowrap">
                    <a href="<?=site_url('site/doc/register-add/'.$k[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-add">
                      <i class="fas fa-plus-circle"></i>&nbsp;RISIKO
                    </a>
                  </td>
                </tr>
                <?php
                if(!empty($rreg)) {
                  foreach($rreg as $reg) {
                    $numFeeds = $this->db
                    ->where(COL_FEEDGROUP, 'REGISTER')
                    ->where(COL_FEEDTYPE, 'COMMENT')
                    ->where(COL_FEEDREF, $reg[COL_UNIQ])
                    ->where(COL_FEEDISREAD, 0)
                    ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                    ->count_all_results(TBL_TRISIKOFEEDS);
                    ?>
                    <tr class="text-sm font-italic">
                      <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                      <td><?=$reg[COL_RISIKOURAIAN]?></td>
                      <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                      <td><?=$reg[COL_SEBABURAIAN]?></td>
                      <td><?=$reg[COL_SEBABSUMBER]?></td>
                      <td class="text-center"><?=$reg[COL_CONTROL]?></td>
                      <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                      <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                      <td class="text-center" style="white-space: nowrap">
                        <a href="<?=site_url('site/doc/register-edit/'.$reg[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                        <a href="<?=site_url('site/doc/register-delete/'.$reg[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                        <a href="<?=site_url('site/doc/comment/register/'.$reg[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                          <?php
                          if($numFeeds>0) {
                            ?>
                            <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                            <?php
                          }
                          ?>
                          <i class="fas fa-comment-alt-dots"></i>
                        </a>
                      </td>
                    </tr>
                    <?php
                  }
                }

                foreach($rsubkegiatan as $s) {
                  $rreg = $this->db
                  ->where(COL_IDKONTEKS, $s[COL_UNIQ])
                  ->get(TBL_TRISIKOREG)
                  ->result_array();

                  ?>
                  <tr class="bg-light text-sm">
                    <td colspan="8">
                      <div class="row d-flex align-items-center justify-content-between mx-1" style="flex-wrap: nowrap">
                        <p class="m-0">
                          <?=$s[COL_KONTEKSURAIAN]?> / <?=!empty($s[COL_SUBTUJUAN])?'<strong>'.$s[COL_SUBTUJUAN].'</strong>':'<span class="text-danger font-italic">(TUJUAN+ KOSONG)</span>'?> / <?=$s[COL_KONTEKSTARGET]?> (<?=$s[COL_KONTEKSSATUAN]?>)
                        </p>
                      </div>
                    </td>
                    <td style="white-space: nowrap">
                      <a href="<?=site_url('site/doc/register-add/'.$s[COL_UNIQ])?>" class="btn btn-outline-info btn-sm btn-add">
                        <i class="fas fa-plus-circle"></i>&nbsp;RISIKO
                      </a>
                    </td>
                  </tr>
                  <?php
                  if(!empty($rreg)) {
                    foreach($rreg as $reg) {
                      $numFeeds = $this->db
                      ->where(COL_FEEDGROUP, 'REGISTER')
                      ->where(COL_FEEDTYPE, 'COMMENT')
                      ->where(COL_FEEDREF, $reg[COL_UNIQ])
                      ->where(COL_FEEDISREAD, 0)
                      ->where(COL_CREATEDBY." !=", $ruser[COL_USERNAME])
                      ->count_all_results(TBL_TRISIKOFEEDS);
                      ?>
                      <tr class="text-sm font-italic">
                        <td style="white-space: nowrap"><?=$reg[COL_RISIKOKODE]?></td>
                        <td><?=$reg[COL_RISIKOURAIAN]?></td>
                        <td><?=$reg[COL_RISIKOPEMILIK]?></td>
                        <td><?=$reg[COL_SEBABURAIAN]?></td>
                        <td><?=$reg[COL_SEBABSUMBER]?></td>
                        <td class="text-center"><?=$reg[COL_CONTROL]?></td>
                        <td><?=$reg[COL_DAMPAKURAIAN]?></td>
                        <td><?=$reg[COL_DAMPAKPIHAK]?></td>
                        <td class="text-center" style="white-space: nowrap">
                          <a href="<?=site_url('site/doc/register-edit/'.$reg[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="fas fa-edit"></i></a>&nbsp;
                          <a href="<?=site_url('site/doc/register-delete/'.$reg[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="fas fa-trash"></i></a>&nbsp;
                          <a href="<?=site_url('site/doc/comment/register/'.$reg[COL_UNIQ])?>" class="btn btn-outline-success btn-sm btn-feed">
                            <?php
                            if($numFeeds>0) {
                              ?>
                              <span class="badge bg-success"><?=number_format($numFeeds)?></span>
                              <?php
                            }
                            ?>
                            <i class="fas fa-comment-alt-dots"></i>
                          </a>
                        </td>
                      </tr>
                      <?php
                    }
                  }
                }
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="9" class="text-center font-italic text-danger">HARAP ISI KONTEKS RISIKO OPERASIONAL TERLEBIH DAHULU!</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Register Risiko</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-feed" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Komentar / Feedback</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer d-block">
        <form id="form-feedback" action="" method="post">
          <div class="input-group">
            <textarea name="<?=COL_FEEDTEXT?>" placeholder="Komentar / Feedback" class="form-control"></textarea>
            <span class="input-group-append">
              <button type="submit" class="btn btn-primary">KIRIM&nbsp;<i class="far fa-paper-plane"></i></button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modalForm = $('#modal-form');
  var modalFeed = $('#modal-feed');

  $('#modal-form').on('hidden.bs.modal', function (e) {
    $(this).closest('#card-main').find('.nav-link.active').trigger('click');
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          $('#modal-form').modal('hide');
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var dis = $(this);
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          dis.closest('#card-main').find('.nav-link.active').trigger('click');
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });

  $('.btn-feed').click(function() {
    var href = $(this).attr('href');

    $('.modal-body', modalFeed).html('<p class="text-center">MEMUAT...</p>');
    modalFeed.modal('show');
    $('.modal-body', modalFeed).load(href, function(){
      $("select", modalFeed).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $("form", modalFeed).attr('action', href);
    });
    return false;
  });

  $('button[type=submit]', modalFeed).click(function(){
    var thisBtn = $(this);
    var thisForm = $(this).closest('form');

    if(!$("textarea", modalFeed).val()) {
      return false;
    }

    thisBtn.attr("disabled", true);
    $('form', modalFeed).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          $('.modal-body', modalFeed).html('<p>Memuat...</p>');
          $('.modal-body', modalFeed).load(thisForm.attr('action'), function(){
            $("select", modalFeed).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
          });
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        thisBtn.attr("disabled", false);
        $("textarea", modalFeed).val('');
      }
    });
  });

  modalFeed.on('hidden.bs.modal', function (e) {
    $(this).closest('#card-main').find('.nav-link.active').trigger('click');
  });

  $('.btn-status').click(function() {
    var thisBtn = $(this);
    var url = $(this).data('url');
    var prompt = ($(this).data('prompt') || 'Apakah anda yakin?');
    if(confirm(prompt)) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          thisBtn.closest('#card-main').find('.nav-link.active').trigger('click');
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
