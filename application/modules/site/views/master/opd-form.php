<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Nama / Nomenklatur</label>
    <input type="text" class="form-control" name="<?=COL_OPDNAMA?>" value="<?=!empty($data)?$data[COL_OPDNAMA]:''?>" required />
  </div>
  <div class="form-group">
    <label>Nama Pimpinan</label>
    <input type="text" class="form-control" name="<?=COL_OPDPIMPINAN?>" value="<?=!empty($data)?$data[COL_OPDPIMPINAN]:''?>" required />
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>NIP</label>
        <input type="text" class="form-control" name="<?=COL_OPDPIMPINANNIP?>" value="<?=!empty($data)?$data[COL_OPDPIMPINANNIP]:''?>" />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Jabatan</label>
        <input type="text" class="form-control" name="<?=COL_OPDPIMPINANJAB?>" value="<?=!empty($data)?$data[COL_OPDPIMPINANJAB]:'Kepala Dinas'?>" />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Kode Referensi</label>
    <input type="text" class="form-control" name="<?=COL_OPDREF?>" value="<?=!empty($data)?$data[COL_OPDREF]:''?>" />
    <p class="text-muted text-sm font-italic mt-1">NB: Kode integrasi OPD dengan Aplikasi <strong>E-SAKIP SITALAKBAJAKUN</strong></p>
  </div>
</form>
