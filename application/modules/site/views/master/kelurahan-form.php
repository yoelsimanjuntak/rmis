<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <label>Kecamatan</label>
        <select class="form-control" name="<?=COL_IDKECAMATAN?>" style="width: 100%">
          <?=GetCombobox("select mkecamatan.*, kab.Kabupaten from mkecamatan left join mkabupaten kab on kab.Uniq = mkecamatan.IdKabupaten order by Kabupaten asc, Kecamatan asc", COL_UNIQ, array(COL_KABUPATEN, COL_KECAMATAN), (!empty($data)?$data[COL_IDKECAMATAN]:null))?>
        </select>
      </div>
      <div class="form-group">
        <label>Kelurahan</label>
        <input type="text" class="form-control" name="<?=COL_KELURAHAN?>" value="<?=!empty($data)?$data[COL_KELURAHAN]:''?>" placeholder="Nama Kelurahan" required />
      </div>
    </div>
  </div>
</form>
