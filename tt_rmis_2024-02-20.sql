# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_rmis
# Generation Time: 2024-02-19 18:36:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_homepage`;

CREATE TABLE `_homepage` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentID` varchar(20) DEFAULT NULL,
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentType` varchar(50) NOT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_homepage` WRITE;
/*!40000 ALTER TABLE `_homepage` DISABLE KEYS */;

INSERT INTO `_homepage` (`Uniq`, `ContentID`, `ContentTitle`, `ContentType`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(1,'TxtWelcome1','Pengantar','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(2,'TxtWelcome2','Pengantar','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(41,'NumProfile','NUMBER 1','NUMBER','Angka 1',NULL,'1','Satuan'),
	(42,'NumProfile','NUMBER 2','NUMBER','Angka 2',NULL,'2','Satuan'),
	(43,'NumProfile','NUMBER 3','NUMBER','Angka 3',NULL,'3','Satuan'),
	(44,'NumProfile','NUMBER 4','NUMBER','Angka 4',NULL,'4','Satuan'),
	(48,'TxtPopup1','Pengumuman','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(49,'TxtPopup2','FORKOPIMDA','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(50,'TxtPopup2','FKUB','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(51,'TxtPopup2','FPK','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(52,'TxtPopup2','FKDM','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(55,'Carousel','Carousel 1','IMG',NULL,NULL,NULL,NULL),
	(56,'Carousel','Carousel 2','IMG',NULL,NULL,NULL,NULL),
	(57,'Carousel','Carousel 3','IMG',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2024-02-14 00:09:52','http://localhost/tt-rmis/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(2,'2024-02-14 00:10:17','http://localhost/tt-rmis/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(3,'2024-02-14 00:10:17','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(4,'2024-02-14 00:11:05','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(5,'2024-02-18 20:38:43','http://localhost/tt-rmis/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(6,'2024-02-18 20:38:43','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(7,'2024-02-18 20:42:40','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(8,'2024-02-18 20:42:55','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(9,'2024-02-18 20:44:10','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(10,'2024-02-18 20:44:46','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(11,'2024-02-18 20:45:01','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(12,'2024-02-18 20:45:11','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(13,'2024-02-18 20:45:41','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(14,'2024-02-18 20:46:08','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(15,'2024-02-18 20:46:12','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(16,'2024-02-18 20:46:39','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(17,'2024-02-18 20:46:43','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(18,'2024-02-18 20:48:01','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(19,'2024-02-18 20:49:36','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(20,'2024-02-18 20:49:46','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(21,'2024-02-18 20:50:02','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(22,'2024-02-18 20:50:19','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(23,'2024-02-18 20:50:40','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(24,'2024-02-18 20:50:46','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(25,'2024-02-18 20:51:17','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(26,'2024-02-18 20:51:22','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(27,'2024-02-18 20:51:27','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(28,'2024-02-18 20:51:30','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(29,'2024-02-18 20:52:23','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(30,'2024-02-18 20:53:07','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(31,'2024-02-18 20:53:10','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(32,'2024-02-18 20:53:24','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(33,'2024-02-18 20:53:37','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(34,'2024-02-18 20:53:48','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(35,'2024-02-18 20:53:55','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(36,'2024-02-18 20:55:02','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(37,'2024-02-18 20:56:08','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(38,'2024-02-18 20:56:18','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(39,'2024-02-18 20:56:31','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(40,'2024-02-18 20:56:49','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(41,'2024-02-18 20:57:08','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(42,'2024-02-18 20:57:20','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(43,'2024-02-18 20:57:31','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(44,'2024-02-18 20:57:40','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(45,'2024-02-18 20:57:55','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(46,'2024-02-18 20:58:02','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(47,'2024-02-18 20:58:11','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(48,'2024-02-18 20:58:31','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(49,'2024-02-18 20:59:07','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(50,'2024-02-18 21:05:34','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(51,'2024-02-18 21:07:25','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(52,'2024-02-18 21:08:00','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(53,'2024-02-18 21:09:53','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(54,'2024-02-18 21:12:40','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(55,'2024-02-18 21:12:50','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(56,'2024-02-18 21:13:10','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(57,'2024-02-18 21:13:41','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(58,'2024-02-18 21:14:45','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(59,'2024-02-18 21:15:08','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(60,'2024-02-18 21:15:23','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(61,'2024-02-18 21:15:52','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(62,'2024-02-18 21:16:04','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(63,'2024-02-18 21:17:42','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(64,'2024-02-18 21:18:03','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(65,'2024-02-18 21:18:32','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(66,'2024-02-18 21:20:35','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(67,'2024-02-18 21:27:52','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(68,'2024-02-18 21:29:20','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(69,'2024-02-18 21:29:51','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(70,'2024-02-18 21:30:52','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(71,'2024-02-18 21:30:55','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(72,'2024-02-18 21:30:55','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(73,'2024-02-18 21:30:58','http://localhost/tt-rmis/site/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(74,'2024-02-18 21:30:58','http://localhost/tt-rmis/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(75,'2024-02-18 21:30:58','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(76,'2024-02-18 21:31:20','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(77,'2024-02-18 21:32:23','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(78,'2024-02-18 21:32:46','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(79,'2024-02-18 21:32:50','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(80,'2024-02-18 21:32:58','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(81,'2024-02-18 21:33:13','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(82,'2024-02-18 21:33:23','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(83,'2024-02-18 21:33:27','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(84,'2024-02-18 21:33:35','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(85,'2024-02-18 21:33:43','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(86,'2024-02-18 21:33:52','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(87,'2024-02-18 21:34:08','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(88,'2024-02-18 21:34:23','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(89,'2024-02-18 21:34:42','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(90,'2024-02-18 21:34:58','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36'),
	(91,'2024-02-18 21:36:19','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36'),
	(92,'2024-02-18 21:37:10','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36'),
	(93,'2024-02-18 21:37:42','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36'),
	(94,'2024-02-18 21:38:03','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(95,'2024-02-18 21:38:08','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(96,'2024-02-18 21:39:13','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(97,'2024-02-18 21:39:36','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(98,'2024-02-18 21:39:44','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(99,'2024-02-18 21:40:37','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(100,'2024-02-18 21:40:51','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(101,'2024-02-18 21:41:36','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(102,'2024-02-18 21:42:04','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(103,'2024-02-18 21:42:08','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(104,'2024-02-18 21:42:17','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(105,'2024-02-18 22:09:32','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(106,'2024-02-18 22:12:56','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(107,'2024-02-18 22:14:00','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(108,'2024-02-18 22:26:18','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(109,'2024-02-18 22:28:36','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(110,'2024-02-18 22:28:47','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(111,'2024-02-18 22:29:28','http://localhost/tt-rmis/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(112,'2024-02-18 22:29:29','http://localhost/tt-rmis/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(113,'2024-02-18 22:30:16','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(114,'2024-02-18 22:31:26','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(115,'2024-02-18 22:31:59','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(116,'2024-02-18 22:34:02','http://localhost/tt-rmis/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(117,'2024-02-18 22:34:52','http://localhost/tt-rmis/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(118,'2024-02-18 22:35:21','http://localhost/tt-rmis/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(119,'2024-02-18 22:35:59','http://localhost/tt-rmis/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(120,'2024-02-19 21:23:11','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(121,'2024-02-19 21:23:19','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(122,'2024-02-19 21:26:12','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(123,'2024-02-19 21:29:10','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(124,'2024-02-19 21:29:30','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(125,'2024-02-19 21:29:42','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(126,'2024-02-19 21:32:26','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(127,'2024-02-19 21:39:08','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(128,'2024-02-19 21:39:54','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(129,'2024-02-19 21:40:09','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(130,'2024-02-19 21:40:46','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(131,'2024-02-19 21:41:11','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(132,'2024-02-19 21:41:30','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(133,'2024-02-19 21:42:05','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(134,'2024-02-19 21:42:25','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(135,'2024-02-19 21:43:19','http://localhost/tt-rmis/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(136,'2024-02-19 21:51:39','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(137,'2024-02-19 21:51:39','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(138,'2024-02-19 21:51:44','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(139,'2024-02-19 21:51:47','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(140,'2024-02-19 21:52:01','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(141,'2024-02-19 21:52:07','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(142,'2024-02-19 21:53:17','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(143,'2024-02-19 21:54:13','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(144,'2024-02-19 21:56:20','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(145,'2024-02-19 21:57:07','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(146,'2024-02-19 21:57:56','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(147,'2024-02-19 21:58:11','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(148,'2024-02-19 21:58:16','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(149,'2024-02-19 21:58:47','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(150,'2024-02-19 22:00:22','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(151,'2024-02-19 22:01:38','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(152,'2024-02-19 22:01:48','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(153,'2024-02-19 22:02:24','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(154,'2024-02-19 22:02:39','http://localhost/tt-rmis/site/master/opd-add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(155,'2024-02-19 22:02:40','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(156,'2024-02-19 22:02:55','http://localhost/tt-rmis/site/master/opd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(157,'2024-02-19 22:02:56','http://localhost/tt-rmis/site/master/opd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(158,'2024-02-19 22:03:01','http://localhost/tt-rmis/site/master/opd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(159,'2024-02-19 22:03:20','http://localhost/tt-rmis/site/master/opd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(160,'2024-02-19 22:03:47','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(161,'2024-02-19 22:03:55','http://localhost/tt-rmis/site/master/opd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(162,'2024-02-19 22:04:40','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(163,'2024-02-19 22:04:46','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(164,'2024-02-19 22:04:52','http://localhost/tt-rmis/site/master/opd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(165,'2024-02-19 22:05:23','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(166,'2024-02-19 22:05:27','http://localhost/tt-rmis/site/master/opd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(167,'2024-02-19 22:06:33','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(168,'2024-02-19 22:06:37','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(169,'2024-02-19 22:07:02','http://localhost/tt-rmis/site/master/opd-add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(170,'2024-02-19 22:07:02','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(171,'2024-02-19 22:07:25','http://localhost/tt-rmis/site/master/opd-edit/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(172,'2024-02-19 22:07:25','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(173,'2024-02-19 22:15:06','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(174,'2024-02-19 22:15:33','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(175,'2024-02-19 22:15:43','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(176,'2024-02-19 22:16:10','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(177,'2024-02-19 22:28:00','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(178,'2024-02-19 22:28:07','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(179,'2024-02-19 22:28:24','http://localhost/tt-rmis/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(180,'2024-02-19 22:28:24','http://localhost/tt-rmis/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(181,'2024-02-19 22:28:24','http://localhost/tt-rmis/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(182,'2024-02-19 22:28:32','http://localhost/tt-rmis/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(183,'2024-02-19 22:28:35','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(184,'2024-02-19 22:28:52','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(185,'2024-02-19 22:28:54','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(186,'2024-02-19 22:29:45','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(187,'2024-02-19 22:30:21','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(188,'2024-02-19 22:30:59','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(189,'2024-02-19 22:31:05','http://localhost/tt-rmis/site/master/opd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(190,'2024-02-19 22:40:10','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(191,'2024-02-19 22:40:29','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(192,'2024-02-19 22:40:46','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(193,'2024-02-19 22:42:00','http://localhost/tt-rmis/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(194,'2024-02-19 22:42:01','http://localhost/tt-rmis/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(195,'2024-02-19 22:42:23','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(196,'2024-02-19 22:42:37','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(197,'2024-02-19 22:42:52','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(198,'2024-02-19 22:43:52','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(199,'2024-02-19 22:44:09','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(200,'2024-02-19 22:45:39','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(201,'2024-02-19 22:46:16','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(202,'2024-02-19 22:46:41','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(203,'2024-02-19 22:49:48','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(204,'2024-02-19 22:50:09','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(205,'2024-02-19 22:50:46','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(206,'2024-02-19 22:54:33','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(207,'2024-02-19 23:00:37','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(208,'2024-02-19 23:00:46','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(209,'2024-02-19 23:01:05','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(210,'2024-02-19 23:01:27','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(211,'2024-02-19 23:01:34','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(212,'2024-02-19 23:02:21','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(213,'2024-02-19 23:02:24','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(214,'2024-02-19 23:02:30','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(215,'2024-02-19 23:02:46','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(216,'2024-02-19 23:02:46','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(217,'2024-02-19 23:03:37','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(218,'2024-02-19 23:03:37','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(219,'2024-02-19 23:04:02','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(220,'2024-02-19 23:05:09','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(221,'2024-02-19 23:05:35','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(222,'2024-02-19 23:05:40','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(223,'2024-02-19 23:10:23','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(224,'2024-02-19 23:12:06','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(225,'2024-02-19 23:12:09','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(226,'2024-02-19 23:12:37','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(227,'2024-02-19 23:13:34','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(228,'2024-02-19 23:13:41','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(229,'2024-02-19 23:15:04','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(230,'2024-02-19 23:15:41','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(231,'2024-02-19 23:16:00','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(232,'2024-02-19 23:29:18','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(233,'2024-02-19 23:30:02','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(234,'2024-02-19 23:35:13','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(235,'2024-02-20 00:03:30','http://localhost/tt-rmis/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(236,'2024-02-20 00:04:40','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(237,'2024-02-20 00:05:45','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(238,'2024-02-20 00:05:45','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(239,'2024-02-20 00:05:54','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(240,'2024-02-20 00:05:55','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(241,'2024-02-20 00:07:08','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(242,'2024-02-20 00:07:26','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(243,'2024-02-20 00:07:33','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(244,'2024-02-20 00:07:33','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(245,'2024-02-20 00:08:13','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(246,'2024-02-20 00:08:17','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(247,'2024-02-20 00:08:52','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(248,'2024-02-20 00:17:19','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(249,'2024-02-20 00:17:35','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(250,'2024-02-20 00:17:46','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(251,'2024-02-20 00:17:46','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(252,'2024-02-20 00:23:26','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(253,'2024-02-20 00:23:34','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(254,'2024-02-20 00:23:44','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(255,'2024-02-20 00:24:05','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(256,'2024-02-20 00:24:10','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(257,'2024-02-20 00:24:47','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(258,'2024-02-20 00:24:55','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(259,'2024-02-20 00:24:55','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(260,'2024-02-20 00:26:28','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(261,'2024-02-20 00:26:31','http://localhost/tt-rmis/site/perencanaan/rpjmd-sync/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(262,'2024-02-20 00:26:32','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(263,'2024-02-20 00:27:37','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(264,'2024-02-20 00:28:08','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(265,'2024-02-20 00:28:51','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(266,'2024-02-20 00:29:35','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(267,'2024-02-20 00:29:40','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(268,'2024-02-20 00:30:02','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(269,'2024-02-20 00:30:05','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(270,'2024-02-20 00:30:06','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(271,'2024-02-20 00:32:05','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(272,'2024-02-20 00:32:29','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(273,'2024-02-20 00:32:43','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(274,'2024-02-20 00:34:15','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(275,'2024-02-20 00:37:00','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(276,'2024-02-20 00:37:08','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(277,'2024-02-20 00:37:20','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(278,'2024-02-20 00:37:27','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(279,'2024-02-20 00:37:31','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(280,'2024-02-20 00:39:09','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(281,'2024-02-20 00:40:18','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(282,'2024-02-20 00:40:46','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(283,'2024-02-20 00:40:54','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(284,'2024-02-20 00:41:12','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(285,'2024-02-20 00:41:17','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(286,'2024-02-20 00:44:14','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(287,'2024-02-20 00:44:37','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(288,'2024-02-20 00:44:46','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(289,'2024-02-20 00:44:54','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(290,'2024-02-20 00:45:08','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(291,'2024-02-20 00:45:18','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(292,'2024-02-20 00:45:51','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(293,'2024-02-20 00:45:58','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(294,'2024-02-20 00:46:05','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(295,'2024-02-20 00:46:15','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(296,'2024-02-20 00:46:22','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(297,'2024-02-20 00:46:46','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(298,'2024-02-20 00:47:17','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(299,'2024-02-20 00:47:22','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(300,'2024-02-20 00:47:29','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(301,'2024-02-20 00:48:02','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(302,'2024-02-20 00:48:05','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(303,'2024-02-20 00:49:08','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(304,'2024-02-20 00:51:38','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(305,'2024-02-20 00:52:24','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(306,'2024-02-20 00:52:37','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-misi/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(307,'2024-02-20 00:52:45','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-misi/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(308,'2024-02-20 00:52:53','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-misi/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(309,'2024-02-20 00:52:53','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(310,'2024-02-20 00:53:30','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(311,'2024-02-20 00:53:45','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(312,'2024-02-20 00:53:49','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-misi/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(313,'2024-02-20 00:53:49','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(314,'2024-02-20 00:53:55','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(315,'2024-02-20 00:54:50','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(316,'2024-02-20 00:54:58','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit-misi/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(317,'2024-02-20 00:54:58','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(318,'2024-02-20 00:55:03','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit-misi/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(319,'2024-02-20 00:55:03','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(320,'2024-02-20 00:55:14','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(321,'2024-02-20 00:55:16','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(322,'2024-02-20 00:55:24','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(323,'2024-02-20 00:58:42','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(324,'2024-02-20 00:58:54','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(325,'2024-02-20 01:01:13','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(326,'2024-02-20 01:01:14','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(327,'2024-02-20 01:01:17','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(328,'2024-02-20 01:01:20','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(329,'2024-02-20 01:01:25','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-misi/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(330,'2024-02-20 01:01:25','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(331,'2024-02-20 01:01:28','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(332,'2024-02-20 01:01:35','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(333,'2024-02-20 01:01:36','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(334,'2024-02-20 01:01:57','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(335,'2024-02-20 01:02:01','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-tujuan/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(336,'2024-02-20 01:02:01','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(337,'2024-02-20 01:02:42','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit-tujuan/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(338,'2024-02-20 01:02:42','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(339,'2024-02-20 01:03:13','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(340,'2024-02-20 01:03:33','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(341,'2024-02-20 01:06:37','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(342,'2024-02-20 01:06:43','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(343,'2024-02-20 01:06:58','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(344,'2024-02-20 01:07:09','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(345,'2024-02-20 01:07:54','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(346,'2024-02-20 01:08:20','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(347,'2024-02-20 01:09:36','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(348,'2024-02-20 01:09:49','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36'),
	(349,'2024-02-20 01:10:17','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36'),
	(350,'2024-02-20 01:10:32','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36'),
	(351,'2024-02-20 01:10:40','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(352,'2024-02-20 01:11:08','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(353,'2024-02-20 01:13:50','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(354,'2024-02-20 01:13:55','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-sasaran/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(355,'2024-02-20 01:13:55','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(356,'2024-02-20 01:14:02','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit-sasaran/11.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(357,'2024-02-20 01:14:02','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(358,'2024-02-20 01:14:05','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(359,'2024-02-20 01:17:28','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(360,'2024-02-20 01:17:46','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(361,'2024-02-20 01:18:09','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(362,'2024-02-20 01:18:45','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(363,'2024-02-20 01:18:51','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(364,'2024-02-20 01:19:21','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(365,'2024-02-20 01:19:28','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(366,'2024-02-20 01:20:15','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(367,'2024-02-20 01:20:29','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(368,'2024-02-20 01:21:39','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(369,'2024-02-20 01:22:22','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(370,'2024-02-20 01:22:51','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(371,'2024-02-20 01:23:02','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(372,'2024-02-20 01:23:22','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(373,'2024-02-20 01:23:52','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(374,'2024-02-20 01:23:58','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(375,'2024-02-20 01:24:10','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(376,'2024-02-20 01:24:15','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(377,'2024-02-20 01:24:52','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(378,'2024-02-20 01:24:56','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(379,'2024-02-20 01:25:10','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(380,'2024-02-20 01:25:11','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(381,'2024-02-20 01:26:43','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(382,'2024-02-20 01:29:08','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-iku/10.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(383,'2024-02-20 01:29:14','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-iku/10.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(384,'2024-02-20 01:29:31','http://localhost/tt-rmis/site/perencanaan/rpjmd-add-iku/10.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(385,'2024-02-20 01:29:31','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(386,'2024-02-20 01:30:54','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(387,'2024-02-20 01:31:04','http://localhost/tt-rmis/site/perencanaan/rpjmd-edit-iku/20.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(388,'2024-02-20 01:31:04','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(389,'2024-02-20 01:31:32','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(390,'2024-02-20 01:31:35','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(391,'2024-02-20 01:31:37','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(392,'2024-02-20 01:31:40','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(393,'2024-02-20 01:31:42','http://localhost/tt-rmis/site/perencanaan/rpjmd.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
	(394,'2024-02-20 01:31:52','http://localhost/tt-rmis/site/perencanaan/rpjmd-det/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita',NULL,1,0,0),
	(2,'Infografis',NULL,1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator'),
	(3,'Publik');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,1,'SETTING_WEB_NAME','SETTING_WEB_NAME','RMIS'),
	(2,2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Risk Management Information System'),
	(3,3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Inspektorat Kota Tebing Tinggi'),
	(5,5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS',''),
	(6,6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(14,14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.01'),
	(16,16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KOTA TEBING TINGGI'),
	(18,18,'SETTING_LINK_INSTAGRAM','SETTING_LINK_INSTAGRAM',''),
	(19,19,'SETTING_LINK_FACEBOOK','SETTING_LINK_FACEBOOK',''),
	(20,20,'SETTING_LINK_YOUTUBE','SETTING_LINK_YOUTUBE',''),
	(21,21,'SETTING_LINK_GOOGLEMAP','SETTING_LINK_GOOGLEMAP',''),
	(22,22,'SETTING_LINK_WHATSAPP','SETTING_LINK_WHATSAPP',''),
	(23,23,'SETTING_LINK_INSTAGRAM_ACC','SETTING_LINK_INSTAGRAM_ACC','');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNo` varchar(50) DEFAULT NULL,
  `ImgFile` varchar(250) DEFAULT NULL,
  `RegDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `Name`, `IdentityNo`, `BirthDate`, `Gender`, `Address`, `PhoneNo`, `ImgFile`, `RegDate`)
VALUES
	(1,'admin','administrator','TEBINGTINGGI',NULL,NULL,NULL,NULL,NULL,NULL,'2023-01-01');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2023-05-09 08:15:14','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mopd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mopd`;

CREATE TABLE `mopd` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `OPDRef` varchar(50) DEFAULT NULL,
  `OPDNama` varchar(200) NOT NULL DEFAULT '',
  `OPDPimpinan` varchar(200) DEFAULT NULL,
  `OPDPimpinanNIP` varchar(200) DEFAULT NULL,
  `OPDPimpinanJab` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mopd` WRITE;
/*!40000 ALTER TABLE `mopd` DISABLE KEYS */;

INSERT INTO `mopd` (`Uniq`, `OPDRef`, `OPDNama`, `OPDPimpinan`, `OPDPimpinanNIP`, `OPDPimpinanJab`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(3,'1.1.1.1','Dinas Pendidikan dan Kebudayaan','IDAM KHALID, S.K.M., M.Kes',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(4,'1.1.1.3','Dinas Perumahan, Kawasan Permukiman dan Pertanahan','CHAIRUN NASRIN NASUTION, S.T., M.Si.',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(5,'1.1.1.5','Dinas Pemberdayaan Perempuan, Perlindungan Anak dan Pemberdayaan Masyarakat','Dra. SRI WAHYUNI, M.Si',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(6,'1.1.1.6','Dinas Pengendalian Penduduk dan Keluarga Berencana','Hj. NINA ZAHARA MZ, SH, M.AP',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(7,'1.1.1.11','Dinas Pemadam Kebakaran dan Penyelamatan','ABDUL HALIM PURBA, S.STP, M.Si',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(8,'1.2.1.1','Dinas Kesehatan','dr. HENNY SRI HARTATI',NULL,'PLT. KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(9,'1.3.1.1','Dinas Pekerjaan Umum dan Penataan Ruang','REZA AGHISTA, ST, M.Si',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(10,'1.6.1.1','Dinas Sosial','Drs.KHAIRIL ANWAR,M.Si',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(11,'2.1.1.1','Dinas Ketenagakerjaan dan Perindustrian','Ir. IBOY HUTAPEA',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(12,'2.2.2.3','Badan Kesatuan Bangsa dan Politik','ABDUL HALIM PURBA, S.STP., M.Si',NULL,'PLT. KEPALA BADAN','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(13,'2.2.2.4','Satuan Polisi Pamong Praja','Drs. YUSTIN BERNAT HUTAPEA',NULL,'KEPALA SATUAN','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(14,'2.2.2.5','Badan Penanggulangan Bencana Daerah','TORA DAENG MASARO, ST, M.Si',NULL,'KEPALA BADAN','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(15,'2.3.1.1','Dinas Ketahanan Pangan dan Pertanian','MARIMBUN MARPAUNG, S.P., M.Si.',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(16,'2.5.1.1','Dinas Lingkungan Hidup','Dr. H. Muhammad Hasbie Ashshiddiqi, M.M., M.Si.',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(17,'2.6.1.1','Dinas Kependudukan dan Pencatatan Sipil','MUHAMMAD FACHRY, S.STP., M.AP.',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(18,'2.9.1.1','Dinas Perhubungan','M. GUNTUR HARAHAP, S.S.T.P, M.Si. ',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(19,'2.10.1.1','Dinas Komunikasi dan Informatika','DEDI PARULIAN SIAGIAN, S.STP, M.Si.',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(20,'2.11.1.1','Dinas Perdagangan, Koperasi, Usaha Kecil dan Menengah','ZAHIDIN, S.Pd., M.Pd.',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(21,'2.12.1.1','Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu','AMRIS SIAHAAN, S.Pd, M.Si',NULL,'PLT. KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(22,'2.13.1.1','Dinas Pemuda, Olahraga dan Pariwisata','SYAHDAMA YANTO, AP',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(23,'2.17.1.1','Dinas Perpustakaan dan Arsip Daerah','MUHAMMAD FADLY, S.Pd, M.Pd',NULL,'KEPALA DINAS','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(24,'4.1.1.1','Sekretariat Daerah Kota','H. KAMLAN, SH.',NULL,'PJ. SEKRETARIS DAERAH','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(25,'4.1.1.2','Sekretariat DPRD','MUHAMMAD SAAT NASUTION, SH',NULL,'SEKRETARIS DPRD','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(26,'4.1.1.4','Kecamatan Bajenis','DIRA ASTAMA TRISNA SIP, M.Si',NULL,'CAMAT','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(27,'4.1.1.5','Kecamatan Rambutan','MARWANSYAH HARAHAP, S.STP',NULL,'CAMAT','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(28,'4.1.1.6','Kecamatan Padang Hilir','RAMADHAN BARQAH PULUNGAN, S.IP.,  M.Si.',NULL,'PLT. CAMAT','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(29,'4.1.1.7','Kecamatan Padang Hulu','H. DENI HANDIKA SIREGAR, SE, M.Si',NULL,'CAMAT','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(30,'4.1.1.8','Kecamatan Tebing Tinggi Kota','MANDA YULIAN, S.STP., M.Si',NULL,'CAMAT','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(31,'4.2.1.1','Inspektorat Kota','H. KAMLAN, S.H., M.M., CGCAE',NULL,'INSPEKTUR','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(32,'4.3.1.1','Badan Perencanaan Pembangunan Daerah','ERWIN SUHERI DAMANIK',NULL,'KEPALA BADAN','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(33,'4.4.1.1','Badan Pengelolaan Keuangan dan Pendapatan Daerah','SRI IMBANG JAYA PUTRA, AP, MSP',NULL,'KEPALA BADAN','admin','2024-02-20 00:00:00',NULL,NULL,0),
	(34,'4.5.1.1','Badan Kepegawaian dan Pengembangan Sumber Daya Manusia','SYAIFUL FAHRI, S.P., M.Si. ',NULL,'KEPALA BADAN','admin','2024-02-20 00:00:00',NULL,NULL,0);

/*!40000 ALTER TABLE `mopd` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpmdiku
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpmdiku`;

CREATE TABLE `tpmdiku` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdSasaran` bigint(10) unsigned NOT NULL,
  `Uraian` varchar(200) NOT NULL DEFAULT '',
  `Target` varchar(50) DEFAULT '',
  `Satuan` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpmdiku` WRITE;
/*!40000 ALTER TABLE `tpmdiku` DISABLE KEYS */;

INSERT INTO `tpmdiku` (`Uniq`, `IdSasaran`, `Uraian`, `Target`, `Satuan`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,1,'PREVALENSI STUNTING','9','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(2,1,'ANGKA KESAKITAN','8,75','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(3,1,'ANGKA HARAPAN HIDUP','74,50','Tahun','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(4,2,'RATA-RATA LAMA SEKOLAH','11,50','Tahun','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(5,3,'INDEKS PROFESIONALITAS ASN','80','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(6,4,'PERSENTASE PMKS YANG TERPENUHI KEBUTUHAN DASARNYA','100','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(7,5,'KONTRIBUSI PDRB SEKTOR UNGGULAN','70,73','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(8,5,'PENGELUARAN RILL PER KAPITA (RIBU/KAPITA)','14.080','Rupiah','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(9,6,'NILAI INVESTASI','178 MILYAR','Rupiah','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(10,6,'INDEKS RASA AMAN','75,05','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(11,7,'PERSENTASE CAKUPAN SANITASI YANG LAYAK','89,20','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(12,7,'PERSENTASE CAKUPAN KEBUTUHAN AIR MINUM','76','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(13,8,'PELAYANAN PENCEGAHAN DAN KESIAPSIAGAAN TERHADAP BENCANA','100','Persen','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(14,8,'INDEKS KUALITAS LINGKUNGAN HIDUP','57,49','Indeks','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(15,9,'NILAI SAKIP','A','Predikat','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(16,9,'OPINI BPK TERHADAP LKPD','WTP','Predikat','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(17,9,'INDEKS SPBE','3,0','Indeks','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(18,10,'INDEKS PELAYANAN PUBLIK','PRIMA','Predikat','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(19,10,'IKM','90','Nilai','admin','2024-02-20 00:26:31',NULL,NULL,0);

/*!40000 ALTER TABLE `tpmdiku` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpmdmisi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpmdmisi`;

CREATE TABLE `tpmdmisi` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdPeriode` bigint(10) unsigned NOT NULL,
  `Uraian` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpmdmisi` WRITE;
/*!40000 ALTER TABLE `tpmdmisi` DISABLE KEYS */;

INSERT INTO `tpmdmisi` (`Uniq`, `IdPeriode`, `Uraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,1,'Rencana Pembangunan Daerah Kota Tebing Tinggi Tahun 2023 s.d 2026','admin','2024-02-20 00:26:31',NULL,NULL,0);

/*!40000 ALTER TABLE `tpmdmisi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpmdperiode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpmdperiode`;

CREATE TABLE `tpmdperiode` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PeriodNama` varchar(200) DEFAULT NULL,
  `PeriodKepalaDaerah` varchar(200) DEFAULT NULL,
  `PeriodFrom` int(11) NOT NULL,
  `PeriodTo` int(11) NOT NULL,
  `Visi` text NOT NULL,
  `PeriodIsAktif` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpmdperiode` WRITE;
/*!40000 ALTER TABLE `tpmdperiode` DISABLE KEYS */;

INSERT INTO `tpmdperiode` (`Uniq`, `PeriodNama`, `PeriodKepalaDaerah`, `PeriodFrom`, `PeriodTo`, `Visi`, `PeriodIsAktif`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,'RPD 2023 - 2026','Drs. SYARMADANI, M.Si',2023,2026,'RENCANA PEMBANGUNAN DAERAH PERIODE 2023 - 2026',1,'admin','2024-02-12 12:02:24','admin','2024-02-20 00:05:54',0);

/*!40000 ALTER TABLE `tpmdperiode` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpmdsasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpmdsasaran`;

CREATE TABLE `tpmdsasaran` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdTujuan` bigint(10) NOT NULL,
  `Uraian` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpmdsasaran` WRITE;
/*!40000 ALTER TABLE `tpmdsasaran` DISABLE KEYS */;

INSERT INTO `tpmdsasaran` (`Uniq`, `IdTujuan`, `Uraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,1,'MENINGKATNYA DERAJAT KESEHATAN MASYARAKAT','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(2,1,'MENINGKATNYA KUALITAS DAN DAYA SAING PENDIDIKAN MASYARAKAT','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(3,1,'MENINGKATNYA KOMPETENSI ASN','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(4,1,'MENINGKATNYA TARAF KESEJAHTERAAN, KUALITAS DAN KELANGSUNGAN HIDUP','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(5,2,'MENINGKATNYA KONTRIBUSI PDRB SEKTOR UNGGULAN','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(6,2,'MENINGKATNYA INVESTASI','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(7,3,'MENINGKATNYA KUALITAS INFRASTRUKTUR PEMBANGUNAN SECARA MERATA','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(8,3,'MENINGKATNYA KUALITAS PENGELOLAAN LINGKUNGAN HIDUP DAN KETAHANAN BENCANA','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(9,4,'BIROKRASI YANG BERSIH DAN AKUNTABEL','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(10,4,'PELAYANAN PUBLIK YANG PRIMA','admin','2024-02-20 00:26:31',NULL,NULL,0);

/*!40000 ALTER TABLE `tpmdsasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpmdtujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpmdtujuan`;

CREATE TABLE `tpmdtujuan` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMisi` bigint(10) unsigned NOT NULL,
  `Uraian` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpmdtujuan` WRITE;
/*!40000 ALTER TABLE `tpmdtujuan` DISABLE KEYS */;

INSERT INTO `tpmdtujuan` (`Uniq`, `IdMisi`, `Uraian`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,1,'Meningkatkan Kualitas Sumber Daya Manusia','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(2,1,'Meningkatkan Penguatan Ekonomi Kreatif','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(3,1,'Meningkatkan Penguatan Infrastruktur','admin','2024-02-20 00:26:31',NULL,NULL,0),
	(4,1,'Pengoptimalan Reformasi Birokrasi','admin','2024-02-20 00:26:31',NULL,NULL,0);

/*!40000 ALTER TABLE `tpmdtujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trenjakegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trenjakegiatan`;

CREATE TABLE `trenjakegiatan` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(10) unsigned NOT NULL,
  `KegKode` varchar(50) DEFAULT NULL,
  `KegUraian` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trenjakegiatansub
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trenjakegiatansub`;

CREATE TABLE `trenjakegiatansub` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdKegiatan` bigint(10) unsigned NOT NULL,
  `SubUraian` varchar(200) NOT NULL DEFAULT '',
  `SubKode` varchar(50) DEFAULT '',
  `SubIndikator` varchar(200) DEFAULT NULL,
  `SubTarget` varchar(50) DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trenjaprogram
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trenjaprogram`;

CREATE TABLE `trenjaprogram` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenstra` bigint(10) unsigned NOT NULL,
  `IdSasaranSkpd` bigint(10) unsigned NOT NULL,
  `Tahun` int(10) NOT NULL,
  `ProgKode` varchar(50) DEFAULT '',
  `ProgUraian` varchar(200) NOT NULL DEFAULT '',
  `ProgKeterangan` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trenstra
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trenstra`;

CREATE TABLE `trenstra` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdOPD` bigint(10) unsigned NOT NULL,
  `IdPeriode` bigint(10) unsigned NOT NULL,
  `RenstraNama` varchar(200) NOT NULL DEFAULT '',
  `RenstraKeterangan` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trenstrasasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trenstrasasaran`;

CREATE TABLE `trenstrasasaran` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdTujuan` bigint(10) unsigned NOT NULL,
  `IdSasaranPmd` bigint(10) unsigned NOT NULL,
  `Uraian` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trenstrasasarandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trenstrasasarandet`;

CREATE TABLE `trenstrasasarandet` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdSasaran` bigint(10) unsigned NOT NULL,
  `Uraian` varchar(200) NOT NULL DEFAULT '',
  `Target` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trenstratujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trenstratujuan`;

CREATE TABLE `trenstratujuan` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenstra` bigint(10) unsigned NOT NULL,
  `IdTujuanPmd` bigint(10) unsigned NOT NULL,
  `Uraian` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trisiko
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trisiko`;

CREATE TABLE `trisiko` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdOPD` bigint(10) unsigned NOT NULL,
  `IdRenstra` bigint(10) unsigned NOT NULL,
  `Tahun` int(10) NOT NULL,
  `Keterangan` text,
  `Status` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trisikokonteks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trisikokonteks`;

CREATE TABLE `trisikokonteks` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdRisiko` bigint(10) unsigned NOT NULL,
  `IdRef` bigint(10) unsigned DEFAULT NULL,
  `Level` enum('TUJUAN','SASARAN','PROGRAM','KEGIATAN','SUBKEGIATAN') DEFAULT NULL,
  `KonteksUraian` varchar(200) NOT NULL DEFAULT '',
  `KonteksIndikator` varchar(200) DEFAULT NULL,
  `KonteksSatuan` varchar(50) DEFAULT NULL,
  `KonteksTarget` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trisikolog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trisikolog`;

CREATE TABLE `trisikolog` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdRisikoLog` bigint(10) unsigned NOT NULL,
  `LogTanggal` date NOT NULL,
  `LogSebab` varchar(200) NOT NULL DEFAULT '',
  `LogDampak` varchar(200) NOT NULL DEFAULT '',
  `LogKeterangan` text,
  `LogRTP` varchar(200) DEFAULT NULL,
  `LogRTPWaktu` varchar(200) DEFAULT NULL,
  `LogRTPRealisasi` varchar(200) DEFAULT NULL,
  `LogRTPKeterangan` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trisikoreg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trisikoreg`;

CREATE TABLE `trisikoreg` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdKonteks` bigint(10) unsigned NOT NULL,
  `Tahap` varchar(200) DEFAULT NULL,
  `RisikoKode` varchar(50) DEFAULT NULL,
  `RisikoUraian` varchar(200) DEFAULT NULL,
  `RisikoPemilik` varchar(200) NOT NULL DEFAULT '',
  `SebabUraian` varchar(200) DEFAULT NULL,
  `SebabSumber` enum('INTERNAL','EKSTERNAL') DEFAULT NULL,
  `Control` enum('C','UC') DEFAULT NULL,
  `DampakUraian` varchar(200) DEFAULT NULL,
  `DampakPihak` varchar(200) NOT NULL DEFAULT '',
  `NumDampak` double DEFAULT NULL COMMENT 'Skala Dampak',
  `NumFreq` double DEFAULT NULL COMMENT 'Skala Kemungkinan',
  `NumRisiko` double DEFAULT NULL COMMENT 'Skala Dampak x Skala Kemungkinan',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trisikortp
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trisikortp`;

CREATE TABLE `trisikortp` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdRisikoReg` bigint(10) unsigned NOT NULL,
  `UraianPengendalian` text NOT NULL,
  `UraianCelah` text NOT NULL,
  `UraianRTP` int(11) NOT NULL,
  `RTPPenanggungJawab` int(11) DEFAULT NULL,
  `RTPWaktu` int(11) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
